CREATE TABLE gr4_partita (
  id_partita INT AUTO_INCREMENT PRIMARY KEY,
  modalita VARCHAR(32),
  punteggio INT,
  username_giocatore VARCHAR(50),
  FOREIGN KEY (username_giocatore) REFERENCES giocatore(username)
);
