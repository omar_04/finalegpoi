<?php
    //parametri
    class Config{
        private static $db_host="localhost";
        private static $db_user="root";
        private static $db_pwd="";
        private static $db_name="Stig_2023";
        public static function getDBhost(){ 
            return self::$db_host;
        }
        public static function getDBuser(){
            return self::$db_user;
        }
        public static function getDBpwd(){
            return self::$db_pwd;
        }
        public static function getDBname(){
            return self::$db_name;
        }
    }
    //connessione al DBMS con scelta del DB
    $conn=new mysqli(Config::getDBhost(),Config::getDBuser(),Config::getDBpwd(),Config::getDBname());
    if($conn->connect_error){
        die("Connessione fallita: ".$conn->connect_error);
    };
    function chiudi_connessione(){
        global $conn;
        mysqli_close($conn);
    }
	header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST');
?>
