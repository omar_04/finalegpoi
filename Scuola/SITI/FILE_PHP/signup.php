<?php
    require("./config.php");
    function check($user){ //controllo esistenza username in db
        global $conn;
        $query="SELECT * FROM utenti WHERE username=?";
        $stmt=$conn->prepare($query);
        $stmt->bind_param("s",$user);
        $stmt->execute();
        $result=$stmt->get_result();
        if($result->num_rows>0){
            return false;
        }else{
            return true;
        }
    }
    
    function signup($nm,$cgnm,$eml,$user,$pwd){ //registrazione
        global $conn;
        //preparazione query
        $query="INSERT INTO utenti(username,password,nome,cognome,email) VALUES(?,?,?,?,?)"; //prepared query
        $stmt=$conn->prepare($query); 
        if(!$stmt){
            die("Preparazione query fallita: ".$conn->error);
        }
        $stmt->bind_param("sssss",$user,$pwd,$nm,$cgnm,$eml); 
        $stmt->execute();
    }
    
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $nome=$_POST['nome'];
        $cognome=$_POST['cognome'];
        $user=$_POST['username'];
        $pwd=$_POST['password'];
        $email=$_POST['email'];
        $univoco=check($user);
        if($univoco){
            signup($nome,$cognome,$email,$user,$pwd);
            echo json_encode(true);
        }else{
            echo json_encode(false);
        }
        chiudi_connessione();
    }
?>
