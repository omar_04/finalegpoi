<?php
    require("./config.php");
    function get($user){
        global $conn;
        //preparazione query
        $query="SELECT punteggio,nomegioco FROM utenti WHERE username=?"; //prepared query
        $stmt=$conn->prepare($query); 
        if(!$stmt){
            die("Preparazione query fallita: ".$conn->error);
        }
        $stmt->bind_param("s",$user); 
        $stmt->execute();
        $result=$stmt->get_result();
        //controllo
        if($result->num_rows==1){
            echo json_encode($result->fetch_all(MYSQLI_ASSOC)[0]);
        }else{
            echo json_encode(false);
        }
    }
    if($_SERVER["REQUEST_METHOD"] == "POST"){ 
        $user=$_POST['user'];
        get($user);
        chiudi_connessione();
    }
?>