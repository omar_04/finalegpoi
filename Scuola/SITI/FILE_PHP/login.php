<?php
    require("./config.php");
    function controllo_credenziali($user,$pwd){
        global $conn;
        //preparazione query
        $query="SELECT * FROM utenti WHERE username=? AND password=?"; //prepared query
        $stmt=$conn->prepare($query); 
        if(!$stmt){
            die("Preparazione query fallita: ".$conn->error);
        }
        $stmt->bind_param("ss",$user,$pwd); 
        $stmt->execute();
        $result=$stmt->get_result();
        //controllo
        if($result->num_rows==1){
            echo json_encode($result->fetch_all(MYSQLI_ASSOC));
        }else{
            echo json_encode(false);
        }
    }
    if($_SERVER["REQUEST_METHOD"] == "POST"){ 
        $user=$_POST['username'];
        $pwd=$_POST['password'];
        controllo_credenziali($user,$pwd);
        chiudi_connessione();
    }
?>