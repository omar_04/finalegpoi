<?php
    require("./config.php");
    function set($punti,$gioco,$user){
        global $conn;
        //preparazione query
        $query="UPDATE utenti SET punteggio=?,nomegioco=? WHERE username=?"; //prepared query
        $stmt=$conn->prepare($query); 
        if(!$stmt){
            die("Preparazione query fallita: ".$conn->error);
        }
        $stmt->bind_param("sss",$punti,$gioco,$user); 
        $stmt->execute();
        $result=$stmt->get_result();
        //controllo
        if($result->num_rows==1){
            echo json_encode($result->fetch_all(MYSQLI_ASSOC)[0]);
        }else{
            echo json_encode(false);
        }
    }
    if($_SERVER["REQUEST_METHOD"] == "POST"){ 
        $user=$_POST['user'];
        $punti=$_POST['punti'];
        $gioco=$_POST['gioco'];
        set($punti,$gioco,$user);
        chiudi_connessione();
    }
?>