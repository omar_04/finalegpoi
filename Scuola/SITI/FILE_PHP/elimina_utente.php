<?php
    require("./config.php");
    function elimina($user){
        global $conn;
        //preparazione query
        $query="DELETE FROM utenti WHERE username=?"; //prepared query
        $stmt=$conn->prepare($query); 
        if(!$stmt){
            die("Preparazione query fallita: ".$conn->error);
        }
        $stmt->bind_param("s",$user); 
        $stmt->execute();
        $result=$stmt->get_result();
        echo json_encode(true);
    }
    if($_SERVER["REQUEST_METHOD"] == "POST"){ 
        $user=$_POST['username'];
        elimina($user);
        chiudi_connessione();
    }
?>