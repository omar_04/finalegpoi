import React from 'react'
import logo from "../img/logowhite.png"
import home from "../img/icons/game.png"
import docs from "../img/icons/docs.png"
import about_us from "../img/icons/about_us.png"
import "./Footergioco.css"
import { Link } from 'react-router-dom'

const Footergioco = () => {
  var nomegioco="geohoot"
  return (
    <footer>
        <img src={logo} className="logo" alt="logo"/>
        <ul>
            <h2>Link utili</h2>
            <li><Link to={`../giochi/${nomegioco}/home`}><img src={home} className="icon" alt="icon"/><span>Home</span></Link></li>
            <li><Link to={`../giochi/${nomegioco}/docs`}><img src={docs} className="icon" alt="icon"/><span>Docs</span></Link></li>
            <li><Link to={`../giochi/${nomegioco}/sudinoi`}><img src={about_us} className="icon" alt="icon"/><span>Su di noi</span></Link></li>
        </ul>
    </footer>
  )
}

export default Footergioco