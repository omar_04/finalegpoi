import "./Headergioco.css"

const Headergioco = () => {
  var nomegioco="GeoHoot"
  var slogan="Esplora il mondo con Geohoot e scatena il tuo spirito avventuroso!"
  var link="http://localhost/Scuola/GIOCHI/Geohoot"
  return (
    <header className='head-gioco'>
        <div>
            <h2>{nomegioco}</h2>
            <h3>{slogan}</h3>			
        </div>
        <a href={link} className="button" id="gioca">GIOCA ORA</a>
    </header>
  )
}

export default Headergioco