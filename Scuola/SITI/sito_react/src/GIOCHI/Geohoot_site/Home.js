import './stile.css'
import Headergioco from './componenti_gioco/Headergioco'
import Footergioco from './componenti_gioco/Footergioco'

const Home = () => {
	return (
		<div className="container gioco">
			<Headergioco/>
			<section>
				<h1>INTRODUZIONE</h1>
				<div>
					GeoHoot è un'applicazione web di tipo gioco, incentrata sulla geografia e ispirata allo stile di Kahoot. Per
					lo sviluppo della nostra web app, abbiamo utilizzato il framework React.js. Il gioco si presenta come una
					serie di quiz a scelta multipla, con domande di livello semplice, dove saranno fornite più opzioni di
					risposta, ma solo una di esse sarà corretta.
				</div>
			</section>
			<section>
				<h1>COME FUNZIONA</h1>
				<div>
					Le risposte saranno presentate attraverso l'uso di due o più pulsanti distinti, su cui il giocatore potrà
					cliccare. Il gioco comprenderà tre tipi di domande distinti:
					<ul>
						<li>Domande di tipo vero o falso.</li>
					</ul>
					<ul>
						<li>Domande semplici con una o più risposte corrette.</li>
					</ul>
					<ul>
						<li>Domande basate su immagini da riconoscere, come ad esempio immagini geografiche di diverse
					regioni italiane.</li>
					</ul>
					Per archiviare queste domande, utilizzeremo un file JSON specifico per ogni tipo di domanda. Il quiz sarà
					generato in modo da includere un numero adeguato di domande per ogni tipo, al fine di garantire
					un'esperienza interattiva ed interessante per il gameplay. Pertanto, il risultato finale sarà un quiz
					"dinamico" che si presenterà in modo diverso ad ogni partita, con la variazione del numero di domande in
					base alla difficoltà selezionata e il tempo disponibile per rispondere alle domande che varierà di
					conseguenza. Prima di poter iniziare a giocare, i nostri giocatori dovranno effettuare l'accesso, in modo da
					consentirci di recuperare i dati degli utenti e creare classifiche generali e statistiche personali.
					
				</div>
			</section>
			<section>
				<h1>PUNTEGGIO</h1>
				<div>
					Una volta conclusa una partita, verrà presentato il vostro punteggio relativo alla partita appena conclusa. Il
					punteggio sarà basato sul numero di domande che abbiamo risposto correttamente, riflettendo la nostra
					conoscenza e le nostre abilità nel gioco.
					Se abbiamo ottenuto un punteggio che supera il nostro record personale o quello di un'altra persona nella
					classifica, il nostro punteggio verrà inserito nella classifica e abbiamo l'opportunità di vedere il nostro
					nome nella lista dei migliori giocatori.
				</div>
			</section>
			<Footergioco nomeGioco="gymmy"/>
		</div>
	)
}

export default Home