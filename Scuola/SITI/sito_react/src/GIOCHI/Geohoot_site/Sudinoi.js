import './sudinoi.css'
import Headergioco from './componenti_gioco/Headergioco'
import Footergioco from './componenti_gioco/Footergioco'
//AVATAR
import agbo from "./img/avatars/agbo.jpg"
import ciuro from "./img/avatars/Ciuraru.jpg"
import dinu from "./img/avatars/DinuTeodorDaniel.jpg"
import massa from "./img/avatars/MassanoAndrea.jpg"
import salvo from "./img/avatars/salvo.jpg"

const Sudinoi = () => {
    return (
        <div className="container gioco">
            <Headergioco/>
            <div className="contAbout">
				<div id="about">
					<div id="titoloAbout">IL NOSTRO TEAM</div>
					<div className="contTextAbout">
						<div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={agbo} alt="agbo" />
                            </div>
                            <div className="nome">Maikel Agban</div>
						</div>
                        <div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={ciuro} alt="ciuro" />
                            </div>
                            <div className="nome">Marco Ciuraru</div>
                        </div>
                        <div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={dinu} alt="dinu" />
                            </div>
                            <div className="nome">Teodor Dinu Daniel</div>
                        </div>
						<div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={massa} alt="massa" />
                            </div>
                            <div className="nome">Andrea Massano</div>
                        </div>
                        <div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={salvo} alt="salvo" />
                            </div>
                            <div className="nome">Simone Salvai</div>
                        </div>
					</div>
				</div>
			</div>
			<hr className="sep"/>
			<div id="info">
				<h1>Chi siamo</h1>
				<div>
					Siamo studenti della classe 5C.
				</div>
			</div>
            <Footergioco nomeGioco="gymmy"/>
        </div>
    )
}

export default Sudinoi