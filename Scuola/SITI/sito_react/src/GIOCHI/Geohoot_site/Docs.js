import './docs.css'
import Headergioco from './componenti_gioco/Headergioco'
import Footergioco from './componenti_gioco/Footergioco'

const Docs = () => {
    function ShowDocs(e) {
        let nome=e.target.value
        let nomeMin=nome.toLowerCase()
        let titolo= document.getElementById("titoloDocs")
        titolo.innerText=nome
        document.querySelectorAll(".docsText").forEach((el)=>{
            el.style.display="none"
        })
        document.querySelector(`div[title='${nomeMin}']`).style.display="flex"
    }
	return (
		<div className="container gioco">
			<Headergioco/>
            <div className="contDocs">
				<div className="indice">
					<div id="titoloIndice">INDICE</div>
					<div id="contSezioni">
						<input className="sezione" type="button" value="INSTALLAZIONE" onClick={ShowDocs}/>   
						<input className="sezione" type="button" value="DOCUMENTAZIONE" onClick={ShowDocs}/>   
					</div>
				</div>
				<div id="docs">
					<div className="contText">
						<div id="titoloDocs">INSTALLAZIONE</div>
						<div style={{'display':'flex','flex-direction':'row'}} className="docsText" title="installazione">
							Per proseguire con l'installazione, scaricare la <a style={{'font-weight':'900', 'margin-right':'5px','margin-left':'5px', 'color':'lightblue'}} target="_blank" href="https://drive.google.com/drive/folders/1kT18RGefm_5UbaFsBAxuj1VHFH2GaOG6?usp=share_link"> Cartella</a> e aprire il file index.html
						</div>
						<div style={{'display':'none','flex-direction':'row'}} className="docsText" title="documentazione">
							Vai alla <a style={{'font-weight':'900','margin-left':'5px', 'color':'lightblue'}} target="_blank" href="https://github.com/teodn7/Gpoi/blob/0fede24379090f5d66acb78917862d09bdb1dac1/Documentazione_GeoHoot-2.pdf">Documentazione</a>
						</div>
					</div>
				</div>
			</div>
			<Footergioco nomeGioco="gymmy"/>
        </div>
        
  )
}

export default Docs