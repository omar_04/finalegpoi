import './docs.css'
import Headergioco from './componenti_gioco/Headergioco'
import Footergioco from './componenti_gioco/Footergioco'

const Docs = () => {
    function ShowDocs(e) {
        let nome=e.target.value
        let nomeMin=nome.toLowerCase()
        let titolo= document.getElementById("titoloDocs")
        titolo.innerText=nome
        document.querySelectorAll(".docsText").forEach((el)=>{
            el.style.display="none"
        })
        document.querySelector(`div[title='${nomeMin}']`).style.display="block"
    }
	return (
		<div className="container gioco">
			<Headergioco/>
            <div className="contDocs">
				<div className="indice">
					<div id="titoloIndice">INDICE</div>
					<div id="contSezioni">
						<input className="sezione" type="button" value="INSTALLAZIONE" onClick={ShowDocs}/>   
						<input className="sezione" type="button" value="DOCUMENTAZIONE" onClick={ShowDocs}/>   
						<input className="sezione" type="button" value="MARIO RUI" onClick={ShowDocs}/>   
					</div>
				</div>
				<div id="docs">
					<div className="contText">
						<div id="titoloDocs">INSTALLAZIONE</div>
						<div className="docsText" title="installazione">
							INSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONE
							INSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONE
							INSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONEINSTALLAZIONE
						</div>
						<div className="docsText" title="documentazione">
							DOCUMENTAZIONEDOCUMENTAZIONEDOCUMENTAZIONEDOCUMENTAZIONEDOCUMENTAZIONEDOCUMENTAZIONEDOCUMENTAZIONEDOCUMENTAZIONE
							DOCUMENTAZIONEDOCUMENTAZIONEDOCUMENTAZIONEDOCUMENTAZIONEDOCUMENTAZIONEDOCUMENTAZIONEDOCUMENTAZIONEDOCUMENTAZIONE
							DOCUMENTAZIONEDOCUMENTAZIONEDOCUMENTAZIONEDOCUMENTAZIONEDOCUMENTAZIONEDOCUMENTAZIONEDOCUMENTAZIONEDOCUMENTAZIONE
						</div>
						<div className="docsText" title="mario rui">
							Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut animi veritatis, natus vel officia magni reprehenderit quo tempore id earum expedita sint facere dignissimos enim rem nulla dolorum ab quibusdam.
						</div>
					</div>
				</div>
			</div>
			<Footergioco nomeGioco="gymmy"/>
        </div>
        
  )
}

export default Docs