import './stile.css'
import Headergioco from './componenti_gioco/Headergioco'
import Footergioco from './componenti_gioco/Footergioco'

const Home = () => {
	return (
		<div className="container gioco">
			<Headergioco/>
			<section>
				<h1>COME FUNZIONA</h1>
				<div>
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Est error voluptates doloremque a perferendis, cupiditate id aperiam provident alias veritatis veniam molestias distinctio voluptatum enim. Totam obcaecati corporis unde dolore.
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Est error voluptates doloremque a perferendis, cupiditate id aperiam provident alias veritatis veniam molestias distinctio voluptatum enim. Totam obcaecati corporis unde dolore.
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Est error voluptates doloremque a perferendis, cupiditate id aperiam provident alias veritatis veniam molestias distinctio voluptatum enim. Totam obcaecati corporis unde dolore.
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Est error voluptates doloremque a perferendis, cupiditate id aperiam provident alias veritatis veniam molestias distinctio voluptatum enim. Totam obcaecati corporis unde dolore.
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Est error voluptates doloremque a perferendis, cupiditate id aperiam provident alias veritatis veniam molestias distinctio voluptatum enim. Totam obcaecati corporis unde dolore.
				</div>
			</section>
			<section>
				<h1>PUNTEGGIO</h1>
				<div>
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Est error voluptates doloremque a perferendis, cupiditate id aperiam provident alias veritatis veniam molestias distinctio voluptatum enim. Totam obcaecati corporis unde dolore. Lorem ipsum dolor sit amet consectetur adipisicing elit. Est error voluptates doloremque a perferendis, cupiditate id aperiam provident alias veritatis veniam molestias distinctio voluptatum enim. Totam obcaecati corporis unde dolore. Lorem ipsum dolor sit amet consectetur adipisicing elit. Est error voluptates doloremque a perferendis, cupiditate id aperiam provident alias veritatis veniam molestias distinctio voluptatum enim. Totam obcaecati corporis unde dolore. Lorem ipsum dolor sit amet consectetur adipisicing elit. Est error voluptates doloremque a perferendis, cupiditate id aperiam provident alias veritatis veniam molestias distinctio voluptatum enim. Totam obcaecati corporis unde dolore. Lorem ipsum dolor sit amet consectetur adipisicing elit. Est error voluptates doloremque a perferendis, cupiditate id aperiam provident alias veritatis veniam molestias distinctio voluptatum enim. Totam obcaecati corporis unde dolore.
				</div>
			</section>
			<section>
				<h1>REGOLE</h1>
				<div>
					<h3>Lorem</h3>
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Est error voluptates doloremque a perferendis, cupiditate id aperiam provident alias veritatis veniam molestias distinctio voluptatum enim. Totam obcaecati corporis unde dolore. Lorem ipsum dolor sit amet consectetur adipisicing elit. Est error voluptates doloremque a perferendis, cupiditate id aperiam provident alias veritatis veniam molestias distinctio voluptatum enim. Totam obcaecati corporis unde dolore. Lorem ipsum dolor sit amet consectetur adipisicing elit. Est error voluptates doloremque a perferendis, cupiditate id aperiam provident alias veritatis veniam molestias distinctio voluptatum enim. Totam obcaecati corporis unde dolore. Lorem ipsum dolor sit amet consectetur adipisicing elit. Est error voluptates doloremque a perferendis, cupiditate id aperiam provident alias veritatis veniam molestias distinctio voluptatum enim. Totam obcaecati corporis unde dolore. Lorem ipsum dolor sit amet consectetur adipisicing elit. Est error voluptates doloremque a perferendis, cupiditate id aperiam provident alias veritatis veniam molestias distinctio voluptatum enim. Totam obcaecati corporis unde dolore.
				</div>
			</section>
			<Footergioco nomeGioco="gymmy"/>
		</div>
	)
}

export default Home