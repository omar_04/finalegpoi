import { Link } from 'react-router-dom';
import "./Headergioco.css"

const Headergioco = () => {
  var nomegioco="Nome gioco"
  var slogan="Slogan del gioco"
  var link=""
  return (
    <header className='head-gioco'>
        <div>
            <h2>{nomegioco}</h2>
            <h3>{slogan}</h3>			
        </div>
        <Link to={link} className="button" id="gioca">GIOCA ORA</Link>
    </header>
  )
}

export default Headergioco