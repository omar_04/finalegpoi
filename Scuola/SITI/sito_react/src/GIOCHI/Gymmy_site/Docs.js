import './docs.css'
import Headergioco from './componenti_gioco/Headergioco'
import Footergioco from './componenti_gioco/Footergioco'

const Docs = () => {
    function ShowDocs(e) {
        let nome=e.target.value
        let nomeMin=nome.toLowerCase()
        let titolo= document.getElementById("titoloDocs")
        titolo.innerText=nome
        document.querySelectorAll(".docsText").forEach((el)=>{
            el.style.display="none"
        })
        document.querySelector(`div[title='${nomeMin}']`).style.display="block"
    }
	return (
		<div className="container gioco">
			<Headergioco/>
            <div className="contDocs">
				<div className="indice">
					<div id="titoloIndice">INDICE</div>
					<div id="contSezioni">
						<input className="sezione" type="button" value="INSTALLAZIONE" onClick={ShowDocs}/>   
						<input className="sezione" type="button" value="DOCUMENTAZIONE" onClick={ShowDocs}/>   
					</div>
				</div>
				<div id="docs">
					<div className="contText">
						<div id="titoloDocs">INSTALLAZIONE</div>
						<div className="docsText" title="installazione">
							<h3>Utente</h3>
							Per un giocatore è sufficiente avviare il gioco dal file <i>index.html</i> in modo da essere indirizzato sul browser e poter iniziare a giocare liberamente.
							<h3>Amministratore</h3>
							In una situazione di amministrazione per avviare il progetto è necessario avere <i>npm</i> installato (Portale di moduli per il funzionamento del gioco).
							Per gestire domande e risposte bisogna avere XAMPP in cui si possono vedere tutte le informazioni utilizzate nel gioco.
							<br/>
							Questo comando serve per far partire il gioco:
							<pre>
								<code>
									npm start
								</code>
							</pre>
							Questo per installare i moduli necessari per il gioco:
							<pre>
								<code>
									npm install
								</code>
							</pre>
							Per quanto riguarda XAMPP, questo è il comando per poter avviare i server:
							<pre>
								<code>
									sudo /opt/lampp/manager-linux-x64.run
								</code>
							</pre>
						</div>
						<div className="docsText" title="documentazione">
						<h3>SCHERMATA DI INIZIO GIOCO</h3>
						inizialmente vengono importati Phaser e i relativi assets, come il logo del gioco, lo sfondo e la forma dei button.
						Per prima cosa viene creata la classe giocaora che estende Phaser.scene; viene subito richiamato il costruttore e con esso viene settato il nome della scena.
						Successivamente nella funzione preload() vengono caricate le varie immagini in memoria, quali lo sfondo, il logo e il button.
						Nella funzione create() vengono invece ricavate le dimensioni (larghezza e altezza) della finestra di gioco e salvate in variabili.
						Viene poi aggiunta l’immagine di sfondo, in una posizione centrata e poi grazie a setScale(2) viene adattata a tutto schermo.
						Vengono effettuate le stesse operazioni con il logo e il button, con la modifica della posizione y in modo da avere la disposizione verticale in modo corretto.
						A riga 20 invece viene gestito l’evento di click sul bottone, che serve per avanzare alla successiva schermata di caricamento
						Infine viene settata la scritta “GIOCA ORA” all’interno del button.
						La funzione update() è rimasta vuota in quanto non devono essere gestite alcune operazioni al di fuori del semplice click sul button.
						<h3>SCHERMATA DI CARICAMENTO</h3>
						Questa volta vengono importate più immagini in quanto abbiamo deciso di ‘costruire’ la barra di caricamento e l’animazione dell’omino che alza e abbassa il bilanciere.
						All’interno della classe caricamento, viene richiamato il costruttore e settato il nome della scena come avvenuto in precedenza, così come vengono caricate in memoria tutte le immagini necessarie per realizzare questa pagina.
						Nella funzione create() viene creata l’animazione per l’omino; come si può vedere da riga 36 vengono alternate le due immagini omino1 e omino2 in modo da simulare il movimento, oltre che venir settati un frame rate di 8 e un repeat di -1, che serve per ripetere il movimento all’infinito.
						Successivamente viene visualizzata a video l’immagine del primo omino, che sarà il punto di partenza della nostra animazione.
						Funzionamento analogo caratterizza la barra di caricamento formata da 5 parti che vengono visualizzate gradualmente, simulando una vera e propria progress bar; come si può notare nel codice, ogni componente della barra ha una durata diversa per renderne più realistico il funzionamento.
						Successivamente, vengono visualizzati il logo e la prima fase della progress bar, per poi richiamare una funzione che serve per passare alla scena successiva, una volta terminato il nostro caricamento.
						<h3>SCHERMATA DI MAIN</h3>
						Nella schermata principale del gioco inizialmente vengono importati gli attrezzi necessari per costruire l’ambiente della palestra.
						Viene creato un oggetto relativo al punteggio del giocatore.
						Successivamente viene dichiarata una funzione per gestire la visualizzazione dei popup all’interno del gioco e un’altra per gestire le interazioni con l’utente.
						La funzione per associare la documentazione è analoga a quella relativa ai vari popup del gioco.
						La funzione modificaPopup invece imposta lo stile e la posizione del riquadro per giocare e vengono modificati alcuni parametri nel momento in cui l’utente seleziona uno di essi.
						Con la funzione get_cookie() viene ricevuto il cookie salvato nella memoria di archiviazione del browser.
						Con get_punti_giocatore() vengono ricavati i punteggi ottenuti dall’utente nei giochi grazie ad un’interrogazione al database, arrotondati ad una cifra decimale.
						aggiungi_giocatore() serve invece per richiamare il file php e grazie ad una funzione fetch vengono aggiunte le credenziali relative al nuovo giocatore.
						Nel preload() vengono caricate le immagini che servono per il gioco, come i vari attrezzi e il personaggio che si muove nella scena della palestra.
						Nella funzione create() vengono posizionati definitavamente e grazie alla crea_attrezzo() si ottimizzano le linee di codice relative a questo compito, creando tutti gli attrezzi che sono presenti nella mappa e gestite le interazioni di hover e click.
						Viene poi aggiunto il personaggio del gioco e settati i tasti necessari per il movimento del character. 
						Infine nella funzione update() troviamo i vari controlli a cascata necessari per gestire il movimento all’interno della palestra, inclusi quelli in diagonale, necessari per dare un tocco di realismo al gioco.

						<h3>SCHERMATA DEL QUIZ</h3>
						Il file quiz.js serve principalmente per ottenere le domande (in modo casuale) dal database mediante un’interrogazione che invia la categoria e la difficoltà e riceve i quesiti che soddisfano tali requisiti, mostrandoli succesivamente a video.
						È presente inoltre un’analoga funzione per ricevere le risposte abbinate a tali domande (viene infatti passato come parametro l’id della domanda).
						Con la get_moltiplicatore() otteniamo il moltiplicatore della difficoltà associata alla domanda precedentemente scaricata, in modo da calcolare in modo dinamico il punteggio per ogni risposta corretta/sbagliata.
						Le funzioni get_punti_giocatore() e get_cookie() sono molto simili a quelle presenti nel main, in quanto sono necessarie per ricavare i punti ottenuti dal giocatore e il relativo cookie associato al suo “username”
						Con update_punti_giocatore() viene gestito l’aggiornamento del punteggio del quiz, modificando il punteggio del personaggio, a seconda di risposte corrette oppure errate.
						Nella create() vengono creati i testi delle domande e i relativi pulsanti con le possibili risposte ai quesiti.
						Con la funzione next() viene gestito il controllo sulla correttezza della risposte e, dopo aver aggiornato il punteggio, si passa alla domanda successiva e, in caso di fine quiz, viene mostrato il pulsante per chiudere la schermata in modo da ritornare al mondo della palestra. Infine nella funzione di update() vengono impostate le domande e le risposte all’interno di array che memorizzano tutti i testi.
						<h3>SCHERMATA DELLA DOCUMENTAZIONE</h3>
						In questa scena viene inizialmente dato un titolo alla documentazione con determinate caratteristiche di stile, in modo da renderlo adeguato al resto della grafica presente nel gioco.
						Successivamente vengono creati dei button che hanno lo scopo di fungere da menu, in quanto servono per richiamare la scena spiegazione.js. Questo serve per effettuare il passaggio di parametri in modo da parametrizzare le query in modo corretto. Si può notare come nel caso venga cliccato il bottone (situazione di pointerdown in cui si passa come parametro la categoria FORZA, oppure RESISTENZA e VELOCITÀ)  si passa alla scena che descrive i vari punti legati alla statistica selezionata.
						Ognuna delle 3 caratteristiche da migliorare ha un button dedicato (in questo caso abbiamo docu1, docu2 e docu3).
						Infine viene dichiarata la funzione next(), che come abbiamo detto prima serve per far iniziare la scena con le relative spiegazioni (stampate a seconda della categoria ricevuta).

						<h3>SCHERMATA DELLE SPIEGAZIONI</h3>
						In questo file la funzione principale è scarica_spiegazioni(), a cui viene passata la categoria della spiegazione. I testi delle spiegazioni vengono scaricati, creati e mostrati a video uno sotto l’altro.
						Oltre alla semplice visualizzazione delle spigazioni è presente anche un pulsante interattivo, esso se viene premuto si viene riportati alla schermata principale della palestra e si può nuovamente tornare a esplorare la mappa.
						Qui non abbiamo codice nella funzione update() in quanto quest’ultima viene richiamata ciclicamente, mentre lo scopo di questo file è mostrare solamente testi statici.

						</div>
					</div>
				</div>
			</div>
			<Footergioco nomeGioco="gymmy"/>
        </div>
        
  )
}

export default Docs