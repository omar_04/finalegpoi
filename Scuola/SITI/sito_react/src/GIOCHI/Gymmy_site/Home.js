import './stile.css'
import Headergioco from './componenti_gioco/Headergioco'
import Footergioco from './componenti_gioco/Footergioco'

const Home = () => {
	return (
		<div className="container gioco">
			<Headergioco/>
			<section>
				<h1>COME FUNZIONA</h1>
				<div>
				Il gioco viene ambientato in una palestra con attrezzatura adatta a svolgere alcuni esercizi fisici. Tale ambiente può essere completamente esplorato dal giocatore e, quando quest'ultimo si avvicina ad un determinato macchinario, è possibile “svolgere” l’esercizio dedicato. Lo svolgimento dell’esercizio consiste in un quiz a 3 domande di teoria basate sulla tipologia del macchinario, infatti esistono 3 categorie di domande: sulla forza, resistenza e velocità. 
E’ possibile consultare la teoria riguardante queste 3 categorie in una sezione apposita della palestra, in modo da leggerla e impararla, per poter poi rispondere correttamente ai quiz del gioco. 

				</div>
			</section>
			<section>
				<h1>PUNTEGGIO</h1>
				<div>
				Alla termine di ogni esercizio, le statistiche del personaggio vengono aggiornate, aumentando o diminuendo in base al numero di risposte corrette. E’ possibile poter rigiocare un esercizio in modo da poter migliorare il punteggio ottenuto in precedenza ma con il rischio di perdere ulteriori punti.
				</div>
			</section>
			<section>
				<h1>REGOLE</h1>
				<div>
					L'utente ha la possibilità di muoversi all'interno dell'ambiente della palestra con il suo personaggio.
					Cliccando su un attrezzo disponibile sulla mappa, verrà mostrato a video un popup per giocare al quiz dell'attrezzo in questione.
					Il quiz è formato da tre domande a risposta chiusa di diversa tipologia (forza, resistenza e velocità) e le relative difficoltà sono ricavate casualmente da una lista di quesiti disponibili.
					Rispondendo correttamente a tali domande viene aumentato il punteggio, in caso contrario avremo un decremento delle statistiche che sono riportate e sempre consultabili al centro della mappa.
					All'interno del gioco è presente inoltre un centro di spiegazioni che l'utente può consultare liberamente in qualsiasi momento; questo serve per preparare il giocatore ai quiz che andrà ad affrontare oppure per ripassare in seguito ad un quiz con esito negativo.
				</div>
			</section>
			<Footergioco nomeGioco="gymmy"/>
		</div>
	)
}

export default Home