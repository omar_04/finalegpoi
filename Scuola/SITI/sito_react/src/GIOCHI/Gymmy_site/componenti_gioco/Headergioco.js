import "./Headergioco.css"

const Headergioco = () => {
  var nomegioco="Gymmy"
  var slogan="Impara allenandoti"
  var link="http://localhost/Scuola/GIOCHI/Gymmy_game"
  return (
    <header className='head-gioco'>
        <div>
            <h2>{nomegioco}</h2>
            <h3>{slogan}</h3>			
        </div>
        <a href={link} className="button" id="gioca">GIOCA ORA</a>
    </header>
  )
}

export default Headergioco