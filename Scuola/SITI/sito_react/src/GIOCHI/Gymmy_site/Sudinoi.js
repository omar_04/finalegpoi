import './sudinoi.css'
import Headergioco from './componenti_gioco/Headergioco'
import Footergioco from './componenti_gioco/Footergioco'
//AVATAR
import rera from "./img/avatars/Octo.jpeg"
import anto from "./img/avatars/Tony.jpeg"
import cappe from "./img/avatars/Ciaplin.jpeg"
import omar from "./img/avatars/Omarre.jpeg"

const Sudinoi = () => {
    return (
        <div className="container gioco">
            <Headergioco/>
            <div className="contAbout">
				<div id="about">
					<div id="titoloAbout">IL NOSTRO TEAM</div>
					<div className="contTextAbout">
						<div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={anto} alt="tony" />
                            </div>
                            <div className="nome">Antonio Amedeo Castaldo</div>
						</div>
                        <div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={cappe} alt="lucappe" />
                            </div>
                            <div className="nome">Luca Capellino</div>
                        </div>
                        <div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={rera} alt="rerabyte" />
                            </div>
                            <div className="nome">Alessio Rera</div>
                        </div>
						<div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={omar} alt="omarre" />
                            </div>
                            <div className="nome">Omar Frailone</div>
                        </div>
					</div>
				</div>
			</div>
			<hr className="sep"/>
			<div id="info">
				<h1>Chi siamo</h1>
				<div>
					Siamo un gruppo di studenti della classe 5C dell'ITIS di Verzuolo che si occupa di programmazione.
                    Il nostro obiettivo era di sviluppare un gioco totalmente esplorabile e istruttivo che possa in qualche modo accrescere la curiosità e le competenze di chi usufruisce del nostro gioco.
                </div>
			</div>
            <Footergioco nomeGioco="gymmy"/>
        </div>
    )
}

export default Sudinoi