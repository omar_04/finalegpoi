import './stile.css'
import Headergioco from './componenti_gioco/Headergioco'
import Footergioco from './componenti_gioco/Footergioco'

const Home = () => {
	return (
		<div className="container gioco">
			<Headergioco/>
			<section>
				<h1>COME FUNZIONA</h1>
				<div>
					Epic trombone è un gioco basato sulla tempistica e sulla coordinazine mente corpo.
					Una volta effettuato il login e scelta la canzone desiderata si può iniziare a giocare.
					Verrà visualizzato un simil-pentagramma verticale e a ogni riga corrisponde una nota che a sua volta
					corrisponde a un tasto della tastiera del computer. Sincronizzate con la base di sottofondo
					scorreranno delle note, solo quando la nota avrà raggiunto il tasto da premere si potrà 'suonare'.

				</div>
			</section>
			<section>
				<h1>PUNTEGGIO</h1>
				<div>
					Il punteggio della partita viene incrementato in base alle note suonate nel momento esatto,
					per ogni nota verranno assegnati 10 punti.
					Se si manca una nota o se viene suonata nel momento sbagliato il punteggio non aumenterà e il counter degli errori salirà a 1,
					il counter degli errori è visualizzabile in alto a destra tramite una chiave di Sol che scompare con l'aumentare degli errori.
					Si perde la partita solo quando si commettono 5 errori CONSECUTIVI.
				</div>
			</section>
			<Footergioco nomeGioco="gymmy"/>
		</div>
	)
}

export default Home