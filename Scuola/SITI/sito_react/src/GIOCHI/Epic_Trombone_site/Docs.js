import './docs.css'
import Headergioco from './componenti_gioco/Headergioco'
import Footergioco from './componenti_gioco/Footergioco'

const Docs = () => {
    function ShowDocs(e) {
        let nome=e.target.value
        let nomeMin=nome.toLowerCase()
        let titolo= document.getElementById("titoloDocs")
        titolo.innerText=nome
        document.querySelectorAll(".docsText").forEach((el)=>{
            el.style.display="none"
        })
        document.querySelector(`div[title='${nomeMin}']`).style.display="block"
    }
	return (
		<div className="container gioco">
			<Headergioco/>
            <div className="contDocs">
				<div className="indice">
					<div id="titoloIndice">INDICE</div>
					<div id="contSezioni">
						<input className="sezione" type="button" value="INSTALLAZIONE" onClick={ShowDocs}/>   
						<input className="sezione" type="button" value="DOCUMENTAZIONE" onClick={ShowDocs}/>   
					</div>
				</div>
				<div id="docs">
					<div className="contText">
						<div id="titoloDocs">INSTALLAZIONE</div>
						<div className="docsText" title="installazione">
							Per poter giocare a epic trombone non è necessario installare nulla, serve solo un browser.
							Nella barra degli indirizzi scrivere "gruppo2gpoi.onrender.com/" e si arriverà alla pagina di login del gioco.
						</div>
						<div className="docsText" title="documentazione">
							La documentazione relativa a EPIC TROMBONE è disponibile al seguente link:<br></br>
							 https://gitlab.com/progetti2023/gr02/-/blob/Documentazione/Documentazione_terzo_sprint.pdf
						</div>
					</div>
				</div>
			</div>
			<Footergioco nomeGioco="gymmy"/>
        </div>
        
  )
}

export default Docs