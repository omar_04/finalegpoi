import { useNavigate } from 'react-router-dom';
import "./Headergioco.css"
import { useEffect,useState } from 'react';

const Headergioco = () => {
  var nomegioco="Epic Trombone"
  var slogan=""

  const [username_storage, setusername_storage] = useState("")

    const nav=useNavigate()
    useEffect(() => {
        if(localStorage.getItem("user")===null){ //controllo se è loggato
            nav("../login")
        } else {
            let usr=JSON.parse(localStorage.getItem("user"))
            setusername_storage(usr['username'])
        }
    }, [])
    var link="http://localhost:1729/epicTrombone/?user="+username_storage

  return (
    <header className='head-gioco'>
        <div>
            <h2>{nomegioco}</h2>
            <h3>{slogan}</h3>			
        </div>
        <a href={link} className="button" id="gioca">GIOCA ORA</a>
    </header>
  )
}

export default Headergioco