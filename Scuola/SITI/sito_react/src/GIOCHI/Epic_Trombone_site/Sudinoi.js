import './sudinoi.css'
import Headergioco from './componenti_gioco/Headergioco'
import Footergioco from './componenti_gioco/Footergioco'
//AVATAR
import Fornero from "./img/avatars/ForneroGiorgio.jpg"
import Giolitti from "./img/avatars/GiolittiPaolo.jpg"
import Xu from "./img/avatars/XuXuFan.jpg"
import Zanche from "./img/avatars/Zanche.PNG"
import Michael from "./img/avatars/Michael.jpeg"
import Cristini from "./img/avatars/Cristini.jpg"



const Sudinoi = () => {
    return (
        <div className="container gioco">
            <Headergioco/>
            <div className="contAbout">
				<div id="about">
					<div id="titoloAbout">IL NOSTRO TEAM</div>
					<div className="contTextAbout">
						<div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={Fornero} alt="fornero" />
                            </div>
                            <div className="nome">Giorgio Fornero</div>
						</div>
                        <div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={Giolitti} alt="giolitti" />
                            </div>
                            <div className="nome">Paolo Giolitti</div>
                        </div>
                        <div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={Xu} alt="XuXu" />
                            </div>
                            <div className="nome">Xu Fan Xu</div>
                        </div>
						<div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={Zanche} alt="Zanche" />
                            </div>
                            <div className="nome">Alessandro Zanchettin</div>
                        </div>
                        <div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={Michael} alt="mich" />
                            </div>
                            <div className="nome">Michael Mellano</div>
						</div>
                        <div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={Cristini} alt="crostino" />
                            </div>
                            <div className="nome">Alex Cristini</div>
						</div>
					</div>
				</div>
			</div>
			<hr className="sep"/>
			<div id="info">
				<h1>Chi siamo</h1>
                    <div>
                    Siamo studenti della 5^C Info dell'ITIS Rivoira di Verzuolo
                    </div>
			</div>
            <Footergioco nomeGioco="gymmy"/>
        </div>
    )
}

export default Sudinoi