import './sudinoi.css'
import Headergioco from './componenti_gioco/Headergioco'
import Footergioco from './componenti_gioco/Footergioco'
//AVATAR
import bekim from "./img/avatars/Bekim.png"
import dalbe from "./img/avatars/dalbe.jpeg"
import mairone from "./img/avatars/MaironeLorenzo.jpg"
import laba from "./img/avatars/labagnara.jpg"
import osja from "./img/avatars/osja.jpg"

const Sudinoi = () => {
    return (
        <div className="container gioco">
            <Headergioco/>
            <div className="contAbout">
				<div id="about">
					<div id="titoloAbout">IL NOSTRO TEAM</div>
					<div className="contTextAbout">
						<div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={bekim} alt="tony" />
                            </div>
                            <div className="nome">Bekim Sulejman</div>
						</div>
                        <div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={dalbe} alt="lucappe" />
                            </div>
                            <div className="nome">Tommaso Dalbesio</div>
                        </div>
                        <div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={mairone} alt="rerabyte" />
                            </div>
                            <div className="nome">Lorenzo Mairone</div>
                        </div>
						<div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={osja} alt="omarre" />
                            </div>
                            <div className="nome">Ragip Osja</div>
                        </div>
                        <div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={laba} alt="omarre" />
                            </div>
                            <div className="nome">Samuel Labagnara</div>
                        </div>
                        {/* <div className="aboutText">
							<div className="contAvatar">
                                <img className="avatar" src={dalbe} alt="omarre" />
                            </div>
                            <div className="nome">Luigi Zhao</div>
                        </div> */}
					</div>
				</div>
			</div>
			<hr className="sep"/>
			<div id="info">
				<h1>Chi siamo</h1>
				<div>
					Siamo studenti della classe 5C ITIS informatica.
				</div>
			</div>
            <Footergioco nomeGioco="gymmy"/>
        </div>
    )
}

export default Sudinoi