import "./Headergioco.css"

const Headergioco = () => {
  var nomegioco="Indovina Dove Sono"
  var slogan=""
  var link="http://localhost/Scuola/GIOCHI/Indovina_dove_sono/"
  return (
    <header className='head-gioco'>
        <div>
            <h2>{nomegioco}</h2>
            <h3>{slogan}</h3>			
        </div>
        <a href={link} className="button" id="gioca">GIOCA ORA</a>
    </header>
  )
}

export default Headergioco