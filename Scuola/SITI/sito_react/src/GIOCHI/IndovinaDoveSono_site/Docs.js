import './docs.css'
import Headergioco from './componenti_gioco/Headergioco'
import Footergioco from './componenti_gioco/Footergioco'

const Docs = () => {
	function ShowDocs(e) {
		let nome = e.target.value
		let nomeMin = nome.toLowerCase()
		let titolo = document.getElementById("titoloDocs")
		titolo.innerText = nome
		document.querySelectorAll(".docsText").forEach((el) => {
			el.style.display = "none"
		})
		document.querySelector(`div[title='${nomeMin}']`).style.display = "block"
	}
	return (
		<div className="container gioco">
			<Headergioco />
			<div className="contDocs">
				<div className="indice">
					<div id="titoloIndice">INDICE</div>
					<div id="contSezioni">
						<input className="sezione" type="button" value="INSTALLAZIONE" onClick={ShowDocs} />
						<input className="sezione" type="button" value="DOCUMENTAZIONE" onClick={ShowDocs} />
					</div>
				</div>
				<div id="docs">
					<div className="contText">
						<div id="titoloDocs">INSTALLAZIONE</div>
						<div className="docsText" title="installazione">
							Innanzitutto bisogna scaricare il file .zip che conterrà la cartelle ed i file
							necessari per avviare il nostro gioco. Per avviarlo bisognerà aver installato
							Nodejs sul proprio computer.
							Passaggi per verificare se Nodejs e npm sono già installati sul nostro
							dispositivo:<br></br>
							1) Digitare sul terminale nodejs -v, se comparirà la versione come nella
							schermata che segue significa che nodejs è già installato (non è detto che
							sia aggiornato all’ultima versione).<br></br>
							2) Sempre sul terminale digitiamo npm -v e come per nodejs se è già
							installato comparirà la versione (potrebbe non essere aggiornato
							all’ultima versione disponibile).<br></br>
							3) Se sono già presenti basterà entrare nella cartella “gioco” e
							successivamente aprire il terminale e digitare npm start, così aprirà il
							nostro browser con la home del gioco.
							Se nodejs non è installato o aggiornato all’ultima versione, quindi
							quando digitiamo il primo comando sul terminale non compare nessuna
							versione, dobbiamo installare il programma.<br></br>
							Passaggi per installare nodejs e npm su Ubuntu:<br></br>
							1) Aprire il terminale del proprio computer e digitare il seguente comando
							“curl -fsSL https://deb.nodesource.com/setup_19.x | sudo -E bash - &&\
							sudo apt-get install -y nodejs”
							partirà l’installazione, una volta finita avrà installato sia nodejs che l’npm.
							Quindi per giocare al nostro gioco dovrai seguire le istruzione del punto 3
							precedente.
						</div>
						<div className="docsText" title="documentazione">
							<a href="https://gitlab.com/progetti2023/gr03/-/blob/9d01178e29dc3a81ee3b4e45eb85579764b764f1/Indovina_dove_sono/Documentazione/Indovina_dove_sono.pdf">Puoi leggerla cliccando qui</a>
						</div>
					</div>
				</div>
			</div>
			<Footergioco nomeGioco="gymmy" />
		</div>

	)
}

export default Docs