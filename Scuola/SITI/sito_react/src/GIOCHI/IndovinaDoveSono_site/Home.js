import './stile.css'
import Headergioco from './componenti_gioco/Headergioco'
import Footergioco from './componenti_gioco/Footergioco'

const Home = () => {
	return (
		<div className="container gioco">
			<Headergioco />
			<section>
				<h1>COME FUNZIONA</h1>
				<div>
					L’obiettivo del gioco è rispondere in modo corretto a più domande possibili. Per ogni risposta corretta si guadagnerà un punto, mentre per ogni risposta sbagliata se ne perderà uno. Al giocatore verrà mostrata la domanda con un’immagine del luogo o bandiera a cui fa riferimento e le quattro risposte da selezionare. Selezionata la risposta desiderata si dovrà cliccare sul tasto “Avanti” per passare al quesito successivo. Il linguaggio di programmazione utilizzato per sviluppare il gioco è JavaScript, più nello specifico tramite l’utilizzo della libreria ReactJS. Le domande e le risposte verranno visualizzate in modo casuale attraverso dei file JSON.

				</div>
			</section>
			<section>
				<h1>PUNTEGGIO</h1>
				<div>
					Il punteggio di ogni giocatore partirà da 0, man mano che l'utente risponderà giusto alle domande verrà incrementato oppure vicersa nel caso in c	ui le risposte siano errate. Nel caso in cui il punteggio del giocatore fosse 0, ad ogni risposta sbagliata il punteggio non verrà decrementato. Ci saranno dalle 10 alle 20 domande, a seconda della difficoltà. Alla fine del livello comparirà una schermata contenente il riepilogo della partita appena effettuata. Il record del giocatore verrà salvato in una Base Dati per poi essere visualizzato in una classifica generale.
				</div>
			</section>
			<section>
				<h1>REGOLE</h1>
				<div>
					Nella schermata del livello verranno mostrate all’utente:
					<br></br>
					• l’icona “casetta” per fare ritorno alla home<br></br>
					• un rapido epilogo del numero di domande mancanti<br></br>
					• l’immagine della città/monumento/bandiera a cui farà riferimento<br></br>
					la domanda<br></br>
					• il quesito e le quattro risposte possibili da selezionare<br></br>
					• il pulsante “Avanti” per proseguire la partita.<br></br>
					Quando selezioniamo una risposta essa verrà evidenziata cambiando
					colore.
					Non sarà possibile avanzare alla domanda successiva senza aver
					selezionato una risposta.
				</div>
			</section>
			<Footergioco nomeGioco="gymmy" />
		</div>
	)
}

export default Home
