import "./Avatar.css"

const Avatar = (props) => {
    return (
    <div className="avatar flip-card">
        <div className="flip-card-inner">
            <div className="flip-card-front">
                <img src={props.img} alt="Avatar"/>
            </div>
            <div className="flip-card-back">
                <h1>{props.nome}</h1>
            </div>
        </div>
    </div>
    )
}

export default Avatar