import {useEffect,useContext,useState, useLayoutEffect} from "react"
import "./Navbar.css"
import { NavLink,useLocation } from 'react-router-dom';
import {userdata} from "../Main"
//ICONE
import logo from "../img/logowhite.png"
import log from "../img/icons/login.png"
import acc from "../img/icons/avatar.png"
import home from "../img/icons/home.png"
import game from "../img/icons/game.png"
import docs from "../img/icons/docs.png"
import aboutus from "../img/icons/about_us.png"
import map from "../img/icons/map.png"

const Navbar = () => {
	const [accesso, setaccesso] = useState({testo:"Login",img:log,link:"./login"});
	const userdt = useContext(userdata)
	const loc=useLocation()
	const [paginagiochi, setpaginagiochi] = useState({link:""});
	useEffect(() => {
		if(Object.keys(userdt.user).length!==0){ //se loggato
			setaccesso({testo:"Account",img:acc,link:"./account"})
		}else{
			setaccesso({testo:"Login",img:log,link:"./login"})
		}
	}, [userdt]);

	useEffect(() => { //creare dinamicamente i link
		if(loc.pathname.includes("/giochi/")){
			let lk=loc.pathname.split("/")
			lk=`/giochi/${lk[2]}`
			setpaginagiochi({link:lk})
		}else{
			setpaginagiochi({link:""})
		}
	}, [loc]);

	useLayoutEffect(() => { //nascondere link attuale
		let a=getLinks()
		a.forEach((link)=>{
			link.parentElement.style.display="initial"
			if(link.classList.value.includes("active")){
				link.parentElement.style.display="none"
			}
		})
	}, [paginagiochi])

	function getLinks(){
		let a=document.querySelectorAll("nav ul li a")
		return a
	}

    return (
		<nav>
			<NavLink to="/" className="logo-link"><img src={logo} className="logo" alt="logo"/></NavLink>
			<ul>
				<li><NavLink to="./mappa"><img src={map} className="icon" alt="icon"/><span>Mappa</span></NavLink></li>
				<li><NavLink to="./"><img src={home} className="icon" alt="icon"/><span>Home</span></NavLink></li>
				{paginagiochi.link!==""?
				<>
				<li><NavLink to={paginagiochi.link+"/home"}><img src={game} className="icon" alt="icon"/><span>Gioco</span></NavLink></li>
				<li><NavLink to={paginagiochi.link+"/docs"}><img src={docs} className="icon" alt="icon"/><span>Docs</span></NavLink></li>
				<li><NavLink to={paginagiochi.link+"/sudinoi"}><img src={aboutus} className="icon" alt="icon"/><span>Su di noi</span></NavLink></li>
				</>
				:null}
				<li><NavLink to={accesso.link}><img src={accesso.img} className="icon" alt="icon"/><span>{accesso.testo}</span></NavLink></li>
				
			</ul>
		</nav>
    )
}

export default Navbar