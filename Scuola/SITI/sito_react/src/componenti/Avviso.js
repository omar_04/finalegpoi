import "./Avviso.css"

const Avviso = ({titolo,msg}) => {
    return (
        <div className="cont-avviso">
            <div className="avviso">
                <h2>{titolo}</h2>
                <span>{msg}</span>
            </div>      
        </div>
    )
}

export default Avviso