import { createContext,useState,useEffect } from 'react'
import { HashRouter, Route, Routes } from 'react-router-dom';
import Avviso from './componenti/Avviso';
import Navbar from './componenti/Navbar';
import Account from './pagine/Account';
import Login from './pagine/Login';
import Mappa from './pagine/Mappa';
import Signup from './pagine/Signup';
import Home from './pagine/Home';
//GIOCHI

//template
// import HomeGioco from './GIOCHI/template_gioco_web/Home';
// import Sudinoi from './GIOCHI/template_gioco_web/Sudinoi';
// import Docs from './GIOCHI/template_gioco_web/Docs';

// //gymmy
import GymmyHomeGioco from './GIOCHI/Gymmy_site/Home';
import GymmySudinoi from './GIOCHI/Gymmy_site/Sudinoi';
import GymmyDocs from './GIOCHI/Gymmy_site/Docs';

//epic trombone
import EpicTromboneHomeGioco from './GIOCHI/Epic_Trombone_site/Home';
import EpicTromboneSudinoi from './GIOCHI/Epic_Trombone_site/Sudinoi';
import EpicTromboneDocs from './GIOCHI/Epic_Trombone_site/Docs';

//geohoot
import GeohootHomeGioco from './GIOCHI/Geohoot_site/Home';
import GeohootSudinoi from './GIOCHI/Geohoot_site/Sudinoi';
import GeohootDocs from './GIOCHI/Geohoot_site/Docs';

//indovina dove sono
import IndovinaDoveSonoHomeGioco from './GIOCHI/IndovinaDoveSono_site/Home';
import IndovinaDoveSonoSudinoi from './GIOCHI/IndovinaDoveSono_site/Sudinoi';
import IndovinaDoveSonoDocs from './GIOCHI/IndovinaDoveSono_site/Docs';

var giochi=[
	// {nome:"template_gioco_web",comp:[<HomeGioco/>,<Docs/>,<Sudinoi/>]},
	{nome:"gymmy",comp:[<GymmyHomeGioco/>,<GymmyDocs/>,<GymmySudinoi/>]},
	{nome:"epic-trombone",comp:[<EpicTromboneHomeGioco/>,<EpicTromboneDocs/>,<EpicTromboneSudinoi/>]},
	{nome:"geohoot",comp:[<GeohootHomeGioco/>,<GeohootDocs/>,<GeohootSudinoi/>]},
	{nome:"indovina-dove-sono",comp:[<IndovinaDoveSonoHomeGioco/>,<IndovinaDoveSonoDocs/>,<IndovinaDoveSonoSudinoi/>]}
]

export const userdata=createContext()

const Main = () => {
	const [user, setuser] = useState({});
	const [avviso, setavviso] = useState({titolo:"",msg:""});
	useEffect(() => {
        if(localStorage.getItem("user")!==null){
			let user_obj=JSON.parse(localStorage.getItem("user"))
            setuser(user_obj)
			document.cookie = "session_username_GPOI=;expires=" + new Date(0).toUTCString()
            document.cookie = `session_username_GPOI=${user_obj['username']}; path=/;`
        }
    }, [])
	const mostraAvviso=(titolo,msg,tempo)=>{
        setavviso({titolo,msg})
        setTimeout(() => {
            setavviso({titolo:"",msg:""})
        }, tempo);
    }
	return (
		<userdata.Provider value={{user,setuser}} >
			<HashRouter>
				{avviso.titolo!==""?<Avviso titolo={avviso.titolo} msg={avviso.msg}/>:null}
				<Navbar/>
				<div id="super-cont">
					<Routes>
						<Route path="/" element={<Home/>}/>
						<Route path="/login" element={<Login avviso={mostraAvviso}/>}/>
						<Route path="/registrati" element={<Signup avviso={mostraAvviso}/>}/>
						<Route path="/account" element={<Account avviso={mostraAvviso}/>} />
						<Route path="/mappa" element={<Mappa/>} />
						{giochi.map((gioco)=>{
							return <>
								<Route path={`/giochi/${gioco.nome}/home`} element={gioco.comp[0]} />
								<Route path={`/giochi/${gioco.nome}/docs`} element={gioco.comp[1]} />
								<Route path={`/giochi/${gioco.nome}/sudinoi`} element={gioco.comp[2]} />
							</>
						})}
					</Routes>
				</div>
			</HashRouter>
		</userdata.Provider>
	)
}

export default Main