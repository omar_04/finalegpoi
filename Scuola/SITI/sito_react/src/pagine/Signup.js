import { useRef, useState,useContext } from "react"
import { useNavigate,Link } from 'react-router-dom';
import eyehidden from "../img/icons/eyehidden.png"
import eyeshow from "../img/icons/eyeshow.png"
import {server} from "../config"
import {userdata} from "../Main"
import sha256 from 'crypto-js/sha256';

const Signup = ({avviso}) => {
    const userdt = useContext(userdata)
    const nav=useNavigate()
    const [icon, seticon] = useState(eyehidden);
    const nome = useRef(null)
    const cognome = useRef(null)
    const email = useRef(null)
    const user = useRef(null)
    const pwd = useRef(null)
    const mostrapwd=()=>{
        let input=document.querySelector("#password>input")
        if(icon===eyehidden){
            seticon(eyeshow)
            input.type="text"
        }else{
            seticon(eyehidden)
            input.type="password"
        }
    }
    const checkInputs=()=>{
        if(nome.current.value==="" || cognome.current.value==="" || email.current.value==="" || user.current.value==="" || pwd.current.value===""){
            return false
        }else{
            return true
        }
    }
    const getInputs=()=>{
        let formData=new FormData();
        let nm=nome.current.value
        let cgnm=cognome.current.value
        let eml=email.current.value
        let username=user.current.value
        let password=sha256(pwd.current.value).toString()
        formData.append("nome",nm)
        formData.append("cognome",cgnm)
        formData.append("email",eml)
        formData.append("username",username)
        formData.append("password",password)
        return formData
    }
    const registra=()=>{
        let link=server['url']+"/signup.php"
        if(checkInputs()){
            let data=getInputs()
            fetch(link,{
                method:"POST",
                body:data
            })
            .then(response=>response.json())
            .then(registrato=>{
                let dati={
                    nome:nome.current.value,
                    cognome:cognome.current.value,
                    email:email.current.value,
                    username:user.current.value,
                    password:pwd.current.value
                }
                if(registrato){
                    userdt.setuser(dati)
                    localStorage.setItem("user",JSON.stringify(dati))
                    document.cookie = "session_username_GPOI=;expires=" + new Date(0).toUTCString()
                    document.cookie = `session_username_GPOI=${dati.username}; path=/;`
                    avviso("Registrato con successo","L'operazione è andata a buon fine",1500)
                    nav("/")
                }else{
                    avviso("Attenzione","Username già esistente",1500)
                }
            })
        }else{
            avviso("Attenzione","Mancano dei dati!",1500)
        }
    }
    return (
        <div id="signup">
            <h1>Registrati</h1>
            <form>
                <p>Registrati per accedere al mondo di gioco</p>
                <label>Nome:</label>
                <input type="text" name="nome" ref={nome} placeholder="Inserisci il tuo nome"/>
                <label>Cognome:</label>
                <input type="text" name="cognome" ref={cognome} placeholder="Inserisci il tuo cognome"/>
                <label>Email:</label>
                <input type="email" name="email" ref={email} placeholder="Inserisci la tua email"/>
                <label>Username:</label>
                <input type="text" name="username" ref={user} placeholder="Inserisci username"/>
                <label>Password:</label>
                <div id="password">
                    <input type="password" name="password" ref={pwd} placeholder="Inserisci password"/>
                    <img src={icon} alt="icon" onClick={mostrapwd}/>
                </div>
                <input type="button" onClick={registra} value="REGISTRATI"/>
                <div id="noaccount">HAI GIÀ UN ACCOUNT? <Link to="../login">ACCEDI</Link></div>
            </form>
        </div>
    )
}

export default Signup