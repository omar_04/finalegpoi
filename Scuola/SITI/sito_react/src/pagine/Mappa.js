import { useEffect, useState } from "react"
import { useNavigate,Link } from 'react-router-dom';
import barca from '../img/assets/imgGames/barca.png';
import peso from '../img/assets/imgGames/peso.png'
import cabina from '../img/assets/imgGames/cabinaTelefonica.png'
import mappamondo from '../img/assets/imgGames/mappamondo.png'
import tromba from '../img/assets/imgGames/tromba.png'
import domanda from '../img/assets/imgGames/puntoDomanda.png'
import caverna from '../img/assets/imgGames/caverna.png'
import play from '../img/assets/imgGames/play.png'
import info from '../img/assets/imgGames/info.png'

import "./Mappa.css"

const Mappa = () => {

    const [username_storage, setusername_storage] = useState("")

    const nav=useNavigate()
    useEffect(() => {
        if(localStorage.getItem("user")===null){ //controllo se è loggato
            nav("../login")
        } else {
            let usr=JSON.parse(localStorage.getItem("user"))
            setusername_storage(usr['username'])
        }
    }, [])
    return (
        <div>
            <div className="containerMappa">
                <div className="mappa">
                    <div className="giocoMappa barca">
                        <img src={barca} alt="barca" />
                        <span>Alla scoperta dell'Iliade</span>
                        <div className="caption">
                            <a href="#">GIOCA ORA <img className="icon" src={play} alt="play" width="5px"/></a>
                            <Link to="#">INFO <img className="icon" src={info} alt="info"/></Link>
                        </div>
                    </div>
                    <div className="giocoMappa peso">
                        <img src={peso} alt="peso"/>
                        <span>Gymmy</span>
                        <div className="caption">
                            <a href="http://localhost/Scuola/GIOCHI/Gymmy_game">GIOCA ORA <img className="icon" src={play} alt="play" width="5px"/></a>
                            <Link to="/giochi/gymmy/home">INFO <img className="icon" src={info} alt="info"/></Link>
                        </div>
                    </div>
                    <div className="giocoMappa cabina">
                        <img src={cabina} alt="cabinaTelefonica"/>
                        <span>Escapist</span>
                        <div className="caption">
                            <a href="#">GIOCA ORA <img className="icon" src={play} alt="play" width="5px"/></a>
                            <Link to="#">INFO <img className="icon" src={info} alt="info"/></Link>
                        </div>
                    </div>
                    <div className="giocoMappa mappamondo">
                        <img src={mappamondo} alt="mappamondo"/>
                        <span>GeoHoot</span>
                        <div className="caption">
                            <a href="http://localhost/Scuola/GIOCHI/Geohoot">GIOCA ORA <img className="icon" src={play} alt="play" width="5px"/></a>
                            <Link to="/giochi/geohoot/home">INFO <img className="icon" src={info} alt="info"/></Link>
                        </div>
                    </div>
                    <div className="giocoMappa tromba">
                        <img src={tromba} alt="tromba"/>
                        <span>Epic Trombone</span>
                        <div className="caption">
                            <a href={"http://localhost:1729/epicTrombone/?user="+username_storage}>GIOCA ORA <img className="icon" src={play} alt="play" width="5px"/></a>
                            <Link to="/giochi/epic-trombone/home">INFO <img className="icon" src={info} alt="info"/></Link>
                        </div>
                    </div>
                    <div className="giocoMappa domanda">
                        <img src={domanda} alt="puntoDomanda"/>
                        <span>Indovina dove sono</span>
                        <div className="caption">
                            <a href="http://localhost/Scuola/GIOCHI/Indovina_dove_sono">GIOCA ORA <img className="icon" src={play} alt="play" width="5px"/></a>
                            <Link to="/giochi/indovina-dove-sono/home">INFO <img className="icon" src={info} alt="info"/></Link>
                        </div>
                    </div>
                    <div className="giocoMappa caverna">
                        <img src={caverna} alt="caverna"/>
                        <span>CreepyMath</span>
                        <div className="caption">
                            <Link to="#">GIOCA ORA <img className="icon" src={play} alt="play" width="5px"/></Link>
                            <Link to="#">INFO <img className="icon" src={info} alt="info"/></Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Mappa
