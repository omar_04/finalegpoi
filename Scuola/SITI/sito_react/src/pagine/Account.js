import {useEffect,useContext, useState} from "react"
import { useNavigate } from 'react-router-dom';
import {userdata} from "../Main"
import {server} from "../config"
import "./Account.css"

const Account = () => {
    const nav=useNavigate()
    const userdt = useContext(userdata)
    const [stats, setstats] = useState([]);
    useEffect(() => {
        if(Object.keys(userdt.user).length===0){
            nav("/login")
        }
        get_stats()
    }, [userdt]);
    const logout=()=>{
        userdt.setuser({})
        localStorage.removeItem("user")
        document.cookie = "session_username_GPOI=;expires=" + new Date(0).toUTCString()
        nav("/login")
    }
    const elimina=()=>{
        let vuoleCancellare = window.confirm("Sei sicuro di voler eliminare il tuo account?")
        if(vuoleCancellare){
            let link=server['url']+"/elimina_utente.php"
            let data=new FormData()
            data.append("username",userdt.user.username)
            fetch(link,{
                method:"POST",
                body:data
            })
            .then(response=>response.json())
            .then(eliminato=>{
                if(eliminato){
                    userdt.setuser({})
                    localStorage.removeItem("user")
                    nav("../login")
                }
            })
        }
    }
    const get_stats=()=>{
        let link=server['url']+"/get_stats.php"
        let data=new FormData()
        data.append("user",userdt.user.username)
        fetch(link,{
            method:"POST",
            body:data
        })
        .then(response=>response.json())
        .then(statistiche=>{
            // console.log(statistiche)
            if(statistiche.nomegioco!==null || statistiche===false){
                setstats([
                    <div key="st" style={{textAlign:"center"}}>
                        <h2 style={{fontWeight:"400"}}>Nome del gioco con il punteggio più alto:</h2>
                        <h3>{statistiche.nomegioco}</h3>
                        <h2 style={{fontWeight:"400"}}>Punteggio:</h2>
                        <h3>{statistiche.punteggio}</h3>
                    </div>
                ])
            }
        })
    }
    return (
        <div id="account">
            <h1>Il tuo account</h1>
            <div className="cont-dati">
                <div className="dati">
                    <label>Nome:</label>
                    <span>{userdt.user['nome']}</span>
                </div>
                <div className="dati">
                    <label>Cognome:</label>
                    <span>{userdt.user['cognome']}</span>
                </div>
                <div className="dati">
                    <label>Username:</label>
                    <span>{userdt.user['username']}</span>
                </div>
                <div className="dati">
                    <label>Email:</label>
                    <span>{userdt.user['email']}</span>
                </div>
            </div>
            <h1>Le tue statistiche</h1>
            <div className="cont-dati">
                {stats.length===0?<h2 style={{textAlign:"center"}}>Non hai ancora giocato!</h2>:stats}
            </div>
            <input type="button" value="Esci" onClick={logout}/>
            <input type="button" style={{backgroundColor:"red",width:"300px",marginTop:"50px"}} value="Elimina account" onClick={elimina}/>
        </div>
    )
}

export default Account