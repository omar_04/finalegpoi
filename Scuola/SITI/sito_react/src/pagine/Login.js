import { useEffect, useRef, useState,useContext } from "react"
import { useNavigate,Link } from 'react-router-dom';
import eyehidden from "../img/icons/eyehidden.png"
import eyeshow from "../img/icons/eyeshow.png"
import {server} from "../config"
import {userdata} from "../Main"
import sha256 from 'crypto-js/sha256';

const Login = ({avviso}) => {
    const userdt = useContext(userdata)
    const nav=useNavigate()
    const [icon, seticon] = useState(eyehidden);
    useEffect(() => {
        if(localStorage.getItem("user")!==null){
            nav("../account")
        }
    }, [])
    const user = useRef(null)
    const pwd = useRef(null)
    const mostrapwd=()=>{
        let input=document.querySelector("#password>input")
        if(icon===eyehidden){
            seticon(eyeshow)
            input.type="text"
        }else{
            seticon(eyehidden)
            input.type="password"
        }
    }
    const checkInputs=()=>{
        if(user.current.value==="" || pwd.current.value===""){
            return false
        }else{
            return true
        }
    }
    const getInputs=()=>{
        let formData=new FormData();
        let username=user.current.value
        let password=sha256(pwd.current.value).toString()
        formData.append("username",username)
        formData.append("password",password)
        return formData
    }
    const accedi=()=>{
        let link=server['url']+"/login.php"
        if(checkInputs()){
            let data=getInputs()
            fetch(link,{
                method:"POST",
                body:data
            })
            .then(response=>response.json())
            .then(loggato=>{
                if(loggato===false){
                    avviso("Attenzione","Credenziali errate!",1500)
                }else{
                    let dati=loggato[0]
                    localStorage.setItem("user",JSON.stringify(dati))
                    let user=dati.username
                    document.cookie = "session_username_GPOI=;expires=" + new Date(0).toUTCString()
                    document.cookie = `session_username_GPOI=${user}; path=/;`
                    userdt.setuser(dati)
                    nav("/mappa")
                }
            })
        }else{
            avviso("Attenzione","Mancano dei dati!",1500)
        }
    }
    return (
        <div id="login">
            <h1>Effettua l'accesso</h1>
            <form>
                <p>Inserisci le credenziali per accedere al mondo di gioco</p>
                <label>Username:</label>
                <input type="text" name="username" ref={user} placeholder="Inserisci username"/>
                <label>Password:</label>
                <div id="password">
                    <input type="password" name="password" ref={pwd} placeholder="Inserisci password"/>
                    <img src={icon} alt="icon" onClick={mostrapwd}/>
                </div>
                <input type="button" onClick={accedi} value="ACCEDI"/>
                <div id="noaccount">NON HAI UN ACCOUNT? <Link to="../registrati">REGISTRATI</Link></div>
            </form>
        </div>
    )
}

export default Login
