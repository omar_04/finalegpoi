import { Link } from 'react-router-dom';
import "./Home.css"
import logo from "../img/logowhite.png"
import Avatar from '../componenti/Avatar';
//INIZIO AVATAR
import maikel from "../img/avatars/agbo.jpg"
import castaldo from "../img/avatars/Tony.jpeg"
import capellino from "../img/avatars/Ciaplin.jpeg"
import ciuro from "../img/avatars/Ciuraru.jpg"
import frailone from "../img/avatars/Omarre.jpeg"
import rera from "../img/avatars/rera.jpeg"
import dinu from "../img/avatars/DinuTeodorDaniel.jpg"
import xu from "../img/avatars/XuXuFan.jpg"
import mellano from "../img/avatars/Michael.jpeg"
import osja from "../img/avatars/osja.jpg"
import mairone from "../img/avatars/MaironeLorenzo.jpg"
import salvai from "../img/avatars/salvo.jpg"
import laba from "../img/avatars/labagnara.jpg"
import alex from "../img/avatars/Cristini.jpg"
import dalbe from "../img/avatars/dalbesioAvatar.JPG"
import giorgio from "../img/avatars/ForneroGiorgio.jpg"
import paolo from "../img/avatars/GiolittiPaolo.jpg"
import massa from "../img/avatars/MassanoAndrea.jpg"
import bek from "../img/avatars/SulejmaniBekim.png"
import zanche from "../img/avatars/Zanche.jpg"
// FINE AVATAR

const Home = () => {
    return (
        <div id="homepage">
            <header>
                <div>
                    <img src={logo} className="logo" alt="logo"/>
                    <h3>Impara giocando</h3>			
                </div>
                <Link to="/login" className="button" id="gioca">Accedi</Link>
            </header>
            <div id="contenitori">
                <div className="cont">
                    <h2>La sfida</h2>
                    <p>La sfida era creare una piattaforma online per dei ragazzi delle scuole medie, in modo da farli divertire giocando, ma facendogli comunque imparare qualcosa, ci siamo riusciti? sarete voi a dirlo!</p>
                </div>
                <div className="cont">
                    <h2 style={{color:"black",textAlign:"center"}}>IL NOSTRO TEAM</h2>
                </div>
                <div className="cont avatars">
                    <Avatar img={castaldo} nome="Antonio Castaldo"/>
                    <Avatar img={capellino} nome="Luca Capellino"/>
                    <Avatar img={frailone} nome="Omar Frailone"/>
                    <Avatar img={rera} nome="Alessio Rera"/>
                    <Avatar img={dinu} nome="Dinu Teodor Daniel"/>
                    <Avatar img={xu} nome="Xu Xu Fan"/>
                    <Avatar img={alex} nome="Alex Cristini"/>
                    <Avatar img={mellano} nome="Michael Mellano"/>
                    <Avatar img={mairone} nome="Lorenzo Mairone"/>
                    <Avatar img={dalbe} nome="Tommaso Dalbesio"/>
                    <Avatar img={osja} nome="Osja Ragip"/>
                    <Avatar img={giorgio} nome="Giorgio Fornero"/>
                    <Avatar img={paolo} nome="Paolo Giolitti"/>
                    <Avatar img={massa} nome="Andrea Massano"/>
                    <Avatar img={salvai} nome="Simone Salvai"/>
                    <Avatar img={bek} nome="Bekim Sulejmani"/>
                    <Avatar img={ciuro} nome="Marco Ciuraru"/>
                    <Avatar img={laba} nome="Samuel Labagnara"/>
                    <Avatar img={maikel} nome="Maikel Agban"/>
                    <Avatar img={zanche} nome="Alessandro Zanchettin"/>
                </div>
            </div>
        </div>
    )
}

export default Home