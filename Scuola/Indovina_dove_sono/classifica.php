<?php
    require("./config.php");
    global $conn;
    $query = "SELECT * FROM gr3_utenti ORDER BY migliorPunteggio DESC";
    $result = $conn->query($query);
    if($result->num_rows == 0){
        echo json_encode(false);
    }else{
        $array = $result->fetch_all(MYSQLI_ASSOC);
        echo json_encode($array);
    }
?>
