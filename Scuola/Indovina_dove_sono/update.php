<?php
	require("./config.php");
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        update($_POST['username'], $_POST['punteggio']);
    }

	function update($username, $punteggio){
		global $conn;

		$query = "SELECT * FROM gr3_utenti WHERE fk_username=?";
		$stmt = $conn->prepare($query);
		if(!$stmt){
			die("Preparazione query fallita: ".$conn->error);
		}
		$stmt->bind_param("s", $username);
		$stmt->execute();
		$result = $stmt->get_result();
		$array = $result->fetch_all(MYSQLI_ASSOC);

		if($result->num_rows == 0){
			echo json_encode(false);
		}else{
			echo json_encode(true);
		}

		$query2 = "UPDATE gr3_utenti SET ultimoPunteggio=? WHERE fk_username=?";
		$stmt2 = $conn->prepare($query2);
		if(!$stmt2){
			die("Preparazione query fallita: ".$conn->error);
		}
		$stmt2->bind_param("is", $punteggio, $username);
		$stmt2->execute();

		if($punteggio > $array[0]["migliorPunteggio"]){
			$query3 = "UPDATE gr3_utenti SET migliorPunteggio=? WHERE fk_username=?";
			$stmt3 = $conn->prepare($query3);
			if(!$stmt3){
				die("Preparazione query fallita: ".$conn->error);
			}
			$stmt3->bind_param("is", $punteggio, $username);
			$stmt3->execute();
		}

	}

?>
