import React from 'react'
import './Alert.css'

const Alert = (props) => {
	return (
		<div className='alert' style={{backgroundColor: props.colore}}>
			<div>
				<img src={props.icon} alt='icon' className='iconAlert'/>
				<h2>{props.messaggio}</h2>
			</div>
		</div>
	)
}

export default Alert