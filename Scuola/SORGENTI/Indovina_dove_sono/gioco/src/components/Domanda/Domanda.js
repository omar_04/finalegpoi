import React from 'react'
import './Domanda.css'

const Domanda = (props) => {
	return (
		<div className='dom-container'>
			<img src={props.img} alt='Immagine'/>
			<h1>{props.domanda}</h1>
		</div>
	)
}

export default Domanda