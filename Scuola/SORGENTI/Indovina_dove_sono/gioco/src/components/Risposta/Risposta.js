import React, { useEffect} from 'react'
import './Risposta.css'

const Risposta = (props) => {
	var vecchioSel=document.querySelector(".selez")
	useEffect(() => {
		if(vecchioSel!==null){
			vecchioSel.classList.remove("selez") //rimuovo vecchio selezionato se esiste
		}
		// eslint-disable-next-line
	}, []);
	function funz(e){
		vecchioSel=document.querySelector(".selez")
		if(vecchioSel!==null){
			vecchioSel.classList.remove("selez")
		}
		e.target.classList.add("selez")
		props.funzione()
	}
	return (
		<div className='risp-container' tabIndex={-1} onClick={funz}>
			<h1>{props.risposta}</h1>
		</div>
	)
}

export default Risposta