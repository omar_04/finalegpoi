import React from 'react'
import iconHome from '../../img/home.png'
import set from '../../img/settings.png'
import trofeo from '../../img/trophy.png'
import user from '../../img/user.png'
import './NavBar.css'
import { useNavigate } from 'react-router-dom'

const NavBar = () => {
	const navigate = useNavigate()
	return (
		<div className='NavBar'>
			<div className='icon' onClick={() => navigate("/home")}>
				<img id="Home" src={iconHome} alt='Home'/>
			</div>
			<div className="icon">
				<img id="trofeoicon" src={trofeo} alt='Classifica' onClick={()=>navigate("/classifica")}/>
			</div>
			<div className="icon" onClick={() => navigate("/profilo")}>
				<img id="setting" src={user} alt='Impostazioni'/>
			</div>
			<div className="icon" onClick={() => navigate("/impostazioni")}>
				<img id="setting" src={set} alt='Impostazioni'/>
			</div>
		</div>
	)
}

export default NavBar
