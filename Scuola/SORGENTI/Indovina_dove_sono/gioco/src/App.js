import React from 'react'
import { HashRouter as Router, Routes, Route } from 'react-router-dom'
import Home from './pages/Home/Home'
import Impostazioni from './pages/Impostazioni/Impostazioni'
import Livello from './pages/Livello/Livello'
import Classifica from './pages/Classifica/Classifica'
import Profilo from './pages/Profilo/Profilo'
import Login from './pages/Login/Login'

const App = () => {
	if((sessionStorage.getItem("scelto") !== "Medio" && sessionStorage.getItem("scelto") !== "Difficile") || sessionStorage.getItem("scelto") === undefined){
		sessionStorage.setItem("scelto", "Facile")
		sessionStorage.setItem("numDomande", 10)
	}

return (
		<Router>
			<Routes>
				<Route path='/' element={<Login/>}/>
				<Route path='/home' element={<Home/>}/>
				<Route path='/livello' element={<Livello/>}/>
				<Route path='/impostazioni' element={<Impostazioni/>}/>
				<Route path='/classifica' element={<Classifica/>} />
				<Route path='/profilo' element={<Profilo/>} />
			</Routes>
		</Router>
	)
}

export default App
