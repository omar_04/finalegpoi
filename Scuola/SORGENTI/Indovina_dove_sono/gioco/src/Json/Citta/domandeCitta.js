export const domandeJsonC = [
	{
		immagine:require('./img_citta/atene.jpg'),
		domanda:"Dove siamo?",
		giusta:"Atene",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/pisa.jpg'),
		domanda:"Dove siamo?",
		giusta:"Pisa",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/versailles.jpg'),
		domanda:"Dove siamo?",
		giusta:"Versailles",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/berlino.jpg'),
		domanda:"Qual è la capitale della Germania?",
		giusta:"Berlino",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/barcellona.jpg'),
		domanda:"Dove siamo?",
		giusta:"Barcellona",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/bruxelles.jpg'),
		domanda:"Dove siamo?",
		giusta:"Bruxelles",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/roma_vaticano.jpg'),
		domanda:"Dove siamo?",
		giusta:"Citta del Vaticano",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/roma_pantheon.jpg'),
		domanda:"Dove siamo?",
		giusta:"Roma",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/kiev.jpg'),
		domanda:"Qual è la capitale dell'Ucraina?",
		giusta:"Kiev",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/venezia.jpg'),
		domanda:"Dove siamo?",
		giusta:"Venezia",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/londra_bigben.jpg'),
		domanda:"Dove siamo?",
		giusta:"Londra",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/parigi.jpg'),
		domanda:"Dove siamo?",
		giusta:"Parigi",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/sarajevo.jpg'),
		domanda:"Qual è la capitale della Bosnia?",
		giusta:"Sarajevo",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/oslo.jpg'),
		domanda:"Qual è la capitale della Norvegia?",
		giusta:"Oslo",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/york.jpg'),
		domanda:"Dove siamo?",
		giusta:"York",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/tirana.jpg'),
		domanda:"Dove siamo?",
		giusta:"Tirana",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/amsterdam.jpg'),
		domanda:"Dove siamo?",
		giusta:"Amsterdam",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/birmingham.jpg'),
		domanda:"Dove siamo?",
		giusta:"Birmingham",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/lisbona.jpg'),
		domanda:"Qual è la capitale del Portogallo?",
		giusta:"Lisbona",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/madrid.jpg'),
		domanda:"Dove siamo?",
		giusta:"Madrid",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/milano.jpg'),
		domanda:"Dove siamo?",
		giusta:"Milano",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/monaco.jpg'),
		domanda:"Dove siamo?",
		giusta:"Monaco",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/montpellier.jpg'),
		domanda:"Dove siamo?",
		giusta:"Montpellier",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/napoli.jpg'),
		domanda:"Dove siamo?",
		giusta:"Napoli",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/torino.jpg'),
		domanda:"Dove siamo?",
		giusta:"Torino",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/amburgo.jpg'),
		domanda:"Dove siamo?",
		giusta:"Amburgo",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/belgrado.jpg'),
		domanda:"Dove siamo?",
		giusta:"Belgrado",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/bilbao.jpg'),
		domanda:"Dove siamo?",
		giusta:"Bilbao",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/bruges.jpg'),
		domanda:"Dove siamo?",
		giusta:"Bruges",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/budapest.jpg'),
		domanda:"Dove siamo?",
		giusta:"Budapest",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/colonia.jpg'),
		domanda:"Dove siamo?",
		giusta:"Colonia",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/copenaghen.jpg'),
		domanda:"Dove siamo?",
		giusta:"Copenaghen",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/dublino.jpg'),
		domanda:"Dove siamo?",
		giusta:"Dublino",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/firenze.jpg'),
		domanda:"Dove siamo?",
		giusta:"Firenze",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/granada.jpg'),
		domanda:"Dove siamo?",
		giusta:"Granada",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/lione.jpg'),
		domanda:"Dove siamo?",
		giusta:"Lione",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/liverpool.jpg'),
		domanda:"Dove siamo?",
		giusta:"Liverpool",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/manchester.jpg'),
		domanda:"Dove siamo?",
		giusta:"Manchester",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/marseille.jpg'),
		domanda:"Dove siamo?",
		giusta:"Marsiglia",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/montecarlo.jpg'),
		domanda:"Dove siamo?",
		giusta:"Montecarlo",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/mosca.jpg'),
		domanda:"Dove siamo?",
		giusta:"Mosca",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/nizza.jpg'),
		domanda:"Dove siamo?",
		giusta:"Nizza",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/porto.jpg'),
		domanda:"Dove siamo?",
		giusta:"Porto",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/praga.jpg'),
		domanda:"Dove siamo?",
		giusta:"Praga",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/rennes.jpg'),
		domanda:"Dove siamo?",
		giusta:"Rennes",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/salisburgo.jpg'),
		domanda:"Dove siamo?",
		giusta:"Salisburgo",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/scutari.jpg'),
		domanda:"Dove siamo?",
		giusta:"Scutari",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/siviglia.jpg'),
		domanda:"Dove siamo?",
		giusta:"Siviglia",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/sofia.jpg'),
		domanda:"Dove siamo?",
		giusta:"Sofia",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/stoccolma.jpg'),
		domanda:"Dove siamo?",
		giusta:"Stoccolma",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/valencia.jpg'),
		domanda:"Dove siamo?",
		giusta:"Valencia",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/varsavia.jpg'),
		domanda:"Dove siamo?",
		giusta:"Varsavia",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/vienna.jpg'),
		domanda:"Dove siamo?",
		giusta:"Vienna",
		tipo: "citta"
	},
	{
		immagine:require('./img_citta/zagabria.jpg'),
		domanda:"Dove siamo?",
		giusta:"Zagabria",
		tipo: "citta"
	}
]
