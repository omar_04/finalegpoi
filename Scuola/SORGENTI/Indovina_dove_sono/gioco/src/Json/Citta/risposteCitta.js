export const risposteJsonC = [
	{
		risposta: "Londra",
		tipo: "citta"
	},
	{
		risposta: "Parigi",
		tipo: "citta"
	},
	{
		risposta: "Berlino",
		tipo: "citta"
	},
	{
		risposta: "Madrid",
		tipo: "citta"
	},
	{
		risposta: "Roma",
		tipo: "citta"
	},
	{
		risposta: "Kiev",
		tipo: "citta"
	},
	{
		risposta: "Budapest",
		tipo: "citta"
	},
	{
		risposta: "Varsavia",
		tipo: "citta"
	},
	{
		risposta: "Vienna",
		tipo: "citta"
	},
	{
		risposta: "Napoli",
		tipo: "citta"
	},
	{
		risposta: "Torino",
		tipo: "citta"
	},
	{
		risposta: "Milano",
		tipo: "citta"
	},
	{
		risposta: "Marsiglia",
		tipo: "citta"
	},
	{
		risposta: "Valencia",
		tipo: "citta"
	},
	{
		risposta: "Siviglia",
		tipo: "citta"
	},
	{
		risposta: "Amsterdam",
		tipo: "citta"
	},
	{
		risposta: "York",
		tipo: "citta"
	},
	{
		risposta: "Manchester",
		tipo: "citta"
	},
	{
		risposta: "Lisbona",
		tipo: "citta"
	},
	{
		risposta: "Sofia",
		tipo: "citta"
	},
	{
		risposta: "Tirana",
		tipo: "citta"
	},
	{
		risposta: "Scutari",
		tipo: "citta"
	},
	{
		risposta: "Bruges",
		tipo: "citta"
	},
	{
		risposta: "Copenaghen",
		tipo: "citta"
	},
	{
		risposta: "Praga",
		tipo: "citta"
	},
	{
		risposta: "Atene",
		tipo: "citta"
	},
	{
		risposta: "Bruxelles",
		tipo: "citta"
	},
	{
		risposta: "Stoccolma",
		tipo: "citta"
	},
	{
		risposta: "Sarajevo",
		tipo: "citta"
	},
	{
		risposta: "Zagabria",
		tipo: "citta"
	},
	{
		risposta: "Monaco",
		tipo: "citta"
	},
	{
		risposta: "Belgrado",
		tipo: "citta"
	},
	{
		risposta: "Porto",
		tipo: "citta"
	},
	{
		risposta: "Nizza",
		tipo: "citta"
	},
	{
		risposta: "Salisburgo",
		tipo: "citta"
	},
	{
		risposta: "Firenze",
		tipo: "citta"
	},
	{
		risposta: "Mosca",
		tipo: "citta"
	},
	{
		risposta: "Dublino",
		tipo: "citta"
	},
	{
		risposta: "Lione",
		tipo: "citta"
	},
	{
		risposta: "Montpellier",
		tipo: "citta"
	},
	{
		risposta: "Rennes",
		tipo: "citta"
	},
	{
		risposta: "Granada",
		tipo: "citta"
	},
	{
		risposta: "Bilbao",
		tipo: "citta"
	},
	{
		risposta: "Colonia",
		tipo: "citta"
	},
	{
		risposta: "Amburgo",
		tipo: "citta"
	},
	{
		risposta: "Liverpool",
		tipo: "citta"
	},
	{
		risposta: "Birmingham",
		tipo: "citta"
	},
	{
		risposta: "Montecarlo",
		tipo: "citta"
	},
	{
		risposta: "Palermo",
		tipo: "citta"
	},
	{
		risposta: "Zurigo",
		tipo: "citta"
	},
	{
		risposta: "Champagne",
		tipo: "citta"
	},
	{
		risposta: "San Pietroburgo",
		tipo: "citta"
	},
	{
		risposta: "Cattaro",
		tipo: "citta"
	},
	{
		risposta: "Prevesa",
		tipo: "citta"
	},
	{
		risposta: "Zante",
		tipo: "citta"
	},
	{
		risposta: "Corfù",
		tipo: "citta"
	},
	{
		risposta: "Osijek",
		tipo: "citta"
	},
	{
		risposta: "Trondheim",
		tipo: "citta"
	},
	{
		risposta: "Salamanca",
		tipo: "citta"
	},
	{
		risposta: "Lucerna",
		tipo: "citta"
	},
	{
		risposta: "Cracovia",
		tipo: "citta"
	},
	{
		risposta: "Valletta",
		tipo: "citta"
	},
	{
		risposta: "Brighton",
		tipo: "citta"
	},
	{
		risposta: "Alacati",
		tipo: "citta"
	},
	{
		risposta: "Tromsø",
		tipo: "citta"
	},
	{
		risposta: "Heidelberg",
		tipo: "citta"
	},
	{
		risposta: "Bucarest",
		tipo: "citta"
	},
	{
		risposta: "Edimburgo",
		tipo: "citta"
	},
	{
		risposta: "Mykonos",
		tipo: "citta"
	},
	{
		risposta: "Ibiza",
		tipo: "citta"
	},
	{
		risposta: "Minsk",
		tipo: "citta"
	},
	{
		risposta: "Tallinn",
		tipo: "citta"
	},
	{
		risposta: "Vilnius",
		tipo: "citta"
	},
	{
		risposta: "Skopje",
		tipo: "citta"
	},
	{
		risposta: "Chisinau",
		tipo: "citta"
	},
	{
		risposta: "San Marino",
		tipo: "citta"
	},
	{
		risposta: "Lubiana",
		tipo: "citta"
	},
	{
		risposta: "Berna",
		tipo: "citta"
	}
]
