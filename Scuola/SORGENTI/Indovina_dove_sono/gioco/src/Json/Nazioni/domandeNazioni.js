export const domandeJsonN = [
    {
        immagine:require("./img_nazioni/armenia.png"),
        domanda:"Che nazione è?",
        giusta:"Armenia",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/austria.png"),
        domanda:"Che nazione è?",
        giusta:"Austria",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/belgio.png"),
        domanda:"Che nazione è?",
        giusta:"Belgio",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/bulgaria.png"),
        domanda:"Che nazione è?",
        giusta:"Bulgaria",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/cipro.png"),
        domanda:"Che nazione è?",
        giusta:"Cipro",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/croazia.png"),
        domanda:"Che nazione è?",
        giusta:"Croazia",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/finlandia.png"),
        domanda:"Che nazione è?",
        giusta:"Finlandia",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/francia.png"),
        domanda:"Che nazione è?",
        giusta:"Francia",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/germania.png"),
        domanda:"Che nazione è?",
        giusta:"Germania",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/grecia.png"),
        domanda:"Che nazione è?",
        giusta:"Grecia",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/inghilterra.png"),
        domanda:"Che nazione è?",
        giusta:"Inghilterra",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/italia.png"),
        domanda:"Che nazione è?",
        giusta:"Italia",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/lettonia.png"),
        domanda:"Che nazione è?",
        giusta:"Lettonia",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/lussemburgo.png"),
        domanda:"Che nazione è?",
        giusta:"Lussemburgo",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/norvegia.png"),
        domanda:"Che nazione è?",
        giusta:"Norvegia",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/olanda.png"),
        domanda:"Che nazione è?",
        giusta:"Olanda",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/polonia.png"),
        domanda:"Che nazione è?",
        giusta:"Polonia",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/portogallo.png"),
        domanda:"Che nazione è?",
        giusta:"Portogallo",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/regno_unito.png"),
        domanda:"Che nazione è?",
        giusta:"Regno Unito",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/repubblica_cieca.png"),
        domanda:"Che nazione è?",
        giusta:"Repubblica Ceca",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/romania.png"),
        domanda:"Che nazione è?",
        giusta:"Romania",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/scozia.png"),
        domanda:"Che nazione è?",
        giusta:"Scozia",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/slovacchia.png"),
        domanda:"Che nazione è?",
        giusta:"Slovacchia",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/slovenia.png"),
        domanda:"Che nazione è?",
        giusta:"Slovenia",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/spagna.png"),
        domanda:"Che nazione è?",
        giusta:"Spagna",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/svezia.png"),
        domanda:"Che nazione è?",
        giusta:"Svezia",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/svizzera.png"),
        domanda:"Che nazione è?",
        giusta:"Svizzera",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/turchia.png"),
        domanda:"Che nazione è?",
        giusta:"Turchia",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/ucraina.png"),
        domanda:"Che nazione è?",
        giusta:"Ucraina",
		tipo: "nazione"
    },
    {
        immagine:require("./img_nazioni/albania.png"),
        domanda:"Che nazione è?",
        giusta:"Albania",
		tipo: "nazione"
    }
]