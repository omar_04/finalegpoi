export const risposteJsonN = [
    {
        risposta:"Italia",
		tipo: "nazione"
    },
    {
        risposta:"Ungheria",
		tipo: "nazione"
    },
    {
        risposta:"Ucraina",
		tipo: "nazione"
    },
    {
        risposta:"Turchia",
		tipo: "nazione"
    },
    {
        risposta:"Svizzera",
		tipo: "nazione"
    },
    {
        risposta:"Svezia",
		tipo: "nazione"
    },
    {
        risposta:"Spagna",
		tipo: "nazione"
    },
    {
        risposta:"Slovenia",
		tipo: "nazione"
    },
    {
        risposta:"Slovacchia",
		tipo: "nazione"
    },
    {
        risposta:"Serbia",
		tipo: "nazione"
    },
    {
        risposta:"San Marino",
		tipo: "nazione"
    },
    {
        risposta:"Regno Unito",
		tipo: "nazione"
    },
    {
        risposta:"Romania",
		tipo: "nazione"
    },
    {
        risposta:"Portogallo",
		tipo: "nazione"
    },
    {
        risposta:"Polonia",
		tipo: "nazione"
    },
    {
        risposta:"Norvegia",
		tipo: "nazione"
    },
    {
        risposta:"Macedonia del Nord",
		tipo: "nazione"
    },
    {
        risposta:"Montenegro",
		tipo: "nazione"
    },
    {
        risposta:"Monaco",
		tipo: "nazione"
    },
    {
        risposta:"Repubblica di Moldova",
		tipo: "nazione"
    },
    {
        risposta:"Malta",
		tipo: "nazione"
    },
    {
        risposta:"Lussemburgo",
		tipo: "nazione"
    },
    {
        risposta:"Lituania",
		tipo: "nazione"
    },
    {
        risposta:"Liechtenstein",
		tipo: "nazione"
    },
    {
        risposta:"Lettonia",
		tipo: "nazione"
    },
    {
        risposta:"Irlanda",
		tipo: "nazione"
    },
    {
        risposta:"Islanda",
		tipo: "nazione"
    },
    {
        risposta:"Grecia",
		tipo: "nazione"
    },
    {
        risposta:"Germania",
		tipo: "nazione"
    },
    {
        risposta:"Georgia",
		tipo: "nazione"
    },
    {
        risposta:"Francia",
		tipo: "nazione"
    },
    {
        risposta:"Finlandia",
		tipo: "nazione"
    },
    {
        risposta:"Estonia",
		tipo: "nazione"
    },
    {
        risposta:"Danimarca",
		tipo: "nazione"
    },
    {
        risposta:"Repubblica Ceca",
		tipo: "nazione"
    },
    {
        risposta:"Cipro",
		tipo: "nazione"
    },
    {
        risposta:"Croazia",
		tipo: "nazione"
    },
    {
        risposta:"Bulgaria",
		tipo: "nazione"
    },
    {
        risposta:"Bosnia-Erzegovina",
		tipo: "nazione"
    },
    {
        risposta:"Belgio",
		tipo: "nazione"
    },
    {
        risposta:"Azerbaigian",
		tipo: "nazione"
    },
    {
        risposta:"Austria",
		tipo: "nazione"
    },
    {
        risposta:"Armenia",
		tipo: "nazione"
    },
    {
        risposta:"Andorra",
		tipo: "nazione"
    },
    {
        risposta:"Albania",
		tipo: "nazione"
    }
]