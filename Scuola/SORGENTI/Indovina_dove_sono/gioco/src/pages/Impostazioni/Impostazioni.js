import React, { useEffect } from 'react'
import './Impostazioni.css'
import NavBar from '../../components/NavBar/NavBar'
import noTime from '../../img/no-alarm.png'
import timeOn from '../../img/clock.png'
import { useNavigate } from 'react-router-dom'

const Impostazioni = () => {
	document.title = "gr03 | Impostazioni"
	const navigate = useNavigate()

	useEffect(() => {
		if(sessionStorage.getItem("logged") !== "true"){
			navigate('/')
		}

		switch(sessionStorage.getItem("scelto")){
			case "Facile":
				document.querySelector("#Facile").classList.add("selez")
				break;
			case "Medio":
				document.querySelector("#Medio").classList.add("selez")
				break;
			case "Difficile":
				document.querySelector("#Difficile").classList.add("selez")
				document.querySelector(".toggle-checkbox").checked = true
				document.querySelector(".toggle-checkbox").disabled = true
				break;
			default:
				document.querySelector("#Facile").classList.add("selez")
				break;
		}
		if(sessionStorage.getItem("timer") === "true"){
			document.querySelector(".toggle-checkbox").checked = true
			document.getElementById("noTime").style.display='none'
			document.getElementById("timeOn").style.display='block'
			ChangeImage()
		}
		// eslint-disable-next-line
	}, []);

	const funzioneButton=(e)=>{
		var vecchioSel=document.querySelector(".selez")
		if(vecchioSel!==null){
			vecchioSel.classList.remove("selez")
		}
		e.target.classList.add("selez")

		sessionStorage.setItem("scelto", e.target.id)
		document.querySelector(".toggle-checkbox").disabled = false
		document.querySelector(".toggle-checkbox").checked = false
		document.getElementById("noTime").style.display='block'
		document.getElementById("timeOn").style.display='none'
		sessionStorage.setItem("timer", false)

		switch(sessionStorage.getItem("scelto")){
			case "Facile":
				sessionStorage.setItem("numDomande", 10)
				break;
			case "Medio":
				sessionStorage.setItem("numDomande", 15)
				break;
			default:
				sessionStorage.setItem("numDomande", 10)
				break;
		}

		if(sessionStorage.getItem("scelto") === "Difficile"){
			document.querySelector(".toggle-checkbox").checked = true
			document.querySelector(".toggle-checkbox").disabled = true
			document.getElementById("noTime").style.display='none'
			document.getElementById("timeOn").style.display='block'
			sessionStorage.setItem("timer", true)
			sessionStorage.setItem("seconds", 7)
			sessionStorage.setItem("numDomande", 20)
		}
	}

	const ChangeImage=()=>{
		if(document.querySelector(".toggle-checkbox").checked){

			switch(sessionStorage.getItem("scelto")){
				case "Facile":
					sessionStorage.setItem("timer", true)
					sessionStorage.setItem("seconds", 20)
					break;
				case "Medio":
					sessionStorage.setItem("timer", true)
					sessionStorage.setItem("seconds", 15)
					break;
				default:
					console.log("Errore nel settaggio del timer")
					break;
			}

			document.getElementById("noTime").style.display='none'
			document.getElementById("timeOn").style.display='block'
		}

		if(!document.querySelector(".toggle-checkbox").checked){
			localStorage.clear();
			document.getElementById("noTime").style.display='block'
			document.getElementById("timeOn").style.display='none'
			sessionStorage.setItem("timer", false)
		}
	}

	return (
		
		<div className='Impostazioni'>
			<NavBar/>
			<div id="contentImpostazioni">
				<h1 id="titlePage">Impostazioni</h1>
				<div id="selectDifficolta">
					<h2 id='titleDiff'>Imposta Difficoltà</h2>
					<div id="buttonTipo">
						<div className='buttonImpostazioni'>
							<input type="button" className="button-35" onClick={funzioneButton} id="Facile" value="Facile"/>
						</div>
						<div className='buttonImpostazioni'>
							<input type="button" className="button-35" onClick={funzioneButton} id="Medio" value="Medio"/>
						</div>
						<div className='buttonImpostazioni'>
							<input type="button" className="button-35" onClick={funzioneButton} id="Difficile" value="Difficile"/>
						</div>
					</div>
					<div id="timeOption">
						<img src={noTime} id="noTime" alt='noTime'/>
						<div id="timeToggle">
							<label className="toggle">
								<input className="toggle-checkbox" type="checkbox" onChange={ChangeImage}/>
								<div className="toggle-switch"></div>
								<span className="toggle-label">
									<h2 style={{color:"white"}}>Time</h2>
								</span>
							</label>
						</div>
						<img src={timeOn} id="timeOn" alt='timeOn'/>
					</div>
				</div>
			</div>			
		</div>
	)
}

export default Impostazioni