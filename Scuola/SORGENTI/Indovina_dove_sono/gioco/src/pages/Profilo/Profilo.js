import React, { useEffect, useState } from 'react'
import './Profilo.css'
import NavBar from '../../components/NavBar/NavBar'
import user from '../../img/ut-img.png'
import { useNavigate } from 'react-router-dom'

const Profilo =()=> {
    const [utente, setutente] = useState({})
    const navigate = useNavigate()
    useEffect(() => {
        if (sessionStorage.getItem("logged") !== "true") {
            navigate('/')
        }

        let formData=new FormData()
		formData.append("username", sessionStorage.getItem("cookie_value"))
        const link = "http://localhost/Scuola/Indovina_dove_sono/login.php"
        // const link = "http://localhost/prog5c/gr3/PHP/login.php"

        let options = {
            method: "POST",
            body: formData
        }
        fetch(link, options)
        .then(response => response.json())
        .then(body =>{
            if(body === false){
                console.log("Errore")
            }else{
                // console.log("Dati:", body)
                setutente(body[0])
            }
        })
        // eslint-disable-next-line
    }, [])

    return (
        <div>
            <div>
                <NavBar />
            </div>
            <div className="box-utente">
                <h1 style={{fontSize: '60px', textAlign: 'center', marginTop: 0}}>Area personale</h1>
                <div className='icona-utente'>
                    <img id='anonymous' src={user} alt='Home' />
                </div>
                <div className='info-utente'>
                    <div className='UserName'>
                        {utente.fk_username !== undefined ? utente.fk_username : null}
                    </div>
                    <div>
                        <h2>Miglior punteggio: {utente.migliorPunteggio}</h2>
                        <h2>Ultimo punteggio: {utente.ultimoPunteggio}</h2>
                    </div>
                </div>
                {/* <div className='box-button' >
                    <label id='titleLout'>Effettua il logout </label>
                    <input type="button" className="button-93" onClick={() => {
                        sessionStorage.setItem("logged", false)
                        navigate('/')
                    }} value="Esci" />
                </div> */}
            </div>
        </div>
    );
}

export default Profilo