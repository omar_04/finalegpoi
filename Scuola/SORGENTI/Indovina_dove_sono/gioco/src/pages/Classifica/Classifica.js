import React, { useEffect, useState } from 'react'
import NavBar from '../../components/NavBar/NavBar'
import './Classifica.css'
import { useNavigate } from 'react-router-dom';

const Classifica = () => {
	document.title = "gr03 | Classifica"
	const [utente, setUtente] = useState(null);
	const navigate = useNavigate()
		
	useEffect(() => {
		if(sessionStorage.getItem("logged") !== "true"){
			navigate('/')
		}

		const link = 'http://localhost/Scuola/Indovina_dove_sono/classifica.php'
        // const link = "http://localhost/prog5c/gr3/PHP/classifica.php"
		fetch(link)
		.then(response => response.json())
		.then(data => {
		if (Array.isArray(data) && data.length > 0) {
			// console.log(data)
			setUtente(data);
			sessionStorage.setItem("user", JSON.stringify(data))
		} else {
			console.log('Error: invalid data received from server.');
		}
		})
		.catch(error => {
		console.log('Error: ', error);
		});
		// eslint-disable-next-line
	}, []);

	return (
		<div className='Classifica'>
		<NavBar />
		<div id='contentClassifica'>
			<h1 className='score'>Classifica</h1>
			{utente ? (
			<>
				<div id='contentscore'>
					<div className='riga'>
						<div className='tit-cella'>
							<h3>Utente</h3>
						</div>
						<div className='tit-cella'>
							<h3>Ultimo punteggio</h3>
						</div>
						<div className='tit-cella'>
							<h3>Punteggio migliore</h3>
						</div>
					</div>
					{utente.map(element => {
						return <div className='riga' key={element.fk_username}>
							<div className='cella' title={element.fk_username}>{element.fk_username}</div>
							<div className='cella'>{element.ultimoPunteggio}</div>
							<div className='cella'>{element.migliorPunteggio}</div>
						</div>
					})}
				</div>
			</>
			) : (
			<h2>Loading...</h2>
			)}
		</div>
		</div>
	);
};

export default Classifica;
