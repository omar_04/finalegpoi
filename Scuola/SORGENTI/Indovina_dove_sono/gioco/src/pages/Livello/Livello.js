import React, { useState, useLayoutEffect } from 'react'
import './Livello.css'
import Domanda from '../../components/Domanda/Domanda'
import Risposta from '../../components/Risposta/Risposta'
import { useNavigate } from 'react-router-dom'
import iconHome from '../../img/home.png'
import NavBar from '../../components/NavBar/NavBar'
import { domandeJsonC } from '../../Json/Citta/domandeCitta'
import { risposteJsonC } from '../../Json/Citta/risposteCitta'
import { domandeJsonN } from '../../Json/Nazioni/domandeNazioni'
import { risposteJsonN } from '../../Json/Nazioni/risposteNazioni'
import imgC from '../../img/praga.jpg'
import imgN from '../../img/italia.png'
import imgM from '../../img/misto.jpg'
import Alert from '../../components/Alert/Alert'
import Attenzione from '../../img/attenzione.png'

var tempoInterval // variabile globale per il setInterval
var ind = 0 // variabile indice globale
var punt = 0 // variabile punteggio globale
var rispCorRiep = [] // array globale contenente le risposte del riepilogo finale
var rispDom = [] // array globale contenente i literal object delle domande e delle risposte
var mostra = false

const Livello = () => {
	document.title = 'gr03 | Livello'

	document.addEventListener('contextmenu', function(e) {
		e.preventDefault();
	});

	const [domande, setdomande] = useState([]) // array stato per la visualizzazione delle domande
	const [risposte, setrisposte] = useState([]) // array stato per la visualizzazione delle risposte
	const [rispCtrl, setrispCtrl] = useState(false) // flag per sapere se l'utente ha selezionato la risposta corretta
	const [selezionato, setselezionato] = useState(false) // flag per sapere se l'utente ha selezionato una risposta
	const [loading, setLoading] = useState(false) // flag per il caricamento caricamento
	const [riepilogo, setriepilogo] = useState([]) // array stato per la visualizzazione del riepilogo
	const [tempo, settempo] = useState(0) // stato per la visualizzazione del tempo
	const [indice, setindice] = useState(0) // stato per la visualizzazione dell'indice
	const [punteggio, setpunteggio] = useState(0) // stato per la visualizzazione del punteggio
	const [modalitaScelta, setmodalitaScelta] = useState(false) // flag per sapere se l'utente ha selezionato una modalità
	
	const navigate = useNavigate()

	useLayoutEffect(() => {
		if(sessionStorage.getItem("logged") !== "true"){
			navigate('/')
		}
		setLoading(true)
		setdomande([])
		setrisposte([])
		setriepilogo([])
		settempo(0)
		setindice(0)
		setpunteggio(0)
		ind = 0
		punt = 0
		rispCorRiep = []
		rispDom = []
		setTimeout(() => {
			setLoading(false)
			window.scrollTo(0, 0)
		}, 2000)
		// eslint-disable-next-line
	}, [])

	// funzione per la scelta della modalità di gioco
	const selezioneModalita = (mod) => {
		switch(mod){
			case 1:
				rispDom = [domandeJsonC, risposteJsonC]
				break
			case 2:
				rispDom = [domandeJsonN, risposteJsonN]
				break
			case 3:
				rispDom = [[...domandeJsonC, ...domandeJsonN], [...risposteJsonC, ...risposteJsonN]]
				break
			default:
				console.error("Errore")
				break
		}
		setmodalitaScelta(true)
		creaDomanda()
	}

	// funzione per la creazione dinamica delle domande
	const creaDomanda = () => {
		let domandeArray = [] // array contenente componenti Domanda
		let corretteArray = [] // array contenente literal object delle risposte corrette
		let corretta={} // literal object della risposta corretta

		rispDom[0].sort(() => Math.random() - 0.5) // sorting delle domande
		rispDom[0].forEach((element, index) => {
			domandeArray.push(
				<Domanda
					domanda={element.domanda}
					img={element.immagine}
					key={index}
				/>
			)
			corretta = {
				risposta: element.giusta,
				tipo: element.tipo
			}
			corretteArray.push(corretta)
		})

		rispCorRiep = corretteArray.slice(0, sessionStorage.getItem("numDomande"))
		setdomande(domandeArray.slice(0, sessionStorage.getItem("numDomande"))) // settaggio domande
		creaRisposte(corretteArray)
	}

	// funzione per la creazione dinamica delle risposte
	const creaRisposte = (rispCorrette) => {
		let risposteArray = [] // array contenente array di componenti Risposta
		let indice = 0 // indice di controllo
		let blocchiRisp = [] // array contenente le risposte
		let appElement // variabile di appoggio
		rispDom[1].sort(() => Math.random() - 0.5)
		while(risposteArray.length<sessionStorage.getItem("numDomande")){ // controllo per evitare che ci siano risposte mancanti
			// eslint-disable-next-line
			rispDom[1].forEach((element, index) => {
				// if (rispCorrette.length !== 0){
					if (indice < 3) {
						if (rispCorrette[0].risposta !== element.risposta && rispCorrette[0].tipo === element.tipo) { // controllo per evitare 2 risposte uguali e di diverso tipo
							blocchiRisp.push(
								<Risposta
									risposta={element.risposta}
									key={element.risposta}
									funzione={() => {
										setrispCtrl(false)
										setselezionato(true)
									}}
								/>
							)
							indice++
						} else {
							appElement = rispDom[1][rispDom[1].length-1]
							rispDom[1][index] = appElement
							rispDom[1][rispDom[1].length-1] = element
							index--
						}
					} else {
						blocchiRisp.push(
							<Risposta
								risposta={rispCorrette[0].risposta}
								key={rispCorrette[0].risposta}
								funzione={() => {
									setrispCtrl(true)
									setselezionato(true)
								}}
							/>
						)
						rispCorrette.shift()  // rimuovo il primo elemento dell'array
						indice = 0
						risposteArray.push(blocchiRisp.sort(() => Math.random() - 0.5)) // sorting delle risposte
						blocchiRisp = []
					}
				// }
			})
		}
		

		setrisposte(risposteArray.slice(0, sessionStorage.getItem("numDomande"))) // selezione del numero di risposte

		if(sessionStorage.getItem("timer") === "true"){ // controllo per il timer
			timer()
		}
	}

	// funzione per il timer
	const timer = () => {
		let secondi = sessionStorage.getItem("seconds")
		settempo(secondi)
		// setInterval per eseguire il codice al suo interno ogni secondo
		tempoInterval = setInterval(()=>{
			if(secondi > 0){ // controllo se il tempo è maggiore di 0
				settempo(tempo => tempo-1)
				secondi--
			}else{ // nel caso non lo fosse, la risposta verrà contata sbagliata
				if(ind < sessionStorage.getItem("numDomande")){
					let immagineR = document.querySelector(".dom-container").children[0].getAttribute('src') // ottengo percorso immagine della domanda
					let domandaR = document.querySelector(".dom-container").children[1].textContent // ottengo testo della domanda
					
					let oggettoRiepilogo = { // literal object per il riepilogo
						img: immagineR,
						domanda: domandaR,
						rispUtente: 'Nessuna risposta scelta',
						rispCorretta: rispCorRiep[ind],
						giusta: false,
						color: '#ff0000'
					}

					if (punt > 0) { // controllo per verificare che il punteggio sia maggiore di 0 per poter essere decrementato
						punt--
					}
					
					setpunteggio(punt)
					setriepilogo(riep=>[...riep, oggettoRiepilogo])
					setindice(indice=> indice + 1) // avanzamento
					ind++
					secondi = sessionStorage.getItem("seconds")
					settempo(secondi)
				}else{
					clearInterval(tempoInterval) // interrompe il setInterval
				}
			}
		}, 1000)
	}

	const carica =()=>{
		let formData=new FormData()
		let user = JSON.parse(sessionStorage.getItem("user"))
		formData.append("username", user[0].fk_username)
		formData.append("punteggio", punt)
		// const link = "http://localhost/Scuola/Indovina_dove_sono/update.php"
        const link = "http://localhost/prog5c/gr3/PHP/update.php"
		let options = {
			method: "POST",
			body: formData
		}
		fetch(link, options)
		.then(response => response.json())
		.then(body=>{
			if(body === false){
				console.log("Errore")
			}else{
				console.log("partita conclusa")
				let puntFinale = (punt/parseInt(sessionStorage.getItem("numDomande")))*100
				getPunteggioTabellaUtenti(puntFinale, user[0].fk_username, "Indovina dove sono")
			}
		})
		function getPunteggioTabellaUtenti(puntigiococentesimi, username, nomegioco){
            let formdata= new FormData()
            formdata.append("user",username)
            let link="http://localhost/Scuola/SITI/FILE_PHP/get_stats.php"
            let options={method:"POST",body:formdata}
            fetch(link,options)
            .then(response=>response.json())
            .then(body=>{
                // console.log(body)
                let overall=body.punteggio
                if(puntigiococentesimi>=overall){
                    setPunteggioTabellaUtenti(username, nomegioco, puntigiococentesimi)
                }
            })
        }
        function setPunteggioTabellaUtenti(username, nomegioco, punteggio){
            let formdata = new FormData()
            formdata.append("user",username)
            formdata.append("punti",punteggio)
            formdata.append("gioco",nomegioco)
            let link="http://localhost/Scuola/SITI/FILE_PHP/set_stats.php"
            let options={method:"POST",body:formdata}
            fetch(link,options)
            .then(response=>response.json())
            .then(body=>{
                console.log("Punteggio aggiornato")
            })
        }
	}

	// funzione di controllo per ogni risposta data
	const controllo = () => {
		if (selezionato) {
			let immagineR = document.querySelector(".dom-container").children[0].getAttribute('src') // ottengo percorso immagine della domanda
			let domandaR = document.querySelector(".dom-container").children[1].textContent // ottengo testo della domanda
			let rispostaR = document.querySelector(".selez").children[0].textContent // ottengo la risposta data dall'utente
			
			let objRiepilogo = { // literal object per il riepilogo
				img: immagineR,
				domanda: domandaR,
				rispUtente:rispostaR,
				rispCorretta: rispCorRiep[ind],
				giusta:true,
				color: '#00ff00'
			}

			if (rispCtrl) { // controllo se l'utente ha risposto in modo esatto
				punt++
			} else if (punt > 0) { // controllo per verificare che il punteggio sia maggiore di 0 per poter essere decrementato
				objRiepilogo.giusta = false
				objRiepilogo.color = '#ff0000'
				punt--
			}else{
				objRiepilogo.giusta = false
				objRiepilogo.color = '#ff0000'
			}

			if(sessionStorage.getItem("timer") === "true"){
				clearInterval(tempoInterval)
				if(ind < sessionStorage.getItem("numDomande")){
					timer()
				}
			}

			setselezionato(false)
			setpunteggio(punt)
			setriepilogo(riep=>[...riep, objRiepilogo]) // settaggio dell'oggetto riepilogo
			setindice(indice=> indice + 1) // avanzamento
			ind++
			window.scrollTo(0, 0)
			if(ind === parseInt(sessionStorage.getItem("numDomande"))){
				clearInterval(tempoInterval)
				document.title = 'gr03 | Riepilogo'
				carica()
			}
		} else {
			document.querySelector('.alert').style.display = 'block'
			if(!mostra){
				mostra = true
				document.querySelector('.alert').classList.add('fadeIn')
				setTimeout(()=>{
					document.querySelector('.alert').classList.remove('fadeIn')
					setTimeout(() => {
						document.querySelector('.alert').style.display = 'none'
					}, 300);
					document.querySelector('.alert').classList.add('fadeOut')
					mostra = false
				}, 2500)
				document.querySelector('.alert').classList.remove('fadeOut')
			}
		}
	}

	return (
		<>
			<Alert
				icon={Attenzione}
				messaggio='Devi scegliere una risposta'
				colore='#ffa801'
			/>
			<div className='liv-container'>
				{indice + 1 > sessionStorage.getItem("numDomande") ?
					<>
						<h1 className='riepilogo'>Hai totalizzato {punteggio} punti</h1>
						<div className='box-riepilogo'>
							{riepilogo.map((element) => {
								return <div className='card-riepilogo' key={Math.random()}>
									<img src={element.img} alt='Immagine'/>
									<div>
										<h2>{element.domanda}</h2>
										<h3>La tua risposta: <span style={{color:element.color}}>{element.rispUtente}</span></h3>
										<h3>Risposta corretta: {element.rispCorretta!==undefined?element.rispCorretta.risposta:"Bho"}</h3>
									</div>
								</div>
							})}
						</div>
						<div className='containerButton'>
							<div>
								<input type='button' className='button-92' value='Torna alla Home' onClick={() => navigate('/home')} />
							</div>
							<div>
								<input type='button' className='button-92' value='Prossimo livello' onClick={() => navigate(0)} />
							</div>
						</div>
					</> :
					<>
						{!modalitaScelta ?
							<>
								<div className='cont-modalita'>
									<NavBar/>
									<div className='tit-modalità'>
										<h1>Scegli la modalità</h1>
									</div>
									<div className='box-modalita'>
										<div className='grid-modalita'>
											<div className='modalita' onClick={() => selezioneModalita(1)}>
												<div className='modalita-inner'>
													<div className='modalita-front'>
														<img src={imgC} alt='Immagine'/>
													</div>
													<div className='modalita-back'>
														<h2>Prova ad indovinare più città che puoi!</h2>
														<h1>Gioca</h1>
													</div>
												</div>
											</div>
											<div className='modalita' onClick={() => selezioneModalita(2)}>
											<div className='modalita-inner'>
													<div className='modalita-front'>
														<img src={imgN} alt='Immagine'/>
													</div>
													<div className='modalita-back'>
														<h2>Prova ad indovinare più nazioni che puoi!</h2>
														<h1>Gioca</h1>
													</div>
												</div>
											</div>
											<div className='modalita' onClick={() => selezioneModalita(3)}>
											<div className='modalita-inner'>
													<div className='modalita-front'>
														<img src={imgM} alt='Immagine'/>
													</div>
													<div className='modalita-back'>
														<h2>Metti in gioco le tue conoscienze con una partita mista!</h2>
														<h1>Gioca</h1>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</>
						:
							<>
								{loading ? 
									<div className="loader-container">
										<div className="spinner"></div>
									</div>
								:
									<>
										<div className='indietro'>
											<div onClick={() => navigate("/home")}>
												<img className="ind" src={iconHome} alt='Home' />
											</div>
										</div>
										<div className='punteggio'>
											<h1>{indice + 1}/{sessionStorage.getItem("numDomande")}</h1>
											{sessionStorage.getItem("timer") === "true" ?
												<h2>{tempo}</h2>:null
											}
										</div>
										<div className='domanda'>
											{domande[indice]}
										</div>
										<div className='risposte'>
											{risposte[indice]}
										</div>
										<input type='button' className='button-91' style={{width:'300px'}} value='Avanti' onClick={() => controllo()} />
									</>
								}
							</>
						}
					</>
				}
			</div >
		</>
	)
}

export default Livello
