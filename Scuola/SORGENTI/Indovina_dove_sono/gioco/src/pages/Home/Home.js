import React, { useEffect } from 'react'
import './Home.css'
import Logo from '../../img/indovinadovesono.png'
import Indietro from '../../img/back.png'
import NavBar from '../../components/NavBar/NavBar'
import { useNavigate } from 'react-router-dom'

const Home = () => {
	document.title = "gr03 | Home"
	const navigate = useNavigate()
	useEffect(() => {
		if(sessionStorage.getItem("logged") !== "true"){
			navigate('/')
		}
		// eslint-disable-next-line
	}, []);
	return (
		<div className='home'>
			<NavBar/>
			<div id="nav">
				<div id="logo">
					<img id="logoimg" src={Logo} alt='Logo'/>
				</div>
			</div>
			<div id="content">
				<div className='gioca'>
					<input type='button' value='Gioca' onClick={()=>navigate('/livello')} className='button-92' style={{width: '90%', textAlign:'center', minWidth:'280px', marginBottom:'1rem'}}/>
				</div>
				<div className="buttonMod">
					<h3>Modalità di Gioco: {sessionStorage.getItem("scelto")}</h3>
				</div>
			</div>
			<a href='http://localhost/Scuola/Play2Learn/#/mappa' className='sito'>
				<img src={Indietro} alt='Indietro' style={{width: '50px', height: '50px'}}/>
			</a>
		</div>
	)
}

export default Home