import React, { useState, useEffect } from 'react'
import './Login.css'
import { useNavigate } from 'react-router'

const Login = () => {
	document.title = "gr03 | Login"
	const navigate = useNavigate()
	const [logged, setlogged] = useState(false)
	sessionStorage.setItem("logged", false)

	useEffect(() => {
		if(get_cookie() !== ""){
			let user = get_cookie()
			sessionStorage.setItem("cookie_value", user)
			console.log(user)
			let formData=new FormData()
			formData.append("username", user)
			const link = "http://localhost/Scuola/Indovina_dove_sono/login.php"
			// const link = "http://localhost/prog5c/gr3/PHP/login.php"

			let options = {
				method: "POST",
				body: formData
			}
			fetch(link, options)
			.then(response => response.json())
			.then(body =>{
				if(body === false){
					console.log("Errore")
					setlogged(false)
				}else{
					// console.log("Dati:", body)
					sessionStorage.setItem("user", JSON.stringify(body))
					sessionStorage.setItem("logged", true)
					setlogged(true)
					navigate("/home")
				}
			})
		}else{
			setlogged(false)
		}
		// eslint-disable-next-line
	}, [])

	const get_cookie =()=>{ //ritorna username se c'è, altrimenti stringa vuota
		var username_cookie=document.cookie.split(";")
		//check cookie presente
		let pres=false
		username_cookie.forEach((ck)=>{
			if(ck.includes("session_username_GPOI")){
				pres=ck
			}
		})
		if(pres!==false){
			username_cookie=pres.split("=")[1]
			//console.log("Username: "+username_cookie)
			return username_cookie;
		}
		else{
			console.log("Cookie assente")
			return "";
		}
	}
	
	return (
		<>
			{	!logged ?
				<div className='login-cont'>
					<div className='login-box'>
						<h1 id="Loginh1">Per giocare devi effettuare l'accesso</h1>
					</div>
				</div>:null
			}
		</>
	)
}

export default Login