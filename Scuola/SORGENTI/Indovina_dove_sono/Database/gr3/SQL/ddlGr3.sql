DROP DATABASE IF EXISTS Stig_2023;
CREATE DATABASE Stig_2023;
USE Stig_2023;

CREATE TABLE IF NOT EXISTS utenti(
	username VARCHAR(15) NOT NULL,
	password CHAR(64) NOT NULL,
	nome VARCHAR(25) NOT NULL,
	cognome VARCHAR(30) NOT NULL,
	punteggio INT(11) DEFAULT 0,
	nomegioco VARCHAR(25) NULL,
	PRIMARY KEY(username)
);

CREATE TABLE IF NOT EXISTS Gr3_utenti(
	fk_username VARCHAR(15),
	ultimoPunteggio INT(11) DEFAULT 0,
	migliorPunteggio INT(11) DEFAULT 0,
	PRIMARY KEY (fk_username),
	FOREIGN KEY (fk_username) REFERENCES utenti(username)
);