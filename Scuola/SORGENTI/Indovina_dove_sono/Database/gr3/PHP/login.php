<?php
	require("./config.php");
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
    	$user=$_POST['username'];
        accesso($user);
    }

    function accesso($username){
        global $conn;
        $query = "SELECT * FROM gr3_utenti WHERE fk_username=?";
        $stmt = $conn->prepare($query);
        if(!$stmt){
            die("Preparazione query fallita: ".$conn->error);
        }
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows == 0){
            $query2 = "INSERT INTO gr3_utenti(fk_username) VALUES(?)";
            $stmt2 = $conn->prepare($query2);
            if(!$stmt2){
                die("Preparazione query fallita: ".$conn->error);
            }
            $stmt2->bind_param("s", $username);
            $stmt2->execute();
            $query3 = "SELECT * FROM gr3_utenti WHERE fk_username=?";
            $stmt3 = $conn->prepare($query3);
            if(!$stmt3){
                die("Preparazione query fallita: ".$conn->error);
            }
            $stmt3->bind_param("s", $username);
            $stmt3->execute();
            $result3 = $stmt3->get_result();
            $array = $result3->fetch_all(MYSQLI_ASSOC);
            echo json_encode($array);
        }else{
            $array = $result->fetch_all(MYSQLI_ASSOC);
            echo json_encode($array);
        }
   }
?>
