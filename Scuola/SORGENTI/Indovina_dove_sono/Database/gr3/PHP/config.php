<?php
    $host_ip = "localhost";
    $db_username = "root";
    $db_password = "";
    $db_name = "Stig_2023";

    $conn = new mysqli($host_ip, $db_username, $db_password, $db_name);
    if($conn->connect_error){
        die("Connessione fallita: ".$conn->connect_error);
    };
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST');
?>