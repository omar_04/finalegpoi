import { useState } from "react"
import "../CSS/Info.css"



const Info = () => {
    const [modal, setModal] = useState(false)
    const toggleModal = () => {
        setModal(!modal)
    }
    return (
        <>
            <button
                onClick={toggleModal}
                className="btn-modal">

            </button>
            {modal && (
                <div className="modal">
                    <div className="overlay" onClick={toggleModal}></div>
                    <div className="modal-content">
                        <h2>Regole del gioco</h2>
                        <p>GeoHoot è un gioco stile quiz. Sarai tenuto a rispondere a delle domande sulla geografia italiana. Le domande possono essere di diverse tipologie, ad esempio V/F oppure immagini da riconoscerne il monumento. Ogni risposta corretta aggiungerà un punto in più al totale del tuo punteggio finale. Ci sono diverse difficoltà di gioco facile,difficile e normale nelle modalita piu difficili avrai un cronometro allo scadere del tempo la tua partita sara finita.</p>
                    </div>
                    <button
                        onClick={toggleModal}
                        className="close-modal"> 
                    </button>
                </div>
            )}    
        </>
    )
}
export default Info