import React, { useContext } from 'react';
import { QuizResultsContext } from '../App';
import { NavLink } from 'react-router-dom';
import colosseo from "../img/Colosseo.jpg"
import pisa from "../img/torre_di_pisa.jpg"
import duomo from "../img/duomo.jpg"
import trevi from "../img/trevi.jpg"
import pompei from "../img/pompei.jpg"
import pantheon from "../img/pantheon.jpg"
import BaSm from "../img/BSM.jpeg"
import Cterre from "../img/5terre.jpg"
import moro from "../img/moro.jpg"
import vesuvio from "../img/vesuvio.jpeg"
import Pvecchio from "../img/PVecchio.jpg"
import Ragusa from "../img/Ragusa.jpg"
import Trulli from "../img/Trulli.jpg"
import DuomoAmalfi from "../img/duomoamalfi.jpg"
import TeatroTaormina from "../img/TeatroTaormina.jpg"
import CasteldelOvo from "../img/castel_dell_ovo.jpg"
import torrediercole from "../img/torrediercole.jpg"
import CastelStAngelo from "../img/CastelAngelo.jpg"
import PalazzoVecchio from "../img/PalazzoVecchio.jpg"
import PzSm from "../img/PSM.jpg"
import DuomoFirenze from "../img/FirenzeDuomo.jpg"

import "../CSS/Resoconto.css"

const QuizResults = () => {
  const imageNames = ["colosseo", "pisa", "duomo", "trevi", "pompei", "pantheon"];
  const resoconto = useContext(QuizResultsContext);
  console.log(resoconto)
  const getImageUrl = (imageName) => {
    switch (imageName) {
      case "colosseo":
        return colosseo;
      case "pisa":
        return pisa;
      case "duomo":
        return duomo;
      case "trevi":
        return trevi;
      case "pompei":
        return pompei;
      case "pantheon":
        return pantheon;
      case "BSM":
        return BaSm;
      case "Cterre":
        return Cterre;
      case "moro":
        return moro;
      case "vesuvio":
        return vesuvio;
      case "PVecchio":
        return Pvecchio;
      case "Ragusa":
        return Ragusa;
      case "Trulli":
        return Trulli;
      case "DuomoAmalfi":
        return DuomoAmalfi;      
      case "TeatroTaormina":
        return TeatroTaormina; 
      case "CasteldelOvo":
        return CasteldelOvo;
      case "torrediercole":
        return torrediercole;
      case "CastelStAngelo":
        return CastelStAngelo;
      case "PalazzoVecchio":
        return PalazzoVecchio;
      case "PSM":
        return PzSm;
      case "DuomoFirenze":
        return DuomoFirenze;
      default:
        return null;
    }
  };
  return (
    <div className='not-scrollable home-container'>
      <div className="header-container">
        <h1 id="titres">Resoconto</h1>  
      </div>
      <div id="contres">
        {resoconto.quizResults.map((result, index) => (
          //ho fatto il css nello style per provare veloce questo div avra come classe wrong o correct in base al fatto se la risposta e giusta o sbagliata 
          <div key={index} style={{
          backgroundImage: `url(${getImageUrl(result.image_url)})`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          width: '100%',
          height:"500px"
          }} className={result.selectedAnswer === result.correctAnswer ? 'correct' : 'wrong'} id="singolo">
            <h3>{result.question}</h3>
            {result.type === 'vero_falso' && (
              <p>
                 {result.correctAnswer}
              </p>
            )}
            {result.type === 'multipla' && (
              <div >
                {result.selectedAnswer === result.correctAnswer ? (
              <p>
                {result.correctAnswer}
              </p>
            ) : (
              <div>
                <p>
                  Risposta corretta: {result.correctAnswer}
                </p>
                <p>
                  Hai messo: {result.selectedAnswer}
                </p>
              </div>
    )}
              </div>
              
            )}
          </div>
        ))}
      </div>
      <div className="footer-container">
        <NavLink to="/select" className="button back-button">Torna Indietro</NavLink>
      </div>
    </div>
  );
};

export default QuizResults;
