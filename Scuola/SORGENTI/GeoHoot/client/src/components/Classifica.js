import React, { useState, useEffect } from 'react';
import "../CSS/Classifica.css"
import trophy from "../CSS/icons/trophy.png"
import back from "../CSS/icons/back.png"
import { NavLink } from 'react-router-dom';


function Classifica() {
  const [modalita, setModalita] = useState('');
  const [classifica, setClassifica] = useState([]);

  useEffect(() => {
    let url = `http://localhost/Scuola/Geohoot/classifica.php`;
    if (modalita !== '') {
      url += `?modalita=${modalita}`;
    }
    fetch(url)
      .then(response => response.json())
      .then(data => setClassifica(data));
  }, [modalita]);

  const handleSelectChange = (event) => {
    setModalita(event.target.value);
  };

  return (
    <div className='home-container'>
      <NavLink to="/home" className="button-little-lead back-button little-lead"><img className='back-img' src={back} /></NavLink>
      <div id="titres">
        classifica
      </div>
      <div className="body-container">
      <form className='form-box'>
        <label>
          Seleziona la modalità:
          <select value={modalita} onChange={handleSelectChange}>
            <option className='select-leader' value="">Scegli--</option>
            <option className='select-leader' value="Facile">Facile</option>
            <option className='select-leader'value="Normale">Normale</option>
            <option className='select-leader'value="Difficile">Difficile</option>
            <option className='select-leader'value="Impossibile">Impossibile</option>
          </select>
        </label>
      </form>
      {classifica.length > 0 ? (
        <div className='table'>

            {classifica.map((record, index) => (
              <div className='tr' key={index}>
                <div className='td'>{record.username_giocatore}</div>
                <div className="score-box">
                  <div className='back-td'>{record.punteggio}</div>
                  <img src={trophy} className='little-trophy' />
                </div>
              </div>
            ))}
        </div>
      ) : (
        <div className='table-null'>Non ci sono record</div>
      )}
      </div>
    </div>
  );
}

export default Classifica;