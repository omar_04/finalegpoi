import { useState, useEffect ,useContext} from 'react';
import { useNavigate,Link} from "react-router-dom"
import '../CSS/Impossibile.css';
import Timer from "./Time"
import { QuizExport ,QuizResultsContext,TempoExport,curquest} from '../App';
import colosseo from "../img/Colosseo.jpg"
import pisa from "../img/torre_di_pisa.jpg"
import duomo from "../img/duomo.jpg"
import trevi from "../img/trevi.jpg"
import pompei from "../img/pompei.jpg"
import pantheon from "../img/pantheon.jpg"
import BaSm from "../img/BSM.jpeg"
import Cterre from "../img/5terre.jpg"
import moro from "../img/moro.jpg"
import vesuvio from "../img/vesuvio.jpeg"
import Pvecchio from "../img/PVecchio.jpg"
import Ragusa from "../img/Ragusa.jpg"
import Trulli from "../img/Trulli.jpg"
import DuomoAmalfi from "../img/duomoamalfi.jpg"
import TeatroTaormina from "../img/TeatroTaormina.jpg"
import CasteldelOvo from "../img/castel_dell_ovo.jpg"
import torrediercole from "../img/torrediercole.jpg"
import CastelStAngelo from "../img/CastelAngelo.jpg"
import PalazzoVecchio from "../img/PalazzoVecchio.jpg"
import PzSm from "../img/PSM.jpg"
import DuomoFirenze from "../img/FirenzeDuomo.jpg"
const Game = () => {
  const imageNames = ["colosseo", "pisa", "duomo", "trevi", "pompei", "pantheon"];
  const [currentQuestionType, setCurrentQuestionType] = useState(null);
  const [questions, setQuestions] = useState([]);
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [score, setScore] = useState(0);
  const [showScore, setShowScore] = useState(false);
  const numquiz = useContext(QuizExport)
  const tempo = useContext(TempoExport)
  const resoconto = useContext(QuizResultsContext)
  const curquesti = useContext(curquest)
  const menu=useNavigate()
  useEffect(() => {
    fetchData();
    setCurrentQuestion(0);
    setScore(0);
    tempo.setT(0.1);
  }, []);

  const fetchData = () => {
    fetch('https://raw.githubusercontent.com/teodn7/Gpoi/main/Monumenti.json')
      .then(response => response.json())
      .then(data => {
        let questions = [...data.questions].sort(() => 0.5 - Math.random());
        let randomQuestions = questions.slice(0, 10).map(q => {
          let answers = q.answers.sort(() => 0.5 - Math.random());
          return { ...q, answers };
        });
        console.log(randomQuestions);
        setQuestions(randomQuestions);
      })
  };
  

  const handleAnswerOptionClick = (isCorrect) => {
    console.log(currentQuestion)
    let end = 4;
    const currentAnswer = questions[currentQuestion]?.answers.find(
      (answer) => answer.correct === isCorrect
    );
    const result = {
      question: questions[currentQuestion]?.question,
      selectedAnswer: currentAnswer.answer,
      correctAnswer: questions[currentQuestion]?.answers.find(
        (answer) => answer.correct
      )?.answer,
    };
    resoconto.setQuizResults([...resoconto.quizResults, result]);
    setCurrentQuestion(currentQuestion + 1);
    curquesti.setCq(currentQuestion)
    if (isCorrect) {
      // Aggiorna il punteggio solo se la risposta è corretta
      setScore(score + 1);
    } else {
      // Richiama la funzione showScore solo se la risposta è sbagliata
      showscore();
    }
    if (currentQuestion === end ) {
      setShowScore(true);
    }
  };
  
  const showscore=()=>{
    setShowScore(true)
  }
  const getImageUrl = (imageName) => {
    switch (imageName) {
      case "colosseo":
        return colosseo;
      case "pisa":
        return pisa;
      case "duomo":
        return duomo;
      case "trevi":
        return trevi;
      case "pompei":
        return pompei;
      case "pantheon":
        return pantheon;
      case "BSM":
        return BaSm;
      case "Cterre":
        return Cterre;
      case "moro":
        return moro;
      case "vesuvio":
        return vesuvio;
      case "PVecchio":
        return Pvecchio;
      case "Ragusa":
        return Ragusa;
      case "Trulli":
        return Trulli;
      case "DuomoAmalfi":
        return DuomoAmalfi;      
      case "TeatroTaormina":
        return TeatroTaormina; 
      case "CasteldelOvo":
        return CasteldelOvo;
      case "torrediercole":
        return torrediercole;
      case "CastelStAngelo":
        return CastelStAngelo;
      case "PalazzoVecchio":
        return PalazzoVecchio;
      case "PSM":
        return PzSm;
      case "DuomoFirenze":
        return DuomoFirenze;


      default:
        return null;
    }
  };
  const handleReturnButtonClick = () => {
    resoconto.setQuizResults("")
    setShowScore(false);
    setCurrentQuestion(0);
    setScore(0);
    menu("/select")
  };
  const currentAnswerOptions = questions[currentQuestion]?.answers || [];
  return (
    <div className='home-container'>
  {showScore && score === 5 ? (
<>
  <div className='score-section'>
    <div id="score-text">
      <p>Complimenti! Hai risposto correttamente a tutte le domande!</p> 
      <p>Puoi passare al vero livello impossibile!</p>      
    </div> 
    <Link className='go-box' to="/finalgame"><button className='go-button'>Vai</button></Link>
  </div>
</>
) : showScore ? (
<div className='score-section'>
  <div id="score-text">
    <p>Hai perso! Non hai risposto correttamente a tutte le domande!</p>
    <p>Hai totalizzato un punteggio di: {score} su {questions.length}</p>
    <p>Ritenta sarai piu fortunato!</p> 
  </div> 
  <div className="button-box">
    <button className='go-button' onClick={handleReturnButtonClick}>Ritorna ai quiz</button>
    <button className='go-button' onClick={()=>{window.location.reload()}}>Riprova</button>
  </div>
</div>
) : (
<>
 {questions.length > 0?(
    <div className='question-section'>
        <div className='question-count'>
        <span>Question {currentQuestion+1}</span>/{questions.length}
        <Timer tempo={tempo.t} funzione={showscore} modalita="impossibile" cq={currentQuestion} setcq={setCurrentQuestion}/>
        </div>
        <div className='question-text'>
            <p>{questions[currentQuestion]?.question}</p>
            {questions[currentQuestion]?.image_url && (
            <img className='img-box' src={getImageUrl(questions[currentQuestion]?.image_url)}/>
            )}
        </div>
    </div>
    ):null}
  <div className="answer-box">
    <div className='answer-section'>
      {currentAnswerOptions.map((answerOption, index) => (
        <button className='answer-button' id={index}
          key={index}
          onClick={() => {
            handleAnswerOptionClick(answerOption.correct);
          }}
        >
          {answerOption.answer}
        </button>
      ))}
    </div>
  </div>
</>
)}
</div>
  );
};
export default Game;    



