import { React, useEffect, useState, useContext } from 'react';
import { QuizExport } from '../App';

const Timer = (props) => {
  const [timeRemaining, setTimeRemaining] = useState(props.tempo * 60);
  const [impossibleTimer, setImpossibleTimer] = useState(false);
  const numquiz = useContext(QuizExport);
  useEffect(() => {
      if (timeRemaining === 0) {
          if( props.cq==4 || props.cq==29) {
              props.funzione();
            }
          if (props.modalita === 'impossibile') {
          // Se il tempo è scaduto nella modalità impossibile, incrementa la domanda corrente e reimposta il timer
          props.setcq(props.cq+1);
          setImpossibleTimer(true);
          setTimeRemaining(props.tempo * 60);
          }
        else {
          props.funzione();
        }
      }
      else {
        // Imposta l'intervallo solo se il tempo rimanente è maggiore di 0
        const intervalId = setInterval(() => {
          setTimeRemaining((prevTime) => prevTime - 1);
        }, 1000);
        return () => clearInterval(intervalId);
      }
    
  }, [timeRemaining]);
  useEffect(() => {
    if (props.modalita === 'impossibile') {
      // Se il tempo è scaduto nella modalità impossibile, incrementa la domanda corrente e reimposta il timer
      setTimeRemaining(props.tempo * 60);
      }
  }, [props.cq]);
  // Converte il tempo rimanente in formato "minuti:secondi"
  const formatTime = () => {
    const minutes = Math.floor(timeRemaining / 60);
    const seconds = timeRemaining % 60;
    return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
  };

  return (
    <div id="timer">
      {formatTime()}
    </div>
  );
};

export default Timer;
