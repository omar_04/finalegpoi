import  { React,useContext, useState} from 'react'
import {NavLink,useNavigate} from "react-router-dom"
import { QuizExport,TempoExport,ModalitaExport } from '../App';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "../CSS/Select.css"
import back from "../CSS/icons/back.png"
import trophy from "../CSS/icons/trophy.png"
import Info from "./Info.js"


const Select = () => {
  const nav = useNavigate()
const tempo = useContext(TempoExport)
const modalita = useContext(ModalitaExport)
const [dif, setDif] = useState(0)
const Navigate = useNavigate()
  function checkTagsFocus() {
      let ceccato=document.querySelector("input:checked")
      if(ceccato==null){
        return false;
      }else{
        return true;
      }
    }
    const nQuiz = (e) => {
      let sel = e.target.children[0];
      const newDif = sel.value; // Salviamo il nuovo valore in una variabile
      setDif(newDif);
      sel.checked = true;
      numquiz.setquiz(newDif); // Passiamo il nuovo valore direttamente a numquiz.setquiz
      if (newDif === "10"){
        tempo.setT(3);
        modalita.setModalita("Normale")
      }  
      else if(newDif === "666") { // Usiamo "===" per confrontare i valori
        tempo.setT(0.1);
        modalita.setModalita("Impossibile")
      } else if (newDif === "15") {
        modalita.setModalita("Difficile")
        tempo.setT(3);
      }else{
        modalita.setModalita("Facile")
        tempo.setT(2);
      }
      
    };

    const check=()=>{
    if(checkTagsFocus()===true && dif!=666){
      Navigate("/game")
    }
    else if(checkTagsFocus()===true && dif==666){
      Navigate("/impossibile")
   }
    else{
      toast.warn('Seleziona una difficoltà!', {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  }

  
  const numquiz = useContext(QuizExport);
  return (
    <div className="home-container">
    <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss={false}
        draggable
        pauseOnHover={false}
        theme="light"
    />

        <div className='trophy-info'>
          <img src={trophy} className='trophy' onClick={()=>{nav("/classifica")}}/>
          <Info id="info"/>
        </div>
     
      <div className="title-box">
      <h1 className="title-full">
        <div className='title-char'>S</div>
        <div className='title-char'>c</div>
        <div className='title-char'>e</div>
        <div className='title-char'>g</div>
        <div className='title-char'>l</div>
        <div className='title-char'>i</div>
      </h1>
      <h1 className="title-full">
        <div className='title-char'>l</div>
        <div className='title-char'>a</div>
      </h1>
        <h1 className="title-full">
          <div className='title-char'>d</div>
          <div className='title-char'>i</div>
          <div className='title-char'>f</div>
          <div className='title-char'>f</div>
          <div className='title-char'>i</div>
          <div className='title-char'>c</div>
          <div className='title-char'>o</div>
          <div className='title-char'>l</div>
          <div className='title-char'>t</div>
          <div className='title-char'>à</div>
        </h1>
      </div>
        <div className="difficolta">
          <div className="m3">
            <div  onClick={nQuiz}className='select-button'><input type="radio" name="difficolta" value="5"/>Facile</div>
            <div  onClick={nQuiz}className='select-button'><input type="radio"  name="difficolta" value="10"/>Normale</div>
            <div  onClick={nQuiz}className='select-button'><input type="radio" name="difficolta" value="15"/>Difficile</div>
          </div>
          <div  onClick={nQuiz}className='select-button2'><input type="radio" name="difficolta" value="666"/>Impossibile</div> 
      </div>
      <div className="button-box">
        <button className="button start-button" onClick={check}>Inizia la Partita</button>
        <NavLink to="/home" className="button-little back-button little"><img className='back-img' src={back} /></NavLink>
      </div>  
    </div>
  )
}
export default Select