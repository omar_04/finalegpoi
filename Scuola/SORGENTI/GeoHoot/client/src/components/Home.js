import {React,useContext,useState} from 'react'
import {NavLink, useNavigate} from "react-router-dom"
import {QuizResultsContext,UsrContext} from '../App';
import "../CSS/Home.css"
import Info from "./Info.js"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import trophy from "../CSS/icons/trophy.png"

const Home = () => {
  
  const resoconto = useContext(QuizResultsContext);
  const Usr = useContext(UsrContext);
  resoconto.setQuizResults("")
  const nav = useNavigate()
 
  const handleSubmit = (e) => {
    e.preventDefault();
    if (document.querySelector(".username-box").value !== "") {
      const username = document.querySelector(".username-box").value;
      Usr.setUsr(username);
      fetch(`http://localhost/Scuola/Geohoot/api.php?username=${username}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then(response => response.json())
      .then(data => {
        if (data.exists) {
          nav("/select");
        } else {
          toast.error('Username non trovato!', {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: false,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
        }
      })
      .catch(error => console.error(error));
    } else {
      toast.warn('Inserire username!', {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  }

  return (
    <div className="home-container">   
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss={false}
        draggable
        pauseOnHover={false}
        theme="light"
        />
      <div className="home-header">
        <NavLink to="/" className="button back-button">Torna Indietro</NavLink>
        <div className='trophy-info'>
          <img src={trophy} className='trophy' onClick={()=>{nav("/classifica")}}/>
          <Info id="info"/>
        </div>
      </div>
        <h1 className="logo-full">
          <div className='logo-char'>G</div>
          <div className='logo-char'>e</div>
          <div className='logo-char'>o</div>
          <div className='logo-char'>H</div>
          <div className='logo-char'>o</div>
          <div className='logo-char'>o</div>
          <div className='logo-char'>t</div>
        </h1>
        <form onSubmit={handleSubmit} className='form-box' action="" method="post">
          <input className='username-box' type="text" name="username" placeholder='username'/>
          <input className="button start-button" type="submit" value="Inizia la Partita" />
        </form>
    </div>
  )
}

export default Home