import { useState, useEffect ,useContext} from 'react';
import { useNavigate,Link} from "react-router-dom"
import '../CSS/Game.css';
import Timer from "./Time"
import { QuizExport ,QuizResultsContext,TempoExport,UsrContext,ModalitaExport} from '../App';
import colosseo from "../img/Colosseo.jpg"
import pisa from "../img/torre_di_pisa.jpg"
import duomo from "../img/duomo.jpg"
import trevi from "../img/trevi.jpg"
import pompei from "../img/pompei.jpg"
import pantheon from "../img/pantheon.jpg"
import BaSm from "../img/BSM.jpeg"
import Cterre from "../img/5terre.jpg"
import moro from "../img/moro.jpg"
import vesuvio from "../img/vesuvio.jpeg"
import Pvecchio from "../img/PVecchio.jpg"
import Ragusa from "../img/Ragusa.jpg"
import Trulli from "../img/Trulli.jpg"
import DuomoAmalfi from "../img/duomoamalfi.jpg"
import TeatroTaormina from "../img/TeatroTaormina.jpg"
import CasteldelOvo from "../img/castel_dell_ovo.jpg"
import torrediercole from "../img/torrediercole.jpg"
import CastelStAngelo from "../img/CastelAngelo.jpg"
import PalazzoVecchio from "../img/PalazzoVecchio.jpg"
import PzSm from "../img/PSM.jpg"
import DuomoFirenze from "../img/FirenzeDuomo.jpg"




const Game = () => {
  const modalita = useContext(ModalitaExport)
  const Usr = useContext(UsrContext)
  const imageNames = ["colosseo", "pisa", "duomo", "trevi", "pompei", "pantheon"];
  const [currentQuestionType, setCurrentQuestionType] = useState(null);
  const [questions, setQuestions] = useState([]);
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [score, setScore] = useState(0);
  const [showScore, setShowScore] = useState(false);
  const numquiz = useContext(QuizExport)
  const tempo = useContext(TempoExport)
  const resoconto = useContext(QuizResultsContext)
  const menu=useNavigate()
  useEffect(() => {
    const fetchData = async () => {
      console.log(Usr.Usr)
      const response1 = await fetch('https://raw.githubusercontent.com/teodn7/Gpoi/main/Domandenormali.json');
      const response2 = await fetch('https://raw.githubusercontent.com/teodn7/Gpoi/main/veroFalso.json');
      const response3 = await fetch('https://raw.githubusercontent.com/teodn7/Gpoi/main/Monumenti.json');
      const data1 = await response1.json();
      const data2 = await response2.json();
      const data3 = await response3.json();
      const allQuestions = [...data1.questions, ...data2.questions, ...data3.questions];
      const randomQuestions = shuffleArray(allQuestions).slice(0,numquiz.quiz);
      const shuffledQuestions = randomQuestions.map(question => {
        const shuffledAnswers = shuffleArray(question.answers);
        return {...question, answers: shuffledAnswers};
      });
      resoconto.setQuizResults([]);
      setQuestions(shuffledQuestions);
    };
    
    const shuffleArray = (array) => {
      let currentIndex = array.length;
      let temporaryValue, randomIndex;
      while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }
      return array;
    };
    
    fetchData();
  }, []);
  const handleAnswerOptionClick = (isCorrect) => {
    const currentAnswer = questions[currentQuestion]?.answers.find(
      (answer) => answer.correct === isCorrect
    );
    // console.log(currentAnswer);
    const result = {
      question: questions[currentQuestion]?.question,
      selectedAnswer: currentAnswer.answer,
      correctAnswer: questions[currentQuestion]?.answers.find(
        (answer) => answer.correct
      )?.answer,
      type: questions[currentQuestion].type, // Aggiunge la proprietà "type" all'oggetto "result"
      image_url: questions[currentQuestion]?.image_url, // Aggiunge la proprietà "image_url" all'oggetto "result"
    };
      if (currentAnswer.correct === true){
        setScore(score + 1);
      }
    console.log("Risposta:"+currentAnswer.answer+ " Giusta:"+currentAnswer.correct +"Punteggio:"+score);
    resoconto.setQuizResults([...resoconto.quizResults, result]);
    setCurrentQuestion(currentQuestion + 1);
    const end = questions.length - 1;
    if (currentQuestion === end) {
       showscore(Usr, modalita);
    }
  };  
  const showscore=(Usr, modalita)=>{
    let username = Usr.Usr;
    let Modalita = modalita.modalita;
    let punteggio = score+1;
    console.log(username,Modalita,punteggio);
    fetch(`http://localhost/Scuola/Geohoot/inserimento.php?username=${username}&modalita=${Modalita}&punteggio=${punteggio}`)
      .then(response => response.text())
      .then(data => console.log(data))
      .catch(error => console.error(error));
    setShowScore(true)
  }
  const getImageUrl = (imageName) => {
    switch (imageName) {
      case "colosseo":
        return colosseo;
      case "pisa":
        return pisa;
      case "duomo":
        return duomo;
      case "trevi":
        return trevi;
      case "pompei":
        return pompei;
      case "pantheon":
        return pantheon;
      case "BSM":
        return BaSm;
      case "Cterre":
        return Cterre;
      case "moro":
        return moro;
      case "vesuvio":
        return vesuvio;
      case "PVecchio":
        return Pvecchio;
      case "Ragusa":
        return Ragusa;
      case "Trulli":
        return Trulli;
      case "DuomoAmalfi":
        return DuomoAmalfi;      
      case "TeatroTaormina":
        return TeatroTaormina; 
      case "CasteldelOvo":
        return CasteldelOvo;
      case "torrediercole":
        return torrediercole;
      case "CastelStAngelo":
        return CastelStAngelo;
      case "PalazzoVecchio":
        return PalazzoVecchio;
      case "PSM":
        return PzSm;
      case "DuomoFirenze":
        return DuomoFirenze;
      default:
        return null;
    }
  };
  const handleReturnButtonClick = () => {
    resoconto.setQuizResults("")
    setShowScore(false);
    setCurrentQuestion(0);
    setScore(0);
    menu("/select")
  };
  const currentAnswerOptions = questions[currentQuestion]?.answers || [];
  return (
    <div className='home-container'>
  {showScore ? (
    <div className='score-section'>
      <div id="score-text">
        Hai totalizzato un punteggio di: {score} su {questions.length}
      </div> 
      <div className="button-box2">
        <button className="button" onClick={handleReturnButtonClick}>Return to Quiz</button>
        <Link className='res-box' to="/resoconto"><button className="button">Resoconto</button></Link>
      </div>
    </div>
  ) : (
    <>
     {questions.length > 0 && (
  <div className='question-section'>
    <div className='question-count'>
      <span>Question {currentQuestion+1}</span>/{questions.length}
      <Timer tempo={tempo.t} funzione={showscore} setcq={setCurrentQuestion} cq={currentQuestion}/>
    </div>
      <div className='question-text'>
        <p>{questions[currentQuestion]?.question}</p>
        {questions[currentQuestion]?.image_url && (
          <img className='img-box' src={getImageUrl(questions[currentQuestion]?.image_url)}/>
        )}
      </div>
  </div>
)}
<div className="answer-box">
    <div className='answer-section'>
      {currentAnswerOptions.map((answerOption, index) => (
        <button className='answer-button' id={index}
          key={index}
          onClick={() => {
            handleAnswerOptionClick(answerOption.correct);
          }}
        >
          {answerOption.answer}
        </button>
      ))}
    </div>
  </div>
    </>
  )}
</div>
  );
};
export default Game;