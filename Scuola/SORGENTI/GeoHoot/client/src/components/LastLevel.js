import { useState, useEffect ,useContext} from 'react';
import { useNavigate,Link} from "react-router-dom"
import Timer from "./Time"
import { QuizExport ,QuizResultsContext,TempoExport,ModalitaExport,UsrContext} from '../App';
import colosseo from "../img/Colosseo.jpg"
import pisa from "../img/torre_di_pisa.jpg"
import duomo from "../img/duomo.jpg"
import trevi from "../img/trevi.jpg"
import pompei from "../img/pompei.jpg"
import pantheon from "../img/pantheon.jpg"
import BaSm from "../img/BSM.jpeg"
import Cterre from "../img/5terre.jpg"
import moro from "../img/moro.jpg"
import vesuvio from "../img/vesuvio.jpeg"
import Pvecchio from "../img/PVecchio.jpg"
import Ragusa from "../img/Ragusa.jpg"
import Trulli from "../img/Trulli.jpg"
import DuomoAmalfi from "../img/duomoamalfi.jpg"
import TeatroTaormina from "../img/TeatroTaormina.jpg"
import CasteldelOvo from "../img/castel_dell_ovo.jpg"
import torrediercole from "../img/torrediercole.jpg"
import CastelStAngelo from "../img/CastelAngelo.jpg"
import PalazzoVecchio from "../img/PalazzoVecchio.jpg"
import PzSm from "../img/PSM.jpg"
import DuomoFirenze from "../img/FirenzeDuomo.jpg"
import "../CSS/LastLevel.css"

const Game = () => {
  const modalita = useContext(ModalitaExport)
  const Usr = useContext(UsrContext)
  const imageNames = ["colosseo", "pisa", "duomo", "trevi", "pompei", "pantheon"];
  const [currentQuestionType, setCurrentQuestionType] = useState(null);
  const [questions, setQuestions] = useState([]);
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [score, setScore] = useState(0);
  const [showScore, setShowScore] = useState(false);
  const menu=useNavigate()
  useEffect(() => {
    const fetchData = async () => {
      console.log(Usr.Usr)
      const response1 = await fetch('https://raw.githubusercontent.com/teodn7/Gpoi/main/Domandenormali.json');
      const response2 = await fetch('https://raw.githubusercontent.com/teodn7/Gpoi/main/veroFalso.json');
      const response3 = await fetch('https://raw.githubusercontent.com/teodn7/Gpoi/main/Monumenti.json');
      const data1 = await response1.json();
      const data2 = await response2.json();
      const data3 = await response3.json();
      const allQuestions = [...data1.questions, ...data2.questions, ...data3.questions];
      const randomQuestions = shuffleArray(allQuestions).slice(0,30);
      const shuffledQuestions = randomQuestions.map(question => {
        const shuffledAnswers = shuffleArray(question.answers);
        return {...question, answers: shuffledAnswers};
      });
      setQuestions(shuffledQuestions);
    };
    
    const shuffleArray = (array) => {
      let currentIndex = array.length;
      let temporaryValue, randomIndex;
      while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }
      return array;
    };
    
    fetchData();
  }, []);
  const handleAnswerOptionClick = (isCorrect) => {
    const currentAnswer = questions[currentQuestion]?.answers.find(
      (answer) => answer.correct === isCorrect
    );
      if (currentAnswer.correct === true) {
        setScore(score + 1);
      }else{
        setShowScore(true)
      }
    const end = questions.length - 1;
      if (currentQuestion === end) {
        showscore(Usr,modalita)
      } else {
      setCurrentQuestion(currentQuestion + 1);
      setCurrentQuestionType(currentAnswer.type);
    }
  };    
  const showscore=(Usr,modalita)=>{
      let username = Usr.Usr;
      let Modalita = modalita.modalita;
      let punteggio = score+5;
      console.log(username,Modalita,punteggio);
      fetch(`http://localhost/Scuola/Geohoot/inserimento.php?username=${username}&modalita=${Modalita}&punteggio=${punteggio}`)
        .then(response => response.text())
        .then(data => console.log(data))
        .catch(error => console.error(error));
    setShowScore(true)
  }
  const getImageUrl = (imageName) => {
    switch (imageName) {
      case "colosseo":
        return colosseo;
      case "pisa":
        return pisa;
      case "duomo":
        return duomo;
      case "trevi":
        return trevi;
      case "pompei":
        return pompei;
      case "pantheon":
        return pantheon;
      case "BSM":
        return BaSm;
      case "Cterre":
        return Cterre;
      case "moro":
        return moro;
      case "vesuvio":
        return vesuvio;
      case "Pvecchio":
        return Pvecchio;
      case "Ragusa":
        return Ragusa;
      case "Trulli":
        return Trulli;
      case "DuomoAmalfi":
        return DuomoAmalfi;      
      case "TeatroTaormina":
        return TeatroTaormina; 
      case "CasteldelOvo":
        return CasteldelOvo;
      case "torrediercole":
        return torrediercole;
      case "CastelStAngelo":
        return CastelStAngelo;
      case "PalazzoVecchio":
        return PalazzoVecchio;
      case "PSM":
        return PzSm;
      case "DuomoFirenze":
        return DuomoFirenze;
      default:
        return null;
    }
  };
  const handleReturnButtonClick = () => {
    setShowScore(false);
    setCurrentQuestion(0);
    setScore(0);
    menu("/select")
  };
  const currentAnswerOptions = questions[currentQuestion]?.answers || [];
  return (
    <div className='home-container'>
  {showScore && score === 29 ? (
<>
  <div className='score-section'>
    <div id="score-text">
      <p>Complimenti! Hai risposto correttamente a tutte le domande!</p> 
    </div> 
    <button className="button" onClick={handleReturnButtonClick}>Return to Quiz</button>
    <Link to="/resoconto"><button className="button">Resoconto</button></Link>  </div>
</>
) : showScore ? (
<div className='score-section'>
  <div id="score-text">
    <p>Hai perso! Non hai risposto correttamente a tutte le domande!</p>
    <p>Hai totalizzato un punteggio di: {score} su {questions.length}</p>
    <p>Ritenta sarai piu fortunato!</p> 
  </div> 
  <div className="button-box">
    <button className='go-button' onClick={handleReturnButtonClick}>Ritorna ai quiz</button>
    <button className='go-button' onClick={()=>{menu("/impossibile")}}>Riprova</button>
  </div>
</div>
) : (
    <>
     {questions.length > 0 && (
  <div className='question-section'>
  <div className='question-count'>
  <span>Question {currentQuestion+1}</span>/{questions.length}
  <Timer tempo={0.25} funzione={showscore} modalita="impossibile" setcq={setCurrentQuestion} cq={currentQuestion}/>
  </div>
  <div className='question-text'>
      <p>{questions[currentQuestion]?.question}</p>
      {questions[currentQuestion]?.image_url && (
      <img className='img-box' src={getImageUrl(questions[currentQuestion]?.image_url)}/>
      )}
  </div>
</div>
)}

<div className="answer-box">
    <div className='answer-section'>
      {currentAnswerOptions.map((answerOption, index) => (
        <button className='answer-button' id={index}
          key={index}
          onClick={() => {
            handleAnswerOptionClick(answerOption.correct);
          }}
        >
          {answerOption.answer}
        </button>
      ))}
    </div>
  </div>
    </>
  )}
</div>
  );
};
export default Game;