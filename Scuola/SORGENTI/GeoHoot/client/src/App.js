import {React,createContext,useState} from 'react'
import { BrowserRouter as Router, Routes, Route} from "react-router-dom"
import Home from './components/Home'
import Classifica from './components/Classifica'
import Game from './components/Game'
import Select from './components/Select'
import Resoconto from './components/Resoconto'
import Impossibile from './components/Impossibile'
import LastLevel from './components/LastLevel'

export const QuizExport = createContext()
export const QuizResultsContext = createContext([]);
export const TempoExport = createContext();
export const ModalitaExport = createContext();
export const curquest = createContext();
export const UsrContext = createContext();


const App = () => {
  const [quiz, setquiz] = useState();
  const [quizResults, setQuizResults] = useState([]);
  const [t, setT] = useState(0);
  const [cq, setCq] = useState()
  const [modalita, setModalita] = useState("");
  const [Usr, setUsr] = useState("");
  return (
  
  <QuizExport.Provider value={{quiz,setquiz}}>
    <QuizResultsContext.Provider value={{quizResults,setQuizResults}}>
    <TempoExport.Provider value={{t,setT}}>
    <curquest.Provider value={{cq,setCq}}>
    <ModalitaExport.Provider value={{modalita,setModalita}}>
    <UsrContext.Provider value={{Usr,setUsr}}>
   <Router>
   <div className="main">
    <Routes>
      <Route path="/" element={<Home />}/>
      <Route path="/home" element={<Home />}/>
      <Route path="/game" element={<Game />}/>
      <Route path="/select" element={<Select />}/>
      <Route path="/resoconto" element={<Resoconto />}/>
      <Route path="/impossibile" element={<Impossibile />}/>
      <Route path="/finalgame" element={<LastLevel />}/>
      <Route path="/classifica" element={<Classifica/>}/>
    
    </Routes>
    </div>
   </Router>
   </UsrContext.Provider>
   </ModalitaExport.Provider>
   </curquest.Provider>
   </TempoExport.Provider>
   </QuizResultsContext.Provider>
   </QuizExport.Provider>
  )
}

export default App