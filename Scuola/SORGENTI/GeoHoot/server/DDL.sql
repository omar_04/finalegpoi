CREATE DATABASE stig_2023;


CREATE TABLE giocatore (
  username VARCHAR(50) PRIMARY KEY
);

CREATE TABLE partita (
  id_partita INT AUTO_INCREMENT PRIMARY KEY,
  modalita VARCHAR(32),
  punteggio INT,
  username_giocatore VARCHAR(50),
  FOREIGN KEY (username_giocatore) REFERENCES giocatore(username)
);

INSERT INTO giocatore (username)
VALUES ('Teodor'), ('Massano'), ('Salvai'), ('Ciuraru'), ('Agban');