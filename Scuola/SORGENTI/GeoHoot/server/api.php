<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type");

// Connessione al database
$conn = new mysqli('localhost', 'root', '', 'stig_2023');


// Funzione per leggere un record dalla tabella "giocatore"
function get_user($username) {
  global $conn;

  $sql = "SELECT * FROM giocatore WHERE username = '$username'";

  $result = $conn->query($sql);

  $user = array();
  if ($row = $result->fetch_assoc()) {
    $user = $row;
    $user['exists'] = true;
  } else {
    $user['exists'] = false;
  }

  return json_encode($user);
}

// Verifica se è stata passata la variabile "username" tramite la richiesta GET o POST
if (isset($_REQUEST['username'])) {
  $username = $_REQUEST['username'];
  echo get_user($username);
}

?>
