<?php
header("Access-Control-Allow-Origin: *");

header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

header("Access-Control-Allow-Headers: Content-Type");
// Connessione al database
$conn = new mysqli('localhost', 'root', '', 'stig_2023');

// Funzione per ottenere i punteggi dalla tabella partita in ordine decrescente di punteggio e modalità
function get_classifica_by_modalita($modalita) {
    global $conn;
  
    if ($modalita === 'Generale') {
      $sql = "SELECT username_giocatore, punteggio, modalita FROM partita ORDER BY punteggio DESC";
    } else {
      $sql = "SELECT username_giocatore, punteggio FROM partita WHERE modalita = '$modalita' ORDER BY punteggio DESC";
    }
  
    $result = $conn->query($sql);
  
    $classifica = array();
    while ($row = $result->fetch_assoc()) {
      $classifica[] = $row;
    }
  
    return json_encode($classifica);
  }
  
// Verifica se sono state passate le variabili "modalita" tramite la richiesta GET
if (isset($_GET['modalita'])) {
  $modalita = $_GET['modalita'];
  echo get_classifica_by_modalita($modalita);
}
?>
