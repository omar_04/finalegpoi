--Elimino database se esiste
DROP DATABASE IF EXISTS GR01_gymmy;
--Creo database se non esiste
CREATE DATABASE IF NOT EXISTS GR01_gymmy;
--Uso database
USE GR01_gymmy;
--Creo tabella Domande
CREATE TABLE IF NOT EXISTS Domande(
	idDomanda INT AUTO_INCREMENT NOT NULL,
	categoria ENUM('forza','resistenza','velocità') NOT NULL,
	testo VARCHAR(200) NOT NULL,
	difficoltaFk ENUM('facile', 'media', 'difficile'),
	punteggio FLOAT NOT NULL,
	PRIMARY KEY (idDomanda)
);
--Creo tabella Risposte
CREATE TABLE IF NOT EXISTS Risposte(
	idRisposta INT AUTO_INCREMENT NOT NULL,
	testo VARCHAR(100) NOT NULL,
	idDomandaFk INT,
	corretta ENUM('true','false') NOT NULL,
	PRIMARY KEY (idRisposta),
	FOREIGN KEY (idDomandaFk) REFERENCES Domande(idDomanda) ON DELETE CASCADE
);
--Creo tabella Difficolta
CREATE TABLE IF NOT EXISTS Difficolta(
	difficolta ENUM('facile','media','difficile') NOT NULL,
	moltiplicatore FLOAT NOT NULL,
	PRIMARY KEY (difficolta)
);
--Aggiungo fk di difficolta nella tabella Domande
ALTER TABLE Domande ADD CONSTRAINT fk_difficolta FOREIGN KEY (difficoltaFk) REFERENCES Difficolta(difficolta) ON DELETE CASCADE;
--Creo tabella Spiegazioni
CREATE TABLE IF NOT EXISTS Spiegazioni(
	idSpiegazione INT AUTO_INCREMENT NOT NULL,
	testo VARCHAR(500) NOT NULL,
	PRIMARY KEY (idSpiegazione)
);
--Creo tabella giocatori
CREATE TABLE IF NOT EXISTS Giocatori(
	username VARCHAR(15) NOT NULL,
	puntiForza INT NOT NULL DEFAULT 0,
	puntiResistenza INT NOT NULL DEFAULT 0,
	puntiVelocita INT NOT NULL DEFAULT 0,
	PRIMARY KEY (username)
);

CREATE VIEW media_tab(username,media_punteggio) AS  SELECT username,(puntiForza + puntiResistenza + puntiVelocita)/3 AS media_punteggio FROM Giocatori;

