USE GR01_gymmy;

--Elimino tabelle in caso di modifiche, ottimizzazione
--CONTROLLO SU CHIAVE ESTERNA
--SET foreign_key_checks=0;
DELETE FROM Domande;
DELETE FROM Risposte;
DELETE FROM Difficolta;
DELETE FROM Spiegazioni;
--SET foreign_key_checks=1;

--Carico dati con CSV nelle tabelle
LOAD DATA INFILE '/opt/lampp/htdocs/prog5C/MATURITA/GYMMY/dbGimmy/difficolta.csv' INTO TABLE Difficolta FIELDS TERMINATED BY ',';

LOAD DATA INFILE '/opt/lampp/htdocs/prog5C/MATURITA/GYMMY/dbGimmy/domande.csv' INTO TABLE Domande FIELDS TERMINATED BY ',';

LOAD DATA INFILE '/opt/lampp/htdocs/prog5C/MATURITA/GYMMY/dbGimmy/risposte.csv' INTO TABLE Risposte FIELDS TERMINATED BY ',';

LOAD DATA INFILE '/opt/lampp/htdocs/prog5C/MATURITA/GYMMY/dbGimmy/spiegazioni.csv' INTO TABLE Spiegazioni FIELDS TERMINATED BY ',';

