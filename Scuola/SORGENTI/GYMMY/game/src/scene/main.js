import Phaser from 'phaser';
import logopath from '../assets/logo.png'
import sfondopath from '../assets/background_game.jpg'
import character from "../assets/omino.png"
//attrezzi
import desk from "../assets/attrezzi/desk.png"
import bilancieri from "../assets/attrezzi/bilancieri.png"
import sbarra from "../assets/attrezzi/sbarra.png"
import spalliera from "../assets/attrezzi/spalliera.png"
import tapisroulant from "../assets/attrezzi/tapisroulant.png"
import tappetino from "../assets/attrezzi/tappetino.png"

class main extends Phaser.Scene{
	constructor(){
		super("main")
		this.server="http://localhost/Scuola/Gymmy/"
        // this.server="http://localhost/GYMMY/GYMMY_FILES/"
        // this.server="http://localhost/prog5c/GYMMY/GYMMY_FILES/"
		this.direzione = new Phaser.Math.Vector2();
		this.punti={
			forza:0,
			velocita:0,
			resistenza:0,
			overall:0
		}
		this.get_cookie()
			this.velocita = 300; // velocità di movimento
			this.popup=function(elementoPopup,elScritta,gioca){ //toggle popup
			// console.log(elementoPopup.visible)
			if(elementoPopup.visible){
				elementoPopup.setVisible(false)
				elScritta.setVisible(false)
				gioca.setVisible(false)
			}
			else{
				elementoPopup.setVisible(true)
				elScritta.setVisible(true)
				gioca.setVisible(true)
			}
		}
		this.associaPopup=function(elPopup,scritta,attrezzo,gioca){
			elPopup.setVisible(false)
			scritta.setVisible(false)
			gioca.setVisible(false)
			let diff_array=["facile","media","difficile"]
			let random = Math.floor(Math.random() * diff_array.length);
			let diff=diff_array[random]
			gioca.setInteractive({useHandCursor: true}).on('pointerdown', ()=>this.scene.start("quiz",{categoria:attrezzo.categoria,difficolta:diff,punti:this.punti}))
			attrezzo.setInteractive({useHandCursor: true}).on("pointerdown",()=>this.popup(elPopup,scritta,gioca))
		}
		this.associaDocumentazione=function(elPopup,scritta,attrezzo,gioca){
			elPopup.setVisible(false)
			scritta.setVisible(false)
			gioca.setVisible(false)
			gioca.setInteractive({useHandCursor: true}).on('pointerdown', ()=>this.scene.start("documentazione"))
			attrezzo.setInteractive({useHandCursor: true}).on("pointerdown",()=>this.popup(elPopup,scritta,gioca))
		}
		const altezzaPopup=130
		const larghezzaPopup=200
		// const colorePopup="0xffffff"
		const colorePopup="0xffc861"
		const colorebordiPopup="0x1a65ac"
		const bordiPopup=15
		this.modificaPopup=function(posx,posy,elPopup,scritta,gioca){
			//console.log(elPopup)
			elPopup.fillStyle(colorePopup)
			elPopup.lineStyle(5, colorebordiPopup, 1) 
			elPopup.strokeRoundedRect(posx-100,posy-100,larghezzaPopup,altezzaPopup,bordiPopup)
			elPopup.fillRoundedRect(posx-100,posy-100,larghezzaPopup,altezzaPopup,bordiPopup)
			scritta.setX(posx)
			scritta.setY(posy-60)
			gioca.setX(posx)
			gioca.setY(posy-20)
			gioca.on("pointerover",()=>{
				gioca.setAlpha(0.8)
				gioca.setScale(gioca.scale-0.1)
			})
			gioca.on("pointerout",()=>{
				gioca.setAlpha(1)
				gioca.setScale(gioca.scale+0.1)
			})
		}
	}
	init(){
		// this.get_cookie()
	}
	get_cookie(){
		var username_cookie=document.cookie.split(";")
		//check cookie presente
		let pres=false
		username_cookie.forEach((ck)=>{
			if(ck.includes("session_username_GPOI")){
				pres=ck
			}
		})
		if(pres!==false){
			username_cookie=pres.split("=")[1]
			//console.log("Username: "+username_cookie)
			this.get_punti_giocatore(username_cookie)
		}
		else{
			console.log("Cookie assente")
		}
		//this.get_punti_giocatore("omarre") per provare la query senza cookies
	}
	get_punti_giocatore(user){
        let formdata=new FormData()
        formdata.append("user",user)
        let link=this.server+"get_punti_giocatore.php"
        let options={method:"POST",body:formdata}
        fetch(link,options)
        .then(response=>response.json())
        .then(body=>{
			if(body==false){ //giocatore non presente in db
				console.log("Giocatore non presente in DB")
				this.aggiungi_giocatore(user)
			}else{
				//console.log("Punteggi giocatore: ",body)
				this.punti.forza=body.puntiForza
				this.punti.velocita=body.puntiVelocita
				this.punti.resistenza=body.puntiResistenza
				this.punti.overall=Math.floor(body.media_punteggio * 10) / 10 //arrotondamento 1 cifa decimale
				let scritta=`Forza:  ${this.punti.forza}\nVelocita':  ${this.punti.velocita}\nResistenza:  ${this.punti.resistenza}\nOverall: ${this.punti.overall}`
				this.progressText=this.add.text(this.sfondo.x-130,this.sfondo.y-110,scritta,{fontSize:20,fill: "#000", fontFamily: "Bayon"}).setDepth(0.5)
			}
        })
    }
	aggiungi_giocatore(user){
        let formdata=new FormData()
        formdata.append("user",user)
        let link=this.server+"set_nuovo_giocatore.php"
        let options={method:"POST",body:formdata}
        fetch(link,options)
        .then(response=>response.json())
        .then(body=>{
            // console.log("Giocatore aggiunto: ",user)
        })
    }
	preload(){
		this.load.image("logo", logopath)
		this.load.image("bg", sfondopath);
		this.load.image("character",character)
		//attrezzatura
		this.load.image("desk",desk)
		this.load.image("bilancieri",bilancieri) //forza
		this.load.image("sbarra",sbarra) //forza
		this.load.image("spalliera",spalliera) //resistenza
		this.load.image("tapisroulant",tapisroulant) //velocità
		this.load.image("tappetino",tappetino) //resistenza
		//cursore personalizzato http://www.rw-designer.com/cursor-library
		this.input.setDefaultCursor("url(https://cur.cursors-4u.net/food/foo-7/foo632.cur), pointer")
	}
	create(){
		const larghezzaMappa = 1920; // imposta la larghezza della mappa in pixel
		const altezzaMappa = 1080; // imposta l'altezza della mappa in pixel
        this.altezzaCanvas=this.sys.game.canvas.height
        this.larghezzaCanvas=this.sys.game.canvas.width
		var x = window.innerWidth/2;
		var y = window.innerHeight/2;
		this.sfondo = this.add.image(0,0, "bg").setScale(1)
		// Imposta i limiti del mondo
		this.physics.world.setBounds(x-800, 0, larghezzaMappa-300, altezzaMappa-300);


		Phaser.Display.Align.In.Center(this.sfondo, this.add.zone(x,y, window.innerWidth, window.innerHeight));
		//posizione attrezzi
		let xDesk=this.sfondo.x+700
		let yDesk=this.sfondo.y-400
		let xBilancieri=this.sfondo.x-500
		let yBilancieri=this.sfondo.y-100
		let xSbarra=this.sfondo.x+200
		let ySbarra=this.sfondo.y+300
		let xSpalliera=this.sfondo.x+100
		let ySpalliera=this.sfondo.y-410
		let xTapisroulant=this.sfondo.x+700
		let yTapisroulant=this.sfondo.y+200
		let xTappetino=this.sfondo.x-400
		let yTappetino=this.sfondo.y+300

		//ATTREZZATURA
		this.desk=this.crea_attrezzo(xDesk,yDesk,"desk","Desk","DOCUMENTAZIONE")
		this.bilancieri=this.crea_attrezzo(xBilancieri,yBilancieri,"bilancieri","Bilancieri","GIOCA ORA")
		this.bilancieri.categoria="Forza"
		this.sbarra=this.crea_attrezzo(xSbarra,ySbarra,"sbarra","Sbarra","GIOCA ORA")
		this.sbarra.categoria="Forza"
		this.spalliera=this.crea_attrezzo(xSpalliera,ySpalliera,"spalliera","Spalliera","GIOCA ORA")
		this.spalliera.categoria="Resistenza"
		this.tapisroulant=this.crea_attrezzo(xTapisroulant,yTapisroulant,"tapisroulant","TapisRoulant","GIOCA ORA")
		this.tapisroulant.categoria="Velocita"
		this.tappetino=this.crea_attrezzo(xTappetino,yTappetino,"tappetino","Tappetino","GIOCA ORA")
		this.tappetino.categoria="Resistenza"

		// aggiunta del personaggio
		this.character=this.physics.add.sprite(this.larghezzaCanvas/2,this.altezzaCanvas/2,"character").setDepth(1);
		this.character.setScale(0.5);
		this.character.setCollideWorldBounds(true);

		// imposta i tasti
		this.W = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
		this.A = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
		this.S = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
		this.D = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);

		// imposta i limiti della telecamera sulla dimensione della mappa
		this.cameras.main.startFollow(this.character, true, 0.5, 0.5);
		this.cameras.main.setZoom(1.5);

		//popup progressi
		this.progress = this.add.rectangle(this.sfondo.x,this.sfondo.y, 300, 250,0xffc861).setDepth(0).setOrigin(0.5,0.5)
		this.progress.setStrokeStyle(4, 0x1a65ac);
		this.get_cookie()
		this.progressWelcome=this.add.text(this.sfondo.x,this.sfondo.y+50,`Benvenuto in`,{fontSize:30,fill: "#000", fontFamily: "Bayon"}).setDepth(0.5).setOrigin(0.5)
		this.logo = this.add.sprite(this.sfondo.x, this.sfondo.y+90, "logo").setOrigin(0.5).setScale(0.15)
	}
	crea_attrezzo(x,y,img,testo,testoPopupButton){
		var attrezzo
		attrezzo=this.physics.add.sprite(x,y,img);
		var popupAttr=this.add.graphics()
		var scrittaAttr=this.add.text(0,0,testo,{fontSize:22,fill: "#000", fontFamily: "Bayon"}).setOrigin(0.5,0.5)
		var giocaOra=this.add.text(0,0,testoPopupButton,{fontSize:25,fill: "#000", fontFamily: "Bayon"}).setOrigin(0.5,0.5).setInteractive({useHandCursor: true}).on("pointerdown",()=>{}).setPadding(0)
		this.modificaPopup(x,y,popupAttr,scrittaAttr,giocaOra)
		if(testo==="Desk"){
			this.associaDocumentazione(popupAttr,scrittaAttr,attrezzo,giocaOra)
		} else {
			this.associaPopup(popupAttr,scrittaAttr,attrezzo,giocaOra)
		}
		attrezzo.setScale(0.8);
		attrezzo.on("pointerover",()=>{
			attrezzo.setAlpha(0.8)
			attrezzo.setScale(attrezzo.scale-0.1)
		})
		attrezzo.on("pointerout",()=>{
			attrezzo.setAlpha(1)
			attrezzo.setScale(attrezzo.scale+0.1)
		})
		return attrezzo
	}
	update(time,delta){
		if(this.W.isDown){
			this.character.setVelocityY(-this.velocita)
			this.character.angle=0
		} else if(this.S.isDown){
			this.character.setVelocityY(this.velocita)
			this.character.angle=180
		}
		else {
			this.character.setVelocity(0)
		}
		if(this.D.isDown){
			this.character.setVelocityX(this.velocita)
			this.character.angle=90
		} else if(this.A.isDown){
			this.character.setVelocityX(-this.velocita)
			this.character.angle=-90}
		else {
			this.character.setVelocityX(0)
		}
		//rotazione immagine diagonale
		if(this.W.isDown && this.D.isDown){
			this.character.angle=45
		} else if(this.W.isDown && this.A.isDown){
			this.character.angle=-45
		}
		else if(this.S.isDown && this.D.isDown){
			this.character.angle=135
		}
		else if(this.S.isDown && this.A.isDown){
			this.character.angle=-135
		}
	}
}

export default main
