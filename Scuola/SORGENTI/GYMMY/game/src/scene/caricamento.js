import Phaser from 'phaser'

import barra1path from '../assets/caricamento/barra_caricamento/barra1.png'
import barra2path from '../assets/caricamento/barra_caricamento/barra2.png'
import barra3path from '../assets/caricamento/barra_caricamento/barra3.png'
import barra4path from '../assets/caricamento/barra_caricamento/barra4.png'
import barra5path from '../assets/caricamento/barra_caricamento/barra5.png'

import omino1path from '../assets/caricamento/loading/loading_sollevamento_1.png'
import omino2path from '../assets/caricamento/loading/loading_sollevamento_2.png'

class caricamento extends Phaser.Scene{
    constructor (){
        super("caricamento");
    }

    preload (){
        //caricamento barra
        this.load.image('barra1', barra1path);
        this.load.image('barra2', barra2path);
        this.load.image('barra3', barra3path);
        this.load.image('barra4', barra4path);
        this.load.image('barra5', barra5path);
        //caricamento omino
        this.load.image('omino1', omino1path);
        this.load.image('omino2', omino2path);
    }

    create (){
        var x = window.innerWidth/2;
		var y = window.innerHeight/2;
        this.sfondo = this.add.image(x, y, "sfondo").setOrigin(0.5).setScale(2)

        this.anims.create({
            key: 'solleva',
            frames: [
                { key: 'omino1' },
                { key: 'omino2', duration: 50 }
            ],
            frameRate: 8,
            repeat: -1
        });

        this.add.sprite(x, y-150, 'omino1').setOrigin(0.5,0.5).setScale(1).play('solleva');

        this.anims.create({
            key: 'carica',
            frames: [
                { key: 'barra1' },
                { key: 'barra2' },
                { key: 'barra3', duration: 100 },
                { key: 'barra4' },
                { key: 'barra5', duration: 50 }
            ],
            frameRate: 3,
        });

        this.logo = this.add.sprite(x, y+150, "logo").setOrigin(0.5,0.5).setScale(0.3)
        this.barra=this.add.sprite(x, y+275, 'barra1').setOrigin(0.5,0.5).setScale(0.4).play('carica');
        this.barra.on('animationcomplete-carica', function () {
            // passa alla scena successiva
            this.scene.start('main');
        }, this);
    }

    update(){
    }
}

export default caricamento