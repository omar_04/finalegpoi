import Phaser from 'phaser';
import sfondopath from '../assets/bg_schermate.jpg'

var info={}

class spiegazione extends Phaser.Scene{
	constructor(){
		super("spiegazione")
        this.server="http://localhost/Scuola/Gymmy/"
        // this.server="http://localhost/prog5C/prove_MATURITA/GYMMY/GYMMY_FILES/"
        // this.server="http://localhost/GYMMY/GYMMY_FILES/"
        this.dati={}
	}
    init(data){
        this.dati=data
        info={spiegazioni: ""}
    }
    //funzione per scaricare le spiegazioni
    scarica_spiegazioni(categoria){
        let formdata=new FormData()
        formdata.append("categoria",categoria)
        let link=this.server+"get_spiegazioni.php"
        let options={method:"POST",body:formdata}
        fetch(link,options)
        .then(response=>response.json())
        .then(spiegazioni=>{
            if(spiegazioni===false){
                alert("Errore nella ricezione delle spiegazioni")
            }
            else{
                spiegazioni.forEach(spi => {
                    spi.testo=spi.testo.replace(new RegExp('è', "g"),"e'").replace(new RegExp('à', "g"),"a'").replace(new RegExp('ù', "g"),"u'").replace(new RegExp('È', "g"),"E'")
                    info.spiegazioni+=spi.testo+".\n"
                });
                var x = window.innerWidth/2;
                var y = window.innerHeight/2;
                this.add.text(x,y+50,info.spiegazioni,{fontSize:20,fill: "#000", fontFamily: "Bayon", lineSpacing: 34, wordWrap: { width: window.innerWidth-150, useAdvancedWrap: true }}).setOrigin(0.5,0.5).setPadding(20).setBackgroundColor('#fff')
            }
        })
    }
    back(){
        this.scene.start("main")
    }
	preload(){
		this.load.image("sfondo", sfondopath);
	}
	create(){
        var x = window.innerWidth/2;
		var y = window.innerHeight/2;
		this.sfondo = this.add.image(x, y, "sfondo").setOrigin(0.5).setScale(2)
        this.btnBack = this.add.text(x+350,y-300, "TORNA INDIETRO",{fontSize:20,fill: "#fff", fontFamily: "Bayon", wordWrap: { width: 350, useAdvancedWrap: true }}).setOrigin(0.5,0.5).setPadding(20).setBackgroundColor('#000').setInteractive({useHandCursor: true}).on("pointerdown",()=>{
            this.back()
        }).on("pointerover",()=>{
			this.btnBack.setScale(this.btnBack.scale-0.1)
		}).on("pointerout",()=>{
			this.btnBack.setScale(this.btnBack.scale+0.1)
		})
        this.titolo=this.add.text(x-470,y-360,this.dati.categoria,{fontSize:80,fill: "#fff", fontFamily: "Bayon", wordWrap: { width: window.innerWidth-250, useAdvancedWrap: true }}).setPadding(20)
        this.scarica_spiegazioni(this.dati.categoria)
    }
	update(time,delta){
        
	}
}

export default spiegazione
