import Phaser from 'phaser';
import logopath from '../assets/logo.png'
import sfondopath from '../assets/bg_schermate.jpg'
import bottonepath from '../assets/bottone.png'

class documentazione extends Phaser.Scene{
	constructor(){
		super("documentazione") //nome della scena
	}
	preload(){ //caricamento immagini in memoria
		this.load.image("sfondo", sfondopath)
	}
	create(){
		var x = window.innerWidth/2;
		var y = window.innerHeight/2;
		this.sfondo = this.add.image(x, y, "sfondo").setOrigin(0.5).setScale(2)
		this.logo = this.add.sprite(x, y-250, "logo").setOrigin(0.5).setScale(0.5)

        //titolo documentazione
        this.titoloDocu=this.add.text(x,y-100,"DOCUMENTAZIONE",{fontSize:80,fill: "#fff", fontFamily: "Bayon", wordWrap: { width: window.innerWidth-250, useAdvancedWrap: true }}).setOrigin(0.5,0.5).setPadding(20)

        //btn menu che richiamano scena spiegazione.js con passaggio di parametri per parametrizzare la query
        this.docu1=this.add.text(x,y+50,"FORZA",{fontSize:40,fill: "#fff", fontFamily: "Bayon", align: "center", wordWrap: { width: window.innerWidth-250, useAdvancedWrap: true }}).setOrigin(0.5,0.5).setPadding(20).setBackgroundColor('#000').setFixedSize(300, 80).setInteractive({useHandCursor: true}).on("pointerdown",()=>{
            this.next('forza')
        }).on("pointerover",()=>{
			this.docu1.setScale(this.docu1.scale-0.1)
		}).on("pointerout",()=>{
			this.docu1.setScale(this.docu1.scale+0.1)
		})
        this.docu2=this.add.text(x,y+150,"RESISTENZA",{fontSize:40,fill: "#fff", fontFamily: "Bayon", align: "center", wordWrap: { width: window.innerWidth-250, useAdvancedWrap: true }}).setOrigin(0.5,0.5).setPadding(20).setBackgroundColor('#000').setFixedSize(300, 80).setInteractive({useHandCursor: true}).on("pointerdown",()=>{
            this.next('resistenza')
        }).on("pointerover",()=>{
			this.docu2.setScale(this.docu2.scale-0.1)
		}).on("pointerout",()=>{
			this.docu2.setScale(this.docu2.scale+0.1)
		})
        this.docu3=this.add.text(x,y+250,"VELOCITA'",{fontSize:40,fill: "#fff", fontFamily: "Bayon", align: "center",  wordWrap: { width: window.innerWidth-250, useAdvancedWrap: true }}).setOrigin(0.5,0.5).setPadding(20).setBackgroundColor('#000').setFixedSize(300, 80).setInteractive({useHandCursor: true}).on("pointerdown",()=>{
            this.next('velocita')
        }).on("pointerover",()=>{
			this.docu3.setScale(this.docu3.scale-0.1)
		}).on("pointerout",()=>{
			this.docu3.setScale(this.docu3.scale+0.1)
		})
	}
    next(param){
        this.scene.start("spiegazione",{categoria: param})
    }
	update(time,delta){ //loop infinito
		
	}
}

export default documentazione