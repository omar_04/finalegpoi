import Phaser from 'phaser';
import logopath from '../assets/logo.png'
import sfondopath from '../assets/bg_schermate.jpg'
import bottonepath from '../assets/bottone.png'

class giocaora extends Phaser.Scene{
	constructor(){
		super("giocaora") //nome della scena
	}
	preload(){ //caricamento immagini in memoria
		this.load.image("sfondo", sfondopath)
		this.load.image("logo", logopath)
		this.load.image("bottone", bottonepath)
	}
	create(){
		var x = window.innerWidth/2;
		var y = window.innerHeight/2;
		this.sfondo = this.add.image(x, y, "sfondo").setOrigin(0.5).setScale(2)
		this.logo = this.add.sprite(x, y-150, "logo").setOrigin(0.5)
		this.bottone = this.add.sprite(x, y+150, "bottone").setOrigin(0.5).setInteractive({useHandCursor: true}).on('pointerdown', ()=>this.scene.start("caricamento")).on("pointerover",()=>{
			this.bottone.setScale(this.bottone.scale-0.1)
			this.giocaOra.setFontSize(40)
		}).on("pointerout",()=>{
			this.bottone.setScale(this.bottone.scale+0.1)
			this.giocaOra.setFontSize(45)
		})
		this.giocaOra = this.add.text(x, y+150, "GIOCA ORA", {fontSize:45, fontFamily: "Bayon", fill: "#fff"}).setOrigin(0.5)
	}
	update(time,delta){ //loop infinito
		
	}
}

export default giocaora