import Phaser from 'phaser';
import sfondopath from '../assets/bg_schermate.jpg'

var info={domande:{},risposte:{}}
var num_domanda=0
var index;
var moltiplicatore=1
var username_cookie
class quiz extends Phaser.Scene{
	constructor(){
		super("quiz")
        this.server="http://localhost/Scuola/Gymmy/"
        this.serverGlobal="http://localhost/Scuola/SITI/FILE_PHP/"
        // this.server="http://localhost/GYMMY/GYMMY_FILES/"
        // this.server="http://localhost/prog5c/GYMMY/GYMMY_FILES/"
        this.punteggio_aggiornare=""
        this.punti=0
        this.diff="facile"
	}
    init(data){
        // data={categoria:"Forza",difficolta:"facile",punti:0}
        //console.log("Dati: ",data)
        this.diff=data.difficolta
        this.punteggio_aggiornare=`punti`+data.categoria
		this.get_cookie()
        this.scarica_domande(data.categoria,data.difficolta)
        this.punti=data.punti
        info={domande:{},risposte:{}}
        num_domanda=0
        index=0;
        moltiplicatore=1
    }
    scarica_domande(cat="forza",diff="facile"){
        let formdata=new FormData()
        formdata.append("categoria",cat)
        formdata.append("difficolta",diff)
        let link=this.server+"get_domande.php"
        let options={method:"POST",body:formdata}
        fetch(link,options)
        .then(response=>response.json())
        .then(domande=>{
            if(domande===false){
                alert("Errore nella ricezione delle domande")
            }
            else{
                domande.forEach(dom => {
                    dom.testo=dom.testo.replace(new RegExp('è', "g"),"e'").replace(new RegExp('à', "g"),"a'").replace(new RegExp('ù', "g"),"u'")
                    info.domande[dom.idDomanda]=dom
                    this.scarica_risposte(dom.idDomanda)
                });
            }
        })
    }
    scarica_risposte(idDom=1){
        let formdata=new FormData()
        formdata.append("idDomanda",idDom)
        let link=this.server+"get_risposte.php"
        let options={method:"POST",body:formdata}
        fetch(link,options)
        .then(response=>response.json())
        .then(risposte=>{
            if(risposte===false){
                alert("Errore nella ricezione delle risposte")
            }
            else{
                risposte.forEach(ris => {
                    ris.testo=ris.testo.replace(new RegExp('è', "g"),"e'").replace(new RegExp('à', "g"),"a'").replace(new RegExp('ù', "g"),"u'")
                });
                info.risposte[idDom]=risposte
            }
        })
    }
    get_moltiplicatore(diff="facile"){
        let formdata=new FormData()
        formdata.append("difficolta",diff)
        let link=this.server+"get_moltiplicatore.php"
        let options={method:"POST",body:formdata}
        fetch(link,options)
        .then(response=>response.json())
        .then(body=>{
            moltiplicatore=body.moltiplicatore
        })
    }
    get_punti_giocatore(user){
        let formdata=new FormData()
        formdata.append("user",user)
        let link=this.server+"get_punti_giocatore.php"
        let options={method:"POST",body:formdata}
        fetch(link,options)
        .then(response=>response.json())
        .then(body=>{
            //console.log("Punteggi giocatore: ",body)
            this.punti=body[this.punteggio_aggiornare]
        })
    }
    get_cookie(){
		username_cookie=document.cookie.split(";")
		//check cookie presente
		let pres=false
		username_cookie.forEach((ck)=>{
			if(ck.includes("session_username_GPOI")){
				pres=ck
			}
		})
		if(pres!==false){
			username_cookie=pres.split("=")[1]
			//console.log("Username: "+username_cookie)
			this.get_punti_giocatore(username_cookie)
		}
		else{
			//console.log("Cookie assente")
		}
		//this.get_punti_giocatore("omarre") per provare la query senza cookies
	}
    
    update_punti_giocatore(user){
        var formdata=new FormData()
        formdata.append("user",user)
        formdata.append("tipo",this.punteggio_aggiornare)
        formdata.append("valore",this.punti)
        let link=this.server+"set_punti_giocatore.php"
        var options={method:"POST",body:formdata}
        fetch(link,options) //aggiorna singolo punteggio
        .then(response=>response.json())
        .then(body=>{
                let link=this.server+"get_punti_giocatore.php"
                fetch(link,options) //ricevi punteggi aggiornati
                .then(response=>response.json())
                .then(punti_ora=>{
                    var overall=Math.floor(punti_ora.media_punteggio * 10) / 10 //arrotondamento 1 cifa decimale
                    let link=this.serverGlobal+"get_stats.php"
                    fetch(link,options) //ricevi punteggi aggiornati dal globale
                    .then(response=>response.json())
                    .then(punti_globali=>{
                        let overallGlobal=Math.floor(punti_globali.punteggio * 10) / 10 //arrotondamento 1 cifa decimale
                        if(overall>=overallGlobal){
                            formdata.append("gioco","Gymmy")
                            formdata.append("punti",overall)
                            let link=this.serverGlobal+"set_stats.php"
                            fetch(link,options) //setta punteggio globale aggiornato
                            .then(response=>response.json())
                            .then(body=>{
                                console.log("body aggiornato")
                            })
                        }
                    })
                })
            }
        );
    }
	preload(){
		this.load.image("sfondo", sfondopath);
	}
	create(){
        this.altezzaCanvas=this.sys.game.canvas.height
        this.larghezzaCanvas=this.sys.game.canvas.width
		var x = window.innerWidth/2;
		var y = window.innerHeight/2;
		this.sfondo = this.add.image(x, y, "sfondo").setOrigin(0.5).setScale(2)
        this.domanda = this.add.text(x,y-150,"",{fontSize:30, fill: "#000",align:"center", fontFamily: "Bayon", wordWrap: { width: window.innerWidth-250, useAdvancedWrap: true }}).setOrigin(0.5,0.5).setPadding(30).setBackgroundColor('#fff')
        this.risp1=this.add.text(x,y+50,"",{fontSize:20,fill: "#fff", fontFamily: "Bayon", wordWrap: { width: window.innerWidth-250, useAdvancedWrap: true }}).setOrigin(0.5,0.5).setPadding(20).setBackgroundColor('#000').setInteractive({useHandCursor: true}).on("pointerdown",()=>{
            this.next('risp1')
        }).on("pointerover",()=>{
			this.risp1.setScale(this.risp1.scale-0.1)
		}).on("pointerout",()=>{
			this.risp1.setScale(this.risp1.scale+0.1)
		})
        this.risp2=this.add.text(x,y+150,"",{fontSize:20,fill: "#fff", fontFamily: "Bayon", wordWrap: { width: window.innerWidth-250, useAdvancedWrap: true }}).setOrigin(0.5,0.5).setPadding(20).setBackgroundColor('#000').setInteractive({useHandCursor: true}).on("pointerdown",()=>{
            this.next('risp2')
        }).on("pointerover",()=>{
			this.risp2.setScale(this.risp2.scale-0.1)
		}).on("pointerout",()=>{
			this.risp2.setScale(this.risp2.scale+0.1)
		})
        this.risp3=this.add.text(x,y+250,"",{fontSize:20,fill: "#fff", fontFamily: "Bayon", wordWrap: { width: window.innerWidth-250, useAdvancedWrap: true }}).setOrigin(0.5,0.5).setPadding(20).setBackgroundColor('#000').setInteractive({useHandCursor: true}).on("pointerdown",()=>{
            this.next('risp3')
        }).on("pointerover",()=>{
			this.risp3.setScale(this.risp3.scale-0.1)
		}).on("pointerout",()=>{
			this.risp3.setScale(this.risp3.scale+0.1)
		})

        //counter domande
        this.counter = this.add.text(x,100, "Domande", {fontSize:40,align:"center", fill: "#000", fontFamily: "Bayon"}).setOrigin(0.5,0.5)

        //button fine
        this.btnFine = this.add.text(x,y+100, "FINE", {fontSize:40,align:"center", fill: "#000", fontFamily: "Bayon"}).setOrigin(0.5,0.5).setVisible(false).setPadding(50,10).setBackgroundColor('#fff')
        this.btnFine.setInteractive({useHandCursor: true}).on("pointerdown",()=>{
            this.update_punti_giocatore(username_cookie)
            this.scene.start("main")
        }).on("pointerover",()=>{
			this.btnFine.setScale(this.btnFine.scale-0.1)
		}).on("pointerout",()=>{
			this.btnFine.setScale(this.btnFine.scale+0.1)
		})
    }
    next(button){
        let btn=this[button]
        let giusta=btn.corretta
        let indice_attuale=index     
        // console.log(info.domande[indice_attuale].difficoltaFk)
        this.get_moltiplicatore(info.domande[indice_attuale].difficoltaFk)   
        if(giusta==="true"){
            // btn.style.backgroundColor='#00ff00'
            this.punti+=(info.domande[indice_attuale].punteggio*moltiplicatore)
            if(this.punti>100){
                this.punti=100
            }
        }
        else {
            this.punti-=(info.domande[indice_attuale].punteggio*moltiplicatore)
            if(this.punti<0){
                this.punti=0
            }
        }  
        // setTimeout(() => {
            num_domanda++
        // }, 2000);
    }
	update(time,delta){
        setTimeout(() => {
            if(Object.keys(info.domande).length==3){
                if(num_domanda<3){
                    index=Object.keys(info.domande)[num_domanda] //index=id domanda
                    // console.log(info)
                    this.domanda.text=info.domande[index].testo
                    this.risp1.text=info.risposte[index][0].testo
                    this.risp1.corretta=info.risposte[index][0].corretta
                    this.risp2.text=info.risposte[index][1].testo
                    this.risp2.corretta=info.risposte[index][1].corretta
                    if(info.risposte[index][2]!==undefined){
                        this.risp3.text=info.risposte[index][2].testo
                        this.risp3.corretta=info.risposte[index][2].corretta
                        this.risp3.setVisible(true)
                    }else{
                        this.risp3.setVisible(false)
                    }
                    //counter
                    this.counter.text=`Domande: ${num_domanda+1}/3     Difficolta': ${this.diff}    Punti: ${this.punti}`
                }else{
                    //counter
                    this.counter.text=`Domande: 3/3\n\nPunti: ${this.punti}`
                    this.counter.setY(window.innerHeight/2-100)
                    this.counter.setFontSize(60)
                    this.btnFine.setVisible(true)

                    //elimino gli elementi alla fine del quiz
                    this.domanda.destroy()
                    this.risp1.destroy()
                    this.risp2.destroy()
                    this.risp3.destroy()
                }
                
            }
        }, 500);
	}
}

export default quiz


// function scarica_spiegazioni(){
//     let link=server+"get_spiegazioni.php"
//     console.log(link)
//     fetch(link)
//     .then(response=>response.json())
//     .then(testi=>{
//         if(testi===false){
//             alert("Errore nella ricezione dei testi")
//         }
//         else{
//             info.spiegazioni=testi
//             console.log(info)
//         }
//     })
// }
