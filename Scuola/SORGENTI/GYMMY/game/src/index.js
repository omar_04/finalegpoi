import giocaora from "./scene/giocaora"
import caricamento from "./scene/caricamento"
import main from "./scene/main"
import quiz from "./scene/quiz"
import documentazione from "./scene/documentazione"
import spiegazione from "./scene/spiegazione"

//Esempi https://phaser.io/examples/v3

var config = {
    type: Phaser.AUTO,
    scale: {
        mode: Phaser.Scale.FIT,
        parent: 'contenitore',
        autoCenter: Phaser.Scale.CENTER_BOTH,
        width: window.innerWidth,
        height: window.innerHeight
    },
    physics: {
		default: 'arcade',
		arcade:{/*debug:true*/},
	},
    // scene: [documentazione,spiegazione]
    // scene: [main,quiz,documentazione,spiegazione]
    scene: [giocaora,caricamento,main,quiz,documentazione,spiegazione]
};
var game = new Phaser.Game(config);
