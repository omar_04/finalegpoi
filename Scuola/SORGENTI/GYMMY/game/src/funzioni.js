var server="http://localhost/prog5c/MATURITA/GYMMY/GYMMY_FILES/"


function get_moltiplicatore(diff="facile"){
    let formdata=new FormData()
    formdata.append("difficolta",diff)
    let link=server+"./get_moltiplicatore.php"
    let options={method:"POST",body:formdata}
    fetch(link,options)
    .then(response=>response.json())
    .then(body=>{
        var moltiplicatore=body.moltiplicatore
        console.log(moltiplicatore)
    })
}

function scarica_domande(cat="forza",diff="facile"){
    let formdata=new FormData()
    formdata.append("categoria",cat)
    formdata.append("difficolta",diff)
    let link=server+"./get_domande.php"
    let options={method:"POST",body:formdata}
    fetch(link,options)
    .then(response=>response.json())
    .then(domande=>{
        if(domande===false){
            alert("Errore nella ricezione delle domande")
        }
        else{
            domande.forEach(dom => {
                info.domande[dom.idDomanda]=dom
                scarica_risposte(dom.idDomanda)
            });
        }
    })
}

function scarica_risposte(idDom=1){
    let formdata=new FormData()
    formdata.append("idDomanda",idDom)
    let link=server+"./get_risposte.php"
    let options={method:"POST",body:formdata}
    fetch(link,options)
    .then(response=>response.json())
    .then(risposte=>{
        if(risposte===false){
            alert("Errore nella ricezione delle risposte")
        }
        else{
            info.risposte[idDom]=risposte
        }
    })
}

function scarica_spiegazioni(){
    let link=server+"./get_spiegazioni.php"
    fetch(link)
    .then(response=>response.json())
    .then(testi=>{
        if(testi===false){
            alert("Errore nella ricezione dei testi")
        }
        else{
            info.spiegazioni=testi
            console.log(info)
        }
    })
}

export function prova(){
    alert("ciao")
}