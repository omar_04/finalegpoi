<?php
	require("./config.php");
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$user=$_POST['user'];
		$tipo=$_POST['tipo']; //es puntiForza
		$nuovo_valore=$_POST['valore']; //nuovo valore del punteggio
		set($user,$tipo,$nuovo_valore);
		chiudi_connessione();
    }
	function set($user,$tipo,$nuovo_valore){ //modifica punteggio specifico del giocatore
		global $conn;
		//preparazione query
		$query="UPDATE Giocatori SET ".$tipo."=? WHERE Giocatori.username=?"; //query
		$stmt=$conn->prepare($query);
		if(!$stmt){
			die("Preparazione query fallita: ".$conn->error);
		}
		$stmt->bind_param("ss",$nuovo_valore,$user); 
		$stmt->execute();
		$result=$stmt->get_result();
		echo json_encode(true);
	}
?>