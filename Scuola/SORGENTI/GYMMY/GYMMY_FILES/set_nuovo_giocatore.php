<?php
	require("./config.php");
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$user=$_POST['user'];
		set($user);
		chiudi_connessione();
    }
	function set($user){ //crea giocatore in tabella
		global $conn;
		//preparazione query
		$query="INSERT INTO Giocatori (username, puntiForza, puntiResistenza, puntiVelocita) VALUES (?, '0', '0', '0');"; //query
		$stmt=$conn->prepare($query);
		if(!$stmt){
			die("Preparazione query fallita: ".$conn->error);
		}
		$stmt->bind_param("s",$user); 
		$stmt->execute();
		$result=$stmt->get_result();
		echo json_encode(true);
	}
?>