<?php
	require("./config.php");
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$user=$_POST['user'];
		get($user);
		chiudi_connessione();
    }
	function get($user){ //restituisce tutti i punti di un giocatore e il suo overall
		global $conn;
		//preparazione query
		$query="SELECT *,media_punteggio FROM Giocatori,media_tab WHERE Giocatori.username=? AND Giocatori.username=media_tab.username"; //query
		$stmt=$conn->prepare($query);
		if(!$stmt){
			die("Preparazione query fallita: ".$conn->error);
		}
		$stmt->bind_param("s",$user); 
		$stmt->execute();
		$result=$stmt->get_result();
		if($result->num_rows==0){ //giocatore non esiste
			echo json_encode(false);
		}else{
			$array=$result->fetch_all(MYSQLI_ASSOC);
			echo json_encode($array[0]);
		}
	}

?>