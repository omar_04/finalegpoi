<?php
	require("./config.php");
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$id=$_POST['idDomanda'];
		get($id);
		chiudi_connessione();
    }
	function get($id){
		global $conn;
		//preparazione query
		$query="SELECT * FROM Risposte WHERE idDomandaFk=?"; //prepared query
		$stmt=$conn->prepare($query);
		if(!$stmt){
			die("Preparazione query fallita: ".$conn->error);
		}
		$stmt->bind_param("s",$id); 
		$stmt->execute();
		$result=$stmt->get_result();
		if($result->num_rows==0){
			echo json_encode(false);
		}else{
			$array=$result->fetch_all(MYSQLI_ASSOC);
			echo json_encode($array);
		}
	}
?>