<?php
    $host_ip="localhost";
    $db_name="Stig_2023";
    $db_username="root";
    $db_password="";

    //connessione al DBMS con scelta del DB
    $conn=new mysqli($host_ip,$db_username,$db_password,$db_name);
    if($conn->connect_error){
        die("Connessione fallita: ".$conn->connect_error);
    };
    function chiudi_connessione(){
        global $conn;
        mysqli_close($conn);
    }
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST');

    //debug errors
    ini_set('error_reporting', E_ALL);
    ini_set( 'display_errors', 1 );
?>