<?php
	require("./config.php");
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $diff=$_POST['difficolta'];
		get($diff);
		chiudi_connessione();
    }
	function get($diff){
		global $conn;
		//preparazione query
		$query="SELECT * FROM gr1_difficolta WHERE difficolta=?"; //prepared query
		$stmt=$conn->prepare($query);
		if(!$stmt){
			die("Preparazione query fallita: ".$conn->error);
		}
		$stmt->bind_param("s",$diff); 
		$stmt->execute();
		$result=$stmt->get_result();
		if($result->num_rows==0){
			echo json_encode(false);
		}else{
			$array=$result->fetch_all(MYSQLI_ASSOC);
			echo json_encode($array[0]);
		}
	}
?>