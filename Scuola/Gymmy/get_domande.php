<?php
	require("./config.php");
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $diff=$_POST['difficolta'];
        $categoria=$_POST['categoria'];
		get($diff,$categoria);
		chiudi_connessione();
    }
	function get($diff,$categoria){
		global $conn;
		//preparazione query
		$query="SELECT * FROM gr1_domande WHERE categoria=? AND difficoltaFk=? ORDER BY RAND() LIMIT 3"; //prepared query
		$stmt=$conn->prepare($query);
		if(!$stmt){
			die("Preparazione query fallita: ".$conn->error);
		}
		$stmt->bind_param("ss",$categoria,$diff); 
		$stmt->execute();
		$result=$stmt->get_result();
		if($result->num_rows==0){
			echo json_encode(false);
		}else{
			$array=$result->fetch_all(MYSQLI_ASSOC);
			echo json_encode($array);
		}
	}
?>