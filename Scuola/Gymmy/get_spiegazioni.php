<?php
	require("./config.php");
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$cat=$_POST['categoria'];
		get(get_xy_from_cat($cat));
		chiudi_connessione();
    }
	function get($xy){ //restituisce tutti i testi di una categoria
		global $conn;
		//preparazione query
		$x=$xy['x'];
		$y=$xy['y'];
		$query="SELECT * FROM gr1_spiegazioni WHERE idSpiegazione>=".$x." AND idSpiegazione<=".$y; //query
		$stmt=$conn->prepare($query);
		if(!$stmt){
			die("Preparazione query fallita: ".$conn->error);
		}
		$stmt->execute();
		$result=$stmt->get_result();
		if($result->num_rows==0){
			echo json_encode(false);
		}else{
			$array=$result->fetch_all(MYSQLI_ASSOC);
			echo json_encode($array);
		}
	}

	function get_xy_from_cat($cat){ //resituisce i 2 valori degli id riguardanti una categoria
		$xy=array('x'=>0,'y'=>1);
		switch ($cat) {
			case 'forza':
				$xy['x']=1;
				$xy['y']=9;
				break;
			case 'resistenza':
				$xy['x']=10;
				$xy['y']=19;
				break;
			case 'velocita':
				$xy['x']=20;
				$xy['y']=29;
				break;
			default:
				break;
		}
		return $xy;
	}
?>