USE Stig_2023;

--Elimino tabelle in caso di modifiche, ottimizzazione
--CONTROLLO SU CHIAVE ESTERNA
--SET foreign_key_checks=0;
DELETE FROM gr1_domande;
DELETE FROM gr1_risposte;
DELETE FROM gr1_difficolta;
DELETE FROM gr1_spiegazioni;
--SET foreign_key_checks=1;

--Carico dati con CSV nelle tabelle
LOAD DATA INFILE '/opt/lampp/htdocs/Scuola/SQL/DML/gr1_difficolta.csv' INTO TABLE gr1_difficolta FIELDS TERMINATED BY ',';

LOAD DATA INFILE '/opt/lampp/htdocs/Scuola/SQL/DML/gr1_domande.csv' INTO TABLE gr1_domande FIELDS TERMINATED BY ',';

LOAD DATA INFILE '/opt/lampp/htdocs/Scuola/SQL/DML/gr1_risposte.csv' INTO TABLE gr1_risposte FIELDS TERMINATED BY ',';

LOAD DATA INFILE '/opt/lampp/htdocs/Scuola/SQL/DML/gr1_spiegazioni.csv' INTO TABLE gr1_spiegazioni FIELDS TERMINATED BY ',';

