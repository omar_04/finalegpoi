USE Stig_2023;

INSERT INTO utenti(username, password, nome, cognome, email) VALUES
("bek", "3c8e94f6eaf41765bbe8c6aa717b07307acf209a93163df199fc7a4d6c9719cd", "Bekim", "Sulejmani","bek@mail.it"),
("dalbe", "b3f1dd41f76e69a7cdb575be2b9c0208769c98d54815b94305717df47e77e2d6", "Tommaso", "Dalbesio","dalbe@mail.it"),
("ragip", "2e93ce5061ca054b92a0c75eaec734fc21f7c5ac8e095bdeabed29bab3aec3b7", "Ragip", "Osja","osja@mail.it"),
("lollo", "d9be766e01ba5a2a497ba5783f916e49f02dafde893debfa037df962c8c33fce", "Lorenzo", "Mairone","mairone@mail.it"),
("laba", "3e410dac6e32fd39cd86f255c0c3ea71aefcf2549edb1f4799d6cb7a8afacd83", "Samuel", "Labagnara","laba@mail.it"),
("zhao", "4ba739741e69e5d4df6d596c94901c58f72c48caaf8711be6fead80e2fa54ddd", "Yujie", "Zhao","zhao@mail.it");

