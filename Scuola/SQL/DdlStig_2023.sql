--Ddl Stig_2023 maturità
--Elimino database se esiste
DROP DATABASE IF EXISTS Stig_2023;
--Creo database se non esiste
CREATE DATABASE IF NOT EXISTS Stig_2023;
--Entro nel database
USE Stig_2023;
--Creo tabella utenti
--dichiaro colonne tabella
CREATE TABLE IF NOT EXISTS utenti(
	username VARCHAR(15) NOT NULL,
	password CHAR(64) NOT NULL,
	nome VARCHAR(25) NOT NULL,
	cognome VARCHAR(30) NOT NULL,
	email VARCHAR(50) NOT NULL,
	punteggio INT(11) DEFAULT 0,
	nomegioco VARCHAR(25) NULL,
	PRIMARY KEY(username)
);
--GR01
--Creo tabella Domande
CREATE TABLE IF NOT EXISTS gr1_domande(
	idDomanda INT AUTO_INCREMENT NOT NULL,
	categoria ENUM('forza','resistenza','velocità') NOT NULL,
	testo VARCHAR(200) NOT NULL,
	difficoltaFk ENUM('facile', 'media', 'difficile'),
	punteggio FLOAT NOT NULL,
	PRIMARY KEY (idDomanda)
);
--Creo tabella Risposte
CREATE TABLE IF NOT EXISTS gr1_risposte(
	idRisposta INT AUTO_INCREMENT NOT NULL,
	testo VARCHAR(100) NOT NULL,
	idDomandaFk INT,
	corretta ENUM('true','false') NOT NULL,
	PRIMARY KEY (idRisposta),
	FOREIGN KEY (idDomandaFk) REFERENCES gr1_domande(idDomanda) ON DELETE CASCADE
);
--Creo tabella Difficolta
CREATE TABLE IF NOT EXISTS gr1_difficolta(
	difficolta ENUM('facile','media','difficile') NOT NULL,
	moltiplicatore FLOAT NOT NULL,
	PRIMARY KEY (difficolta)
);
--Aggiungo fk di difficolta nella tabella Domande
ALTER TABLE gr1_domande ADD CONSTRAINT fk_difficolta FOREIGN KEY (difficoltaFk) REFERENCES gr1_difficolta(difficolta) ON DELETE CASCADE;
--Creo tabella Spiegazioni
CREATE TABLE IF NOT EXISTS gr1_spiegazioni(
	idSpiegazione INT AUTO_INCREMENT NOT NULL,
	testo VARCHAR(500) NOT NULL,
	PRIMARY KEY (idSpiegazione)
);
--Creo tabella giocatori
CREATE TABLE IF NOT EXISTS gr1_giocatori(
	username VARCHAR(15) NOT NULL,
	puntiForza INT NOT NULL DEFAULT 0,
	puntiResistenza INT NOT NULL DEFAULT 0,
	puntiVelocita INT NOT NULL DEFAULT 0,
	PRIMARY KEY (username)
);

CREATE VIEW gr1_media_tab(username,media_punteggio) AS  SELECT username,(puntiForza + puntiResistenza + puntiVelocita)/3 AS media_punteggio FROM gr1_giocatori;

--GR02
--creo tabella canzoni
CREATE TABLE IF NOT EXISTS gr2_canzoni(
	nome_canzone VARCHAR(25) NOT NULL PRIMARY KEY,
	mp3 VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS gr2_gioca ( 
	cod_partita INT auto_increment NOT NULL PRIMARY KEY,
	punteggio_partita INT(11) DEFAULT 0,
	username_fk VARCHAR(15),
	nome_canzone_fk VARCHAR(25),
	FOREIGN KEY(username_fk) REFERENCES utenti(username) ON DELETE CASCADE,
	FOREIGN KEY(nome_canzone_fk) REFERENCES gr2_canzoni(nome_canzone) ON DELETE CASCADE
);

--GR03
--Creo tabella g3_utenti
CREATE TABLE IF NOT EXISTS gr3_utenti(
	fk_username VARCHAR(15),
	ultimoPunteggio INT(11) DEFAULT 0,
	migliorPunteggio INT(11) DEFAULT 0,
	PRIMARY KEY (fk_username),
	FOREIGN KEY (fk_username) REFERENCES utenti(username) ON DELETE CASCADE
);

--GR04
--Creo tabella partita
CREATE TABLE IF NOT EXISTS gr4_partita (
	id_partita INT AUTO_INCREMENT PRIMARY KEY,
	modalita VARCHAR(32),
	punteggio INT,
	username_giocatore VARCHAR(50),
	FOREIGN KEY (username_giocatore) REFERENCES utenti(username) ON DELETE CASCADE
);


/*DML*/

/*GR1*/
DELETE FROM gr1_domande;
DELETE FROM gr1_risposte;
DELETE FROM gr1_difficolta;
DELETE FROM gr1_spiegazioni;
LOAD DATA INFILE '/opt/lampp/htdocs/Scuola/SQL/DML/gr1_difficolta.csv' INTO TABLE gr1_difficolta FIELDS TERMINATED BY ',';
LOAD DATA INFILE '/opt/lampp/htdocs/Scuola/SQL/DML/gr1_domande.csv' INTO TABLE gr1_domande FIELDS TERMINATED BY ',';
LOAD DATA INFILE '/opt/lampp/htdocs/Scuola/SQL/DML/gr1_risposte.csv' INTO TABLE gr1_risposte FIELDS TERMINATED BY ',';
LOAD DATA INFILE '/opt/lampp/htdocs/Scuola/SQL/DML/gr1_spiegazioni.csv' INTO TABLE gr1_spiegazioni FIELDS TERMINATED BY ',';

/*GR2*/
INSERT INTO gr2_canzoni(mp3, nome_canzone)
VALUES 	("BellaCiao", "Bella Ciao"),
		("InnoAllaGioia", "Inno Alla Gioia"),
		("innoIT", "Inno d italia"),
		("Imagine", "Imagine"),
		("MiiTheme", "Mii Theme"),
		("One_Metallica", "One - Metallica");

/*GR3*/
INSERT INTO utenti(username, password, nome, cognome, email) VALUES
("bek", "3c8e94f6eaf41765bbe8c6aa717b07307acf209a93163df199fc7a4d6c9719cd", "Bekim", "Sulejmani","bek@mail.it"),
("dalbe", "b3f1dd41f76e69a7cdb575be2b9c0208769c98d54815b94305717df47e77e2d6", "Tommaso", "Dalbesio","dalbe@mail.it"),
("ragip", "2e93ce5061ca054b92a0c75eaec734fc21f7c5ac8e095bdeabed29bab3aec3b7", "Ragip", "Osja","osja@mail.it"),
("lollo", "d9be766e01ba5a2a497ba5783f916e49f02dafde893debfa037df962c8c33fce", "Lorenzo", "Mairone","mairone@mail.it"),
("laba", "3e410dac6e32fd39cd86f255c0c3ea71aefcf2549edb1f4799d6cb7a8afacd83", "Samuel", "Labagnara","laba@mail.it"),
("zhao", "4ba739741e69e5d4df6d596c94901c58f72c48caaf8711be6fead80e2fa54ddd", "Yujie", "Zhao","zhao@mail.it");