#REQUISITI
Per avviare la piattaforma sono necessari:
- nodejs (>=17), npm
- XAMPP/Web server

#AVVIO
Avviare web server e mysql server in XAMPP
Se assenti, dare i permessi di esecuzione al file di start con 'chmod +x start.sh'
In /opt/lampp/htdocs/Scuola avviare il file start.sh
Andare in "http://localhost/Scuola/Play2Learn/" da un qualsiasi browser.

#INFORMAZIONI AGGIUNTIVE
In Scuola/SORGENTI sono presenti i codici sorgenti dei giochi, e del sito web, in caso di modifiche al sito web, è possibile eseguire il file "Scuola/update_site.sh" (dando i permessi di esecuzione) per ricreare la build aggiornata.

