Epic Trombone

----------------------------------------------------------------------

Codice SQL per unire i database ( la tabella utenti deve già essere stata creata ):

CREATE TABLE gr2_canzoni(
	nome_canzone VARCHAR(25) NOT NULL PRIMARY KEY,
	mp3 VARCHAR(50) NOT NULL
);

CREATE TABLE gr2_gioca ( 
	cod_partita INT auto_increment NOT NULL PRIMARY KEY,
	punteggio_partita INT(11) DEFAULT 0,
	username_fk VARCHAR(15),
	nome_canzone_fk VARCHAR(25),
	FOREIGN KEY(username_fk) REFERENCES gr2_utenti(username),
	FOREIGN KEY(nome_canzone_fk) REFERENCES gr2_canzoni(nome_canzone)
);

----------------------------------------------------------------------

Per far partire il videogioco:

	node server.js

( In ascolto su porta 1729 al momento )

----------------------------------------------------------------------

Nel file: "Epic_Trombone_V6/Epic_Trombone_V4_2/db/DB_connection.js"
Cambiare la variabile per il nome del db che usate voi.

----------------------------------------------------------------------

Collegamento mappa-epictrombone:

Nel pulsante che porta al gioco mettete come URL : "localhost:1729/epicTrombone/?user={username}"
Es.
"localhost:1729/epicTrombone/?user=Fornero"

----------------------------------------------------------------------

Epic