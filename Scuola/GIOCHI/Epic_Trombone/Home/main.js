const express = require('express')
const router = express.Router()
const path = require('path')

const path_home = path.join(__dirname, "public")
console.log(path_home)
router.use(express.static(path_home))

router.get('/', (req, res) => {
    console.log(req.mess + "home")
    res.setHeader("content-type", "text/html")
    res.setHeader("cache-control", "no-cache")
    res.sendFile('home.html', { root: path_home })
})

module.exports = router