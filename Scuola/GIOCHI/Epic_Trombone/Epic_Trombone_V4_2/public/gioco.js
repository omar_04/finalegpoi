const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const user = urlParams.get('user')
console.log(user)

import menu from "./menu.js"
import select from "./select.js"
import play from "./play.js"
import piume from "./Piume.js"
import options from "./options.js"
import classifica from "./classifica.js";
import podio from "./podio.js";

let config = {
    type: Phaser.AUTO,
    backgroundColor: 0x000000,
    width: 1200,
    height: 700,
    parent: "game",
    pixelArt: true,
    physics: {
        default: 'arcade',
        arcade: { debug: false },
    },
    scene: [menu, select, play, options, piume, classifica, podio],
    audio: {
        volume: 100
    },
    user: true,
    lobbysound: true,
    lobbysoundflg: true
};

let game = new Phaser.Game(config);
game.config.user = {
    username: user
}
game.config.lobbysoundflg = 0