import Buttons from "./Button.js"
import Notes from "./Note.js"
import Punteggio from "./ClassPunteggio.js"

const valNote = {
    "do": 402,
    "re": 468,
    "mi": 534,
    "fa": 600,
    "sol": 666,
    "la": 732,
    "si": 798,
}

const config = {
    "songDelay": 1,
    "speed": 200,
}

let Song

class play extends Phaser.Scene {
    constructor() {
        super("play")
    }

    init(data) {
        this.nameSong = data.id
        console.log(this.nameSong)
    }

    preload() {
        this.cache.audio.remove("song")

        this.load.image('cursor', 'assets/img/particelle/cursor.png')
        this.load.image("guideLine", "./assets/img/bg/line.png")
        this.load.atlas("tasto", "assets/img/tasti/tasti.png", "assets/img/tasti/tasti.json")
        this.load.atlas("err", "assets/img/err/err.png", "assets/img/err/err.json")
        this.load.video('background', 'assets/img/bg/bg.mp4', 'loadeddata', false, true)
        this.load.atlas("heart", "assets/img/heart/heart.png", "assets/img/heart/heart.json")

        this.load.image("emitterNote1", "./assets/img/emitterNote/music-note.png")
        this.load.image("emitterNote2", "./assets/img/emitterNote/music-note2.png")
        this.load.image("emitterNote3", "./assets/img/emitterNote/music-note3.png")
        this.load.image("emitterNote4", "./assets/img/emitterNote/music-note4.png")
        this.load.image("emitterNote5", "./assets/img/emitterNote/music-note5.png")

        this.load.image("err", "./assets/img/particelle/err.png")

        Notes.preload(this)
        Buttons.preload(this)
        // this.load.audio("song", ["./assets/music/"+this.nameSong+".mp3"])                // mp3 in futuro caricato dinamicamente
        this.load.audio("song", ["./assets/music/" + this.nameSong[1] + ".mp3"])
        this.load.audio("lose", ["./assets/music/gameover_trombone.mp3"])
        this.load.audio("win", ["./assets/music/win.mp3"])
        //fetch("./assets/json/"+this.nameSong+".json")                                     // json in futuro caricato dinamicamente
        fetch("./assets/json/" + this.nameSong[1] + ".json")
            .then(response => response.json())
            .then(data => Song = data);
        this.load.scenePlugin({
            key: 'rexuiplugin',
            url: 'https://unpkg.com/phaser3-rex-plugins@1.1.85/dist/rexuiplugin.min.js',
            sceneKey: 'rexUI'
        });
    }

    create() {
        this.game.config.lobbysound.stop()
        this.game.config.lobbysoundflg = 0
        this.flgFetch = 0

        this.vid = this.add.video(600, 350, 'background').setScale(0.7)
        this.vid.play(true)
        this.vid.setPaused(false)

        this.add.image(402, 275, "guideLine").setScale(0.35)
        this.add.image(468, 275, "guideLine").setScale(0.35)
        this.add.image(534, 275, "guideLine").setScale(0.35)
        this.add.image(600, 275, "guideLine").setScale(0.35)
        this.add.image(666, 275, "guideLine").setScale(0.35)
        this.add.image(732, 275, "guideLine").setScale(0.35)
        this.add.image(798, 275, "guideLine").setScale(0.35)

        this.spriteTasti = {}

        this.anims.create({
            key: 'btnIdle',
            frames: this.anims.generateFrameNames('tasto', {
                start: 1,
                end: 1,
                zeroPad: 1,
                prefix: 'frame',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0,
        })

        this.anims.create({
            key: 'btnPlay',
            frames: this.anims.generateFrameNames('tasto', {
                start: 2,
                end: 2,
                zeroPad: 1,
                prefix: 'frame',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0,
        })

        // testo prima di iniziare
        this.textSpaceStart = this.add.text(220, 220, "Premi INVIO per iniziare!", { fontSize: 50, color: "#ffffff", fontFamily: "myFont" }).setVisible(true)
        // animazione scritta
        this.tweens.add({
            targets: this.textSpaceStart,
            scaleX: 1,
            scaleY: 1.7,
            ease: 'Cubic.easeInOut',
            duration: 2500,
            yoyo: true,
            repeat: -1
        })

        this.spriteTasti.do = this.physics.add.sprite(valNote["do"], 650, "tasto").setScale(1)
        this.spriteTasti.re = this.physics.add.sprite(valNote["re"], 650, "tasto").setScale(1)
        this.spriteTasti.mi = this.physics.add.sprite(valNote["mi"], 650, "tasto").setScale(1)
        this.spriteTasti.fa = this.physics.add.sprite(valNote["fa"], 650, "tasto").setScale(1)
        this.spriteTasti.sol = this.physics.add.sprite(valNote["sol"], 650, "tasto").setScale(1)
        this.spriteTasti.la = this.physics.add.sprite(valNote["la"], 650, "tasto").setScale(1)
        this.spriteTasti.si = this.physics.add.sprite(valNote["si"], 650, "tasto").setScale(1)

        this.tasto_lettera = {}
        this.tasto_lettera.A = this.add.text(valNote["do"] - 15, 630, "A", { fontFamily: 'myFont', fontSize: 40, color: "#0c5daf" })
        this.tasto_lettera.S = this.add.text(valNote["re"] - 15, 630, "S", { fontFamily: 'myFont', fontSize: 40, color: "#0c5daf" })
        this.tasto_lettera.D = this.add.text(valNote["mi"] - 15, 630, "D", { fontFamily: 'myFont', fontSize: 40, color: "#0c5daf" })
        this.tasto_lettera.F = this.add.text(valNote["fa"] - 15, 630, "F", { fontFamily: 'myFont', fontSize: 40, color: "#0c5daf" })
        this.tasto_lettera.G = this.add.text(valNote["sol"] - 15, 630, "G", { fontFamily: 'myFont', fontSize: 40, color: "#0c5daf" })
        this.tasto_lettera.H = this.add.text(valNote["la"] - 15, 630, "H", { fontFamily: 'myFont', fontSize: 40, color: "#0c5daf" })
        this.tasto_lettera.J = this.add.text(valNote["si"] - 15, 630, "J", { fontFamily: 'myFont', fontSize: 40, color: "#0c5daf" })

        this.spriteErr = {}

        this.anims.create({
            key: 'errIdle',
            frames: this.anims.generateFrameNames('err', {
                start: 2,
                end: 2,
                zeroPad: 1,
                prefix: 'output-onlinepngtools',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0,
        })

        this.anims.create({
            key: 'errActive',
            frames: this.anims.generateFrameNames('err', {
                start: 1,
                end: 1,
                zeroPad: 1,
                prefix: 'output-onlinepngtools',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 0,
        })

        this.spriteErr["do"] = this.physics.add.sprite(valNote["do"], 550, "err").setScale(0.3).anims.play("errIdle")
        this.spriteErr["re"] = this.physics.add.sprite(valNote["re"], 550, "err").setScale(0.3).anims.play("errIdle")
        this.spriteErr["mi"] = this.physics.add.sprite(valNote["mi"], 550, "err").setScale(0.3).anims.play("errIdle")
        this.spriteErr["fa"] = this.physics.add.sprite(valNote["fa"], 550, "err").setScale(0.3).anims.play("errIdle")
        this.spriteErr["sol"] = this.physics.add.sprite(valNote["sol"], 550, "err").setScale(0.3).anims.play("errIdle")
        this.spriteErr["la"] = this.physics.add.sprite(valNote["la"], 550, "err").setScale(0.3).anims.play("errIdle")
        this.spriteErr["si"] = this.physics.add.sprite(valNote["si"], 550, "err").setScale(0.3).anims.play("errIdle")

        this.btns = new Buttons(this)
        Object.entries(valNote).forEach(entry => {
            const [TipoButton, x] = entry;
            this.btns.spawnButton(x, 675, TipoButton)
        });

        // creazione classe delle note
        this.notes = new Notes(this)
        this.flg = 0
        this.musicState = 0

        this.cursors = this.input.keyboard.addKeys({
            do: Phaser.Input.Keyboard.KeyCodes.A,
            re: Phaser.Input.Keyboard.KeyCodes.S,
            mi: Phaser.Input.Keyboard.KeyCodes.D,
            fa: Phaser.Input.Keyboard.KeyCodes.F,
            sol: Phaser.Input.Keyboard.KeyCodes.G,
            la: Phaser.Input.Keyboard.KeyCodes.H,
            si: Phaser.Input.Keyboard.KeyCodes.J,
            pause: Phaser.Input.Keyboard.KeyCodes.ESC,
            start: Phaser.Input.Keyboard.KeyCodes.ENTER
        })
        // if (game.input.activePointer.withinGame){
        //     game.input.enabled = true;
        // } else {
        //     game.input.enabled = false;
        // } 
        this.physics.add.overlap(this.btns, this.notes, (btn, n) => {
            //btn=>il tasto coperto dalla nota
            //n => la nota che sta coprendo il tasto
            let press = false //se è stato premuto
            switch (btn.type) { //controllo tipo di tasto
                case "do":
                    press = Phaser.Input.Keyboard.JustDown(this.cursors.do) //boolean
                    break;
                case "re":
                    press = Phaser.Input.Keyboard.JustDown(this.cursors.re)
                    break;
                case "mi":
                    press = Phaser.Input.Keyboard.JustDown(this.cursors.mi)
                    break;
                case "fa":
                    press = Phaser.Input.Keyboard.JustDown(this.cursors.fa)
                    break;
                case "sol":
                    press = Phaser.Input.Keyboard.JustDown(this.cursors.sol)
                    break;
                case "la":
                    press = Phaser.Input.Keyboard.JustDown(this.cursors.la)
                    break;
                case "si":
                    press = Phaser.Input.Keyboard.JustDown(this.cursors.si)
                    break;
                default:
                    break;
            }
            if (n.type == btn.type && press) {
                Punteggio.Giusto()
                this.p.aggiorna()

                this.emitterNote1.setPosition(0, 0);
                this.emitterNote1.active = true;
                this.emitterNote1.setEmitZone({
                    source: new Phaser.Geom.Rectangle(0, 0, 350, 700),
                    type: "random",
                    quantity: 1000
                })
                this.emitterNote1.explode()

                this.emitterNote2.setPosition(0, 0);
                this.emitterNote2.active = true;
                this.emitterNote2.setEmitZone({
                    source: new Phaser.Geom.Rectangle(0, 0, 350, 700),
                    type: "random",
                    quantity: 1000
                })
                this.emitterNote2.explode()

                this.emitterNote3.setPosition(0, 0);
                this.emitterNote3.active = true;
                this.emitterNote3.setEmitZone({
                    source: new Phaser.Geom.Rectangle(0, 0, 350, 700),
                    type: "random",
                    quantity: 1000
                })
                this.emitterNote3.explode()

                this.emitterNote4.setPosition(0, 0);
                this.emitterNote4.active = true;
                this.emitterNote4.setEmitZone({
                    source: new Phaser.Geom.Rectangle(0, 0, 350, 700),
                    type: "random",
                    quantity: 1000
                })
                this.emitterNote4.explode()

                this.emitterNote5.setPosition(0, 0);
                this.emitterNote5.active = true;
                this.emitterNote5.setEmitZone({
                    source: new Phaser.Geom.Rectangle(0, 0, 350, 700),
                    type: "random",
                    quantity: 1000
                })
                this.emitterNote5.explode()

                this.emitterNote1.setPosition(0, 0);
                this.emitterNote1.active = true;
                this.emitterNote1.setEmitZone({
                    source: new Phaser.Geom.Rectangle(850, 0, 350, 700),
                    type: "random",
                    quantity: 1000
                })
                this.emitterNote1.explode()

                this.emitterNote2.setPosition(0, 0);
                this.emitterNote2.active = true;
                this.emitterNote2.setEmitZone({
                    source: new Phaser.Geom.Rectangle(850, 0, 350, 700),
                    type: "random",
                    quantity: 1000
                })
                this.emitterNote2.explode()

                this.emitterNote3.setPosition(0, 0);
                this.emitterNote3.active = true;
                this.emitterNote3.setEmitZone({
                    source: new Phaser.Geom.Rectangle(850, 0, 350, 700),
                    type: "random",
                    quantity: 1000
                })
                this.emitterNote3.explode()

                this.emitterNote4.setPosition(0, 0);
                this.emitterNote4.active = true;
                this.emitterNote4.setEmitZone({
                    source: new Phaser.Geom.Rectangle(850, 0, 350, 700),
                    type: "random",
                    quantity: 1000
                })
                this.emitterNote4.explode()

                this.emitterNote5.setPosition(0, 0);
                this.emitterNote5.active = true;
                this.emitterNote5.setEmitZone({
                    source: new Phaser.Geom.Rectangle(850, 0, 350, 700),
                    type: "random",
                    quantity: 1000
                })
                this.emitterNote5.explode()

                n.destroy()
            }
        })

        this.sound.pauseOnBlur = false
        this.music = this.sound.add("song", { volume: (this.game.config.audio.volume / 100) })

        this.lose = this.sound.add("lose", { volume: (this.game.config.audio.volume / 100) })
        this.win = this.sound.add("win", { volume: (this.game.config.audio.volume / 100) })

        //velo trasparente scuro
        this.velo = this.add.graphics({ x: 0, y: 0 });
        this.velo.fillStyle('0x000000', 0.6);
        this.velo.fillRect(0, 0, 1200, 700);
        this.velo.setVisible(false)

        //testo pausa
        this.txt_pause = this.add.text(515, 250, 'Pause', { fontSize: 50, color: "white", fontFamily: 'myFont' })
        this.txt_pause.setVisible(false)

        this.emitterNote1 = this.add.particles("emitterNote1").createEmitter({
            scale: {
                start: 1,
                end: 0
            },
            speed: {
                min: 0,
                max: 200
            },
            active: false,
            lifespan: 600,
            quantity: 1
        })
        this.emitterNote2 = this.add.particles("emitterNote2").createEmitter({
            scale: {
                start: 1,
                end: 0
            },
            speed: {
                min: 0,
                max: 200
            },
            active: false,
            lifespan: 600,
            quantity: 1
        })
        this.emitterNote3 = this.add.particles("emitterNote3").createEmitter({
            scale: {
                start: 1,
                end: 0
            },
            speed: {
                min: 0,
                max: 200
            },
            active: false,
            lifespan: 600,
            quantity: 1
        })
        this.emitterNote4 = this.add.particles("emitterNote4").createEmitter({
            scale: {
                start: 1,
                end: 0
            },
            speed: {
                min: 0,
                max: 200
            },
            active: false,
            lifespan: 600,
            quantity: 1
        })
        this.emitterNote5 = this.add.particles("emitterNote5").createEmitter({
            scale: {
                start: 1,
                end: 0
            },
            speed: {
                min: 0,
                max: 200
            },
            active: false,
            lifespan: 600,
            quantity: 1
        })

        this.add.text(100, 30, this.nameSong[0], { fontSize: 32, fontFamily: "myFont" })
        this.p = new Punteggio(0, this)
        Punteggio.TotSbagli()
        Punteggio.creonumeroSbagliConsecutivi()


        //testo vittoria
        this.txt_end = this.add.text(470, 250, 'Vittoria', { fontSize: 50, color: "white", fontFamily: 'myFont' })
        this.txt_end.setVisible(false)
        this.txt_riep = this.add.text(470, 350, '', { fontSize: 50, color: "white", fontFamily: 'myFont' })
        this.txt_riep.setVisible(false)
        this.gameover = this.add.text(470, 250, 'Gameover', { fontSize: 50, color: "white", fontFamily: 'myFont' })
        this.gameover.setVisible(false)

        this.scene.launch('piume')

        this.theLastOfUs

        this.resume = this.add.text(430, 500, 'Resume', { fontSize: 50, color: "#ff4c7c", fontFamily: 'myFont' })
            .setOrigin(0.5)
            .setPadding(20)
            .setStyle({ backgroundColor: '#0c5daf' })
            .setInteractive()
            .on('pointerdown', () => this.musicResume())
            .on('pointerover', () => this.resume.setScale(1.2))
            .on('pointerout', () => this.resume.setScale(1))
            .setVisible(false)

        this.exit = this.add.text(770, 500, 'Exit', { fontSize: 50, color: "#ff4c7c", fontFamily: 'myFont' })
            .setOrigin(0.5)
            .setPadding(20)
            .setStyle({ backgroundColor: '#0c5daf' })
            .setInteractive()
            .on('pointerdown', () => {
                this.lose.stop()
                this.win.stop()
                this.scene.start("select")
            })
            .on('pointerover', () => this.exit.setScale(1.2))
            .on('pointerout', () => this.exit.setScale(1))
            .setVisible(false)

        this.retry = this.add.text(430, 500, 'Retry', { fontSize: 50, color: "#ff4c7c", fontFamily: 'myFont' })
            .setOrigin(0.5)
            .setPadding(20)
            .setStyle({ backgroundColor: '#0c5daf' })
            .setInteractive()
            .on('pointerdown', () => {
                this.lose.stop()
                this.win.stop()
                this.scene.start("play")
            })
            .on('pointerover', () => this.retry.setScale(1.2))
            .on('pointerout', () => this.retry.setScale(1))
            .setVisible(false)

        this.exit2 = this.add.text(770, 500, 'Exit', { fontSize: 50, color: "#ff4c7c", fontFamily: 'myFont' })
            .setOrigin(0.5)
            .setPadding(20)
            .setStyle({ backgroundColor: '#0c5daf' })
            .setInteractive()
            .on('pointerdown', () => {
                this.lose.stop()
                this.win.stop()
                this.scene.start("select")
            })
            .on('pointerover', () => this.exit2.setScale(1.2))
            .on('pointerout', () => this.exit2.setScale(1))
            .setVisible(false)

        this.heart = this.physics.add.sprite(1030, 110, "heart").setFrame("heart5.png").setScale(0.3)

        this.input.keyboard.enabled = true;
        this.input.keyboard.disableGlobalCapture();

        // particelle mouse
        let canvas = this.sys.canvas;
        canvas.style.cursor = 'none';
        let scene = this
        var emitter = this.add.particles('cursor').createEmitter({
            x: 0,
            y: 0,
            blendMode: 'SCREEN',
            scale: { start: 0.3, end: 0.2 },
            quantity: 2,
            rotation: 0,
            lifespan: 200,
        });
        var emitZones = [];
        var emitZoneIndex = 0;

        this.input.on('pointermove', function (pointer) {
            emitter.setPosition(pointer.x, pointer.y)
            scene.scale.updateBounds();
        });
        emitter.setEmitZone(emitZones[emitZoneIndex]);
        //------------------------------------------------------------------------------
        this.noteNumber = 0
    }

    update(time, delta) {
        if (this.flg == 0) {
            Object.keys(Song["note"]).forEach(nt => {
                Song["note"][nt].forEach(t => {
                    this.notes.spawnNote(valNote[nt], -200 * (Song["position"][t] + config["songDelay"]), nt, this.spriteErr[nt])
                    this.noteNumber += 1
                })
            })
            this.theLastOfUs = this.physics.add.sprite(600, -200 * (Song["durata"] + 1), "blank")
            this.theLastOfUs.ultimo = true
            this.theLastOfUs.setVisible(false)
            this.flg = 666
        } else if (this.flg == 666 && Phaser.Input.Keyboard.JustDown(this.cursors.start)) {
            this.textSpaceStart.setVisible(false)
            console.log("Start...")
            this.flg = 1
        } else if (this.flg == 1) {
            this.time.addEvent({ delay: (3200 + (1000 * config["songDelay"])), callback: this.musicStart, callbackScope: this, loop: false })
            this.notes.setVelocityY(config["speed"])
            this.theLastOfUs.setVelocityY(config["speed"])
            this.flg = 2
        }

        if (this.flg == 2 && Phaser.Input.Keyboard.JustDown(this.cursors.pause) && this.musicState == 1) {
            this.musicPause()
        } else if (this.flg == 3 && Phaser.Input.Keyboard.JustDown(this.cursors.pause)) {
            this.musicResume()
        }

        if (this.flg == 2) {

            if (Punteggio.getnumeroSbagliConsecutivi() >= 5 && this.musicState != 10) {
                this.musicStop()
                this.p.getOutput().setVisible(false)
                this.velo.setVisible(true)
                this.gameover.setVisible(true)
                this.retry.setVisible(true)
                this.exit2.setVisible(true)
                this.lose.play()
                this.musicState = 10
            }

            if (this.theLastOfUs.y > 800) {
                this.p.getOutput().setVisible(false)
                this.velo.setVisible(true)
                this.txt_end.setVisible(true)
                this.txt_riep.setText('Score: ' + this.p.getPunteggio())
                this.txt_riep.setVisible(true)
                this.retry.setVisible(true)
                this.exit2.setVisible(true)
                this.musicStop()
                if (this.flgFetch == 0) {
                    this.win.play()
                    this.flgFetch = 1
                    console.log(this.game.config.user.username)
                    fetch("/epicTrombone/plays/controllo_esistenza?username=" + this.game.config.user.username + "&song=" + this.nameSong[0])
                        .then(res => res.json())
                        .then(data => {
                            console.log(data)
                            if (data.length > 0) {
                                console.log("Esiste già")
                                console.log(data[0]["punteggio_partita"])
                                console.log(this.p.getPunteggio())
                                if (this.p.getPunteggio() > data[0]["punteggio_partita"]) {
                                    console.log("Nuovo punteggio")
                                    fetch("/epicTrombone/plays/update?username=" + this.game.config.user.username + "&punteggio=" + this.p.getPunteggio() + "&song=" + this.nameSong[0])
                                } else {
                                    console.log("Tengo il vecchio punteggio")
                                }
                            } else {
                                console.log("Non esiste")
                                fetch("/epicTrombone/plays/add?username=" + this.game.config.user.username + "&punteggio=" + this.p.getPunteggio() + "&song=" + this.nameSong[0])
                            }
                        })
                    fetch("/epicTrombone/utenti/maxPunteggio?username=" + this.game.config.user.username + "&punteggio=" + (100 / this.noteNumber) * (this.p.getPunteggio() / 10))
                }
            }

            this.buttonAnim(time)

            switch (Punteggio.getnumeroSbagliConsecutivi()) {
                case 5:
                    this.heart.setFrame("heart0.png");
                    break;
                case 4:
                    this.heart.setFrame("heart1.png");
                    break;
                case 3:
                    this.heart.setFrame("heart2.png");
                    break;
                case 2:
                    this.heart.setFrame("heart3.png");
                    break;
                case 1:
                    this.heart.setFrame("heart4.png");
                    break;

                default:
                    this.heart.setFrame("heart5.png");
                    break;
            }
        }
    }

    buttonAnim(time) {
        if (Phaser.Input.Keyboard.DownDuration(this.cursors.do, time + 2000)) {
            this.spriteTasti.do.anims.play('btnPlay', false)
        } else {
            this.spriteTasti.do.anims.play('btnIdle', false)
        }
        if (Phaser.Input.Keyboard.DownDuration(this.cursors.re, time + 2000)) {
            this.spriteTasti.re.anims.play('btnPlay', false)
        } else {
            this.spriteTasti.re.anims.play('btnIdle', false)
        }
        if (Phaser.Input.Keyboard.DownDuration(this.cursors.mi, time + 2000)) {
            this.spriteTasti.mi.anims.play('btnPlay', false)
        } else {
            this.spriteTasti.mi.anims.play('btnIdle', false)
        }
        if (Phaser.Input.Keyboard.DownDuration(this.cursors.fa, time + 2000)) {
            this.spriteTasti.fa.anims.play('btnPlay', false)
        } else {
            this.spriteTasti.fa.anims.play('btnIdle', false)
        }
        if (Phaser.Input.Keyboard.DownDuration(this.cursors.sol, time + 2000)) {
            this.spriteTasti.sol.anims.play('btnPlay', false)
        } else {
            this.spriteTasti.sol.anims.play('btnIdle', false)
        }
        if (Phaser.Input.Keyboard.DownDuration(this.cursors.la, time + 2000)) {
            this.spriteTasti.la.anims.play('btnPlay', false)
        } else {
            this.spriteTasti.la.anims.play('btnIdle', false)
        }
        if (Phaser.Input.Keyboard.DownDuration(this.cursors.si, time + 2000)) {
            this.spriteTasti.si.anims.play('btnPlay', false)
        } else {
            this.spriteTasti.si.anims.play('btnIdle', false)
        }
    }

    musicStart() {
        this.music.play()
        this.musicState = 1
    }

    musicStop() {
        this.music.stop()
        this.musicState = 0
        this.notes.setVelocityY(0)
        this.theLastOfUs.setVelocityY(0)
    }

    musicPause() {
        this.music.pause()
        this.musicState = 2
        this.velo.setVisible(true)
        this.txt_pause.setVisible(true)
        this.resume.setVisible(true)
        this.exit.setVisible(true)
        this.notes.setVelocityY(0)
        this.theLastOfUs.setVelocityY(0)
        this.flg = 3
    }

    musicResume() {
        this.musicState = 1
        this.music.resume()
        this.velo.setVisible(false)
        this.txt_pause.setVisible(false)
        this.resume.setVisible(false)
        this.exit.setVisible(false)
        this.notes.setVelocityY(config["speed"])
        this.theLastOfUs.setVelocityY(config["speed"])
        this.flg = 2
    }
}

export default play