//Contenitore Tasti
const Tasto_impostazione = (scene, e, p) => {
    var buttons = scene.rexUI.add.buttons({
        x: 0,
        y: 0,
        name: [e, p], //nome e path canzone
        buttons: [createButton(scene, e)]
    })
    return buttons
}

//Tasto
var createButton = function (scene, text) {
    return scene.rexUI.add.label({
        width: 800,
        height: 90,
        //Prova 0xFe000 per vedere la differenza
        background: scene.rexUI.add.roundRectangle(0, 0, 0, 0, 20, 0x0c5daf),
        text: scene.add.text(0, 0, text, {
            fontSize: 45,
            fontFamily: 'myFont',
            color: "#ff4c7c"
        }),
        align: 'center',
        space: {
            left: 10,
            right: 10,
        }
    });
}

export default Tasto_impostazione