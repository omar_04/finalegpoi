import Tasto_impostazione from './Funzione_Tasto.js';

const loadSongs = async () => {
    const flagCanz = new Promise((resolve, reject) => {
        fetch("/epicTrombone/canzoni/get")
            .then(response => response.json())
            .then((data) => {
                let temp = []
                data.forEach(el => {
                    temp.push({
                        "nome_canzone": el["nome_canzone"],
                        "path": el["mp3"]
                    })
                })
                resolve(temp)
            })
    })
    return await flagCanz
}

class select extends Phaser.Scene {
    constructor() {
        super("select")
    }

    preload() {
        this.load.image('cursor', 'assets/img/particelle/cursor.png');
        this.load.image('tn', 'assets/area_tasti.png');
        this.load.video('background', 'assets/img/bg/bg.mp4', 'loadeddata', false, true)
        this.load.atlas("catS", "assets/img/catGif/catS.png", "assets/img/catGif/catS.json")
        this.load.audio('lobbymusic', 'assets/music/lobbymusic.mp3')

        this.load.scenePlugin({
            key: 'rexuiplugin',
            url: 'https://unpkg.com/phaser3-rex-plugins@1.1.85/dist/rexuiplugin.min.js',
            sceneKey: 'rexUI'
        });

        this.nomeCanzoni = []
        this.glafCanz = 0

        loadSongs()
            .then((data) => {
                this.nomeCanzoni = data
                this.glafCanz = 1
            })
    }

    create() {
        this.sound.pauseOnBlur = false
        if (this.game.config.lobbysoundflg == 0) {
            this.lobbyMusic = this.sound.add("lobbymusic", { volume: (this.game.config.audio.volume / 100) })
            this.lobbyMusic.play()
            this.lobbyMusic.loop = true;
            this.game.config.lobbysound = this.lobbyMusic
            this.game.config.lobbysoundflg = 1
        }

        this.vid = this.add.video(600, 350, 'background').setScale(0.7)
        this.vid.play(true)
        this.vid.setPaused(false)

        this.mioCat = this.physics.add
            .sprite(960, 550, "catS")
            .setSize(40, 65)
            .setOffset(70, 20)

        const anims = this.anims;
        anims.create({
            key: 'idle',
            frames: [
                { key: 'catS', frame: "catS1.png" },
                { key: 'catS', frame: "catS2.png" },
                { key: 'catS', frame: "catS3.png" },
                { key: 'catS', frame: "catS4.png" },
                { key: 'catS', frame: "catS5.png" },
            ],
            frameRate: 8,
            repeat: -1
        });


        this.menu = this.add.text(1060, 60, 'Indietro', { fontSize: 35, color: "#ff4c7c", fontFamily: 'myFont' })
            .setOrigin(0.5)
            .setPadding(20)
            .setStyle({ backgroundColor: '#0c5daf' })
            .setInteractive()
            .on('pointerdown', () => this.scene.start("menu"))
            .on('pointerover', () => this.menu.setScale(1.2))
            .on('pointerout', () => this.menu.setScale(1))


        // particelle mouse
        const scene = this
        let canvas = this.sys.canvas;
        canvas.style.cursor = 'none';
        this.emitter = this.add.particles('cursor').createEmitter({
            x: 0,
            y: 0,
            blendMode: 'SCREEN',
            scale: { start: 0.3, end: 0.2 },
            quantity: 2,
            rotation: 0,
            lifespan: 200,
        });
        this.emitZones = [];
        this.emitZoneIndex = 0;

        this.input.on('pointermove', function (pointer) {
            this.scene.emitter.setPosition(pointer.x, pointer.y)
            scene.scale.updateBounds();
        });
        this.emitter.setEmitZone(this.emitZones[this.emitZoneIndex]);
        //------------------------------------------------------------------------------
    }

    update(time, delta) {
        this.mioCat.play("idle", true);
        if (this.glafCanz == 1) {
            this.glafCanz = 2
            this.genera()
        }
    }

    genera() {
        var scrollablePanel = this.rexUI.add.scrollablePanel({
            x: 300,
            y: 350,
            width: 100,
            height: 750,

            //0 -> verticale
            //1 -> orizzontale
            scrollMode: 0,

            // Spazio colore rosso
            // background: this.rexUI.add.roundRectangle(0, 0, 0, 0, 10, 0xFE0000), //

            //BORDO ROSSO
            //Spazio in cui creo cose
            panel: {
                child: createGrid(this),
            },

            //barra di scorrimento
            // slider: {
            //     //spazio di scorrimento
            //     track: this.rexUI.add.roundRectangle(0, 0, 10, 10, 10, 0xFE00D4),
            //     //pallino
            //     thumb: this.rexUI.add.roundRectangle(0, 0, 0, 0, 20, 0x0FFE00),
            // },

            //rottelino del mouse
            mouseWheelScroller: {
                focus: false,
                speed: 1
            },

            header: this.rexUI.add.label({
                height: 100,

                // orientation: 0,
                // background: this.rexUI.add.roundRectangle(0, 0, 20, 20, 0, COLOR_DARK),
                text: this.add.text(0, 0, 'NOME CANZONI', {
                    fontSize: 45,
                    fontFamily: 'myFont'
                }),
                align: 'center',
            }),

            space: {
                left: 10,
                right: 10,
                top: 15,
                bottom: 15,

                panel: 10,
                header: 10,
                footer: 10,
            }
        })
            .layout()
            .setChildrenInteractive()
            .on('child.click', function (child, pointer, event) {
                this.displayList.scene.scene.start('play', { id: child.name })
            })
            .on('child.over', function (child, pointer, event) {
                // child.setX(child.x + 1);
                while (child.x <= 350) {
                    child.x++
                }
            })
            .on('child.hover', function (child, pointer, event) {
                // child.setX(child.x + 1);
                while (child.x <= 350) {
                    child.x++
                }
            })
            .on('child.out', function (child, pointer, event) {
                while (child.x >= 300) {
                    child.setX(child.x - 0.1);
                }

            })
        // .on('child.pressstart', function(child, pointer, event) {
        //     console.log(child)
        // })
        this.emitter = this.add.particles('cursor').createEmitter({
            x: 0,
            y: 0,
            blendMode: 'SCREEN',
            scale: { start: 0.3, end: 0.2 },
            quantity: 2,
            rotation: 0,
            lifespan: 200,
        });
        this.emitZones = [];
        this.emitZoneIndex = 0;
        this.emitter.setEmitZone(this.emitZones[this.emitZoneIndex]);
    }
}

const createGrid = (scene) => {
    // Create table body
    var sizer = scene.rexUI.add.fixWidthSizer({
        space: {
            //margin
            left: 3, //sinistra
            right: 3, //destra
            top: 3, //sopra
            bottom: 3, //sotto
            item: 1, //spazio tra gli oggetti
            line: 10, //spazio tra le righe dei oggetti
        },
    })
    //creo un background per i tasti --> roundRectangle(x, y, width, height, radius, color)
    //Background tasti blue
    // .addBackground(scene.rexUI.add.roundRectangle(0, 0, 0, 0, 0, 0x0000FE))

    //Array contenente nome delle canzoni
    let nomiCanzioni = scene.nomeCanzoni

    //Ciclo per ogni canzone e gli dedico uno tasto
    nomiCanzioni.forEach(element => {
        sizer.add(Tasto_impostazione(scene, element["nome_canzone"], element["path"])) //
    });
    return sizer;
}

export default select;