class menu extends Phaser.Scene {
    constructor() {
        super("menu")
    }

    preload() {
        this.load.image('cursor', 'assets/img/particelle/cursor.png');
        this.load.image('flares', 'assets/img/particelle/part1.png');
        this.load.image('gatto', 'assets/img/catGif/catM.png');
        this.load.video('background', 'assets/img/bg/bg.mp4', 'loadeddata', false, true)
        this.load.audio('lobbymusic', 'assets/music/lobbymusic.mp3')
        this.load.script('webfont', 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js');
    }

    create() {
        this.sound.pauseOnBlur = false
        if (this.game.config.lobbysoundflg == 0) {
            this.lobbyMusic = this.sound.add("lobbymusic", { volume: (this.game.config.audio.volume / 100) })
            this.lobbyMusic.play()
            this.lobbyMusic.loop = true;
            this.game.config.lobbysound = this.lobbyMusic
            this.game.config.lobbysoundflg = 1
        }

        this.vid = this.add.video(600, 350, 'background').setScale(0.7)
        this.vid.play(true)
        this.vid.setPaused(false)
        
        this.scene.launch('piume')

        var cat1 = this.add.image(1000, 500, 'gatto').setInteractive();
        var cat2 = this.add.image(200, 400, 'gatto').setInteractive();
        cat1.flipX = 180

        this.tweens.add({
            targets: cat1,
            y: 400,
            x: 1000,
            scale: 1.1,
            ease: 'Cubic.easeInOut',
            rotation: 0.5,
            duration: 2000,
            yoyo: true,
            repeat: -1
        });
        this.tweens.add({
            targets: cat2,
            y: 500,
            x: 200,
            rotation: -0.5,
            scale: 1.1,
            ease: 'Cubic.easeInOut',
            duration: 2000,
            yoyo: true,
            repeat: -1
        });

        const scene = this

        WebFont.load({
            google: {
                families: ['myFont']
            },
            active: function () {
                scene.titolo = scene.add.text(600, 100, 'EPIC TROMBONE', { fontFamily: 'myFont', fontSize: 70, color: "white" }).setOrigin(0.5);
                scene.tweens.add({
                    targets: scene.titolo,

                    scaleX: 1.1,
                    scaleY: 1.1,

                    ease: 'Cubic.easeInOut',
                    duration: 2500,
                    yoyo: true,
                    repeat: -1
                });
                scene.gioca = scene.add.text(600, 300, 'Play', { fontSize: 50, color: "#ff4c7c", fontFamily: 'myFont' })
                    .setOrigin(0.5)
                    .setPadding(30)
                    .setDepth(0)
                    .setStyle({ backgroundColor: '#0c5daf' })
                    .setInteractive()
                    .on('pointerdown', () => scene.scene.start("select"))
                    .on('pointerover', () => scene.gioca.setScale(1.2))
                    .on('pointerout', () => scene.gioca.setScale(1))

                scene.opzioni = scene.add.text(600, 450, 'Opzioni', { fontSize: 50, color: "#ff4c7c", fontFamily: 'myFont' })
                    .setOrigin(0.5)
                    .setPadding(30)
                    .setStyle({ backgroundColor: '#0c5daf' })
                    .setInteractive()
                    .on('pointerdown', () => scene.scene.start("options"))
                    .on('pointerover', () => scene.opzioni.setScale(1.2))
                    .on('pointerout', () => scene.opzioni.setScale(1))

                scene.classifica = scene.add.text(600, 600, 'Classifica', { fontSize: 50, color: "#ff4c7c", fontFamily: 'myFont' })
                    .setOrigin(0.5)
                    .setPadding(30)
                    .setStyle({ backgroundColor: '#0c5daf' })
                    .setInteractive()
                    .on('pointerdown', () => scene.scene.start("classifica"))
                    .on('pointerover', () => scene.classifica.setScale(1.2))
                    .on('pointerout', () => scene.classifica.setScale(1))

                // particelle mouse
                let canvas = scene.sys.canvas;
                canvas.style.cursor = 'none';
                var emitter = scene.add.particles('cursor').createEmitter({
                    x: 0,
                    y: 0,
                    blendMode: 'SCREEN',
                    scale: { start: 0.3, end: 0.2 },
                    quantity: 2,
                    rotation: 0,
                    lifespan: 200,
                });
                var emitZones = [];
                var emitZoneIndex = 0;

                scene.input.on('pointermove', function (pointer) {
                    emitter.setPosition(pointer.x, pointer.y)
                    scene.scale.updateBounds();
                });
                emitter.setEmitZone(emitZones[emitZoneIndex]);

                //------------------------------------------------------------------------------
            }
        });
    }

    update(time, delta) {



    }
}

export default menu;