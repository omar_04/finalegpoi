import Tasto_impostazione from './Funzione_Tasto.js';

class options extends Phaser.Scene {
    constructor() {
        super("options")
    }

    preload() {
        this.fl = this.game.config.audio.volume
        this.load.image('cursor', 'assets/img/particelle/cursor.png');

        this.load.scenePlugin({
            key: 'rexuiplugin',
            url: 'https://unpkg.com/phaser3-rex-plugins@1.1.85/dist/rexuiplugin.min.js',
            sceneKey: 'rexUI'
        });

        this.load.video('background', 'assets/img/bg/bg.mp4', 'loadeddata', false, true)
        this.load.audio('note', 'assets/music/note.mp3')
    }

    create() {
        console.log(this.rexUI)
        this.game.config.lobbysound.stop()
        this.game.config.lobbysoundflg = 0
        this.vid = this.add.video(600, 350, 'background').setScale(0.7)
        this.vid.play(true)
        this.vid.setPaused(false)

        const COLOR_PRIMARY = 0x0c5daf;
        const COLOR_LIGHT = 0xff4c7c;

        this.titolo = this.add.text(480, 200, 'Volume', { fontFamily: 'myFont', fontSize: 50, color: "#0c5daf" });

        this.numberBar = this.rexUI.add.numberBar({
            x: 600,
            y: 300,
            width: 800, // Fixed width

            slider: {
                // width: 120, // Fixed width
                track: this.rexUI.add.roundRectangle(0, 0, 0, 0, 10, COLOR_PRIMARY),
                indicator: this.rexUI.add.roundRectangle(0, 0, 0, 0, 10, COLOR_LIGHT),
                input: 'click',
            },

            text: this.add.text(0, 0, '', { fontSize: 50, color: "#ff4c7c", fontFamily: 'myFont' }),

            space: {
                left: 10,
                right: 10,
                top: 10,
                bottom: 10,

                icon: 10,
                slider: 10,
            },

            valuechangeCallback: function (value, oldValue, numberBar) {
                numberBar.text = Math.round(Phaser.Math.Linear(0, 100, value))
                this.scene.game.config.audio.volume = numberBar.text
                console.log("Volume: " + this.scene.game.config.audio.volume)
                this.bell = this.scene.sound.add("note", { volume: (this.scene.game.config.audio.volume) / 50 })
                this.bell.play()
            },
        })
            .layout();

        this.numberBar.setValue(this.fl, 0, 100)

        this.game.config.audio.volume = this.fl


        this.menu = this.add.text(600, 600, 'Indietro', { fontSize: 50, color: "#ff4c7c", fontFamily: 'myFont' })
            .setOrigin(0.5)
            .setPadding(20)
            .setStyle({ backgroundColor: '#0c5daf' })
            .setInteractive()
            .on('pointerdown', () => this.scene.start("menu"))
            .on('pointerover', () => this.menu.setScale(1.2))
            .on('pointerout', () => this.menu.setScale(1))



        // particelle mouse
        const scene = this
        let canvas = this.sys.canvas;
        canvas.style.cursor = 'none';
        var emitter = this.add.particles('cursor').createEmitter({
            x: 0,
            y: 0,
            blendMode: 'SCREEN',
            scale: { start: 0.3, end: 0.2 },
            quantity: 2,
            rotation: 0,
            lifespan: 200,
        });
        var emitZones = [];
        var emitZoneIndex = 0;

        this.input.on('pointermove', function (pointer) {
            emitter.setPosition(pointer.x, pointer.y)
            scene.scale.updateBounds();
        });
        emitter.setEmitZone(emitZones[emitZoneIndex]);
        //------------------------------------------------------------------------------
    }

    update(time, delta) {

    }
}



export default options;