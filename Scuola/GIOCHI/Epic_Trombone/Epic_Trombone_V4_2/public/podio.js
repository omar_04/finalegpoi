const caricaCLassifica = async (nome_canzone) => {
    const flag = new Promise((resolve, reject) => {
        fetch('/epicTrombone/plays/classifica?song=' + nome_canzone)
            .then(res => res.json())
            .then((data) => {
                let punti = []
                let username = []
                let canzone = []
                let info = []
                data.forEach(el => {
                    canzone.push(el["nome_canzone_fk"])
                    username.push(el["username_fk"])
                    punti.push(el["punteggio_partita"])
                })
                info.push(canzone)
                info.push(punti)
                info.push(username)

                resolve(data)
            })
    })
    return await flag
}


class podio extends Phaser.Scene {
    constructor() {
        super("podio")
    }

    init(data) {
        this.nameSong = data.id
    }


    preload() {
        this.load.scenePlugin({
            key: 'rexuiplugin',
            url: 'https://unpkg.com/phaser3-rex-plugins@1.1.85/dist/rexuiplugin.min.js',
            sceneKey: 'rexUI'
        });
        var url;
        url = 'https://raw.githubusercontent.com/rexrainbow/phaser3-rex-notes/master/dist/rexgridtableplugin.min.js';
        this.load.plugin('rexgridtableplugin', url, true);
        url = 'https://raw.githubusercontent.com/rexrainbow/phaser3-rex-notes/master/dist/rexscrollerplugin.min.js';
        this.load.plugin('rexscrollerplugin', url, true);

        this.load.image('podio', 'assets/img/classifica/podio.png');
        this.load.image("catPC", "assets/img/catGif/catPC.png")
        this.load.video('background', 'assets/img/bg/bg.mp4', 'loadeddata', false, true)


        this.punti = []
        this.user = []


        this.nomeCanzoni = []
        this.glafCanz = 0



        caricaCLassifica(this.nameSong[0])
            .then((data) => {
                this.nomeCanzoni = data
                this.glafCanz = 1
            })



    }

    genera() {
        console.log(this.nomeCanzoni)
        try {
            var t1 = this.add.text(610, 48, this.nomeCanzoni[0].username_fk, { fontFamily: 'myFont', fontSize: 18, color: "white" }).setOrigin(0.5);
            var t2 = this.add.text(500, 90, this.nomeCanzoni[1].username_fk, { fontFamily: 'myFont', fontSize: 18, color: "white" }).setOrigin(0.5);
            var t3 = this.add.text(716, 130, this.nomeCanzoni[2].username_fk, { fontFamily: 'myFont', fontSize: 18, color: "white" }).setOrigin(0.5);

            this.tweens.add({
                targets: [t1, t2, t3],
                scaleX: 1.1,
                scaleY: 1.1,
                duration: 1500,
                yoyo: true,
                repeat: -1
            });
        } catch (error) {
            console.error(error);
        }


        var gridTable = this.rexUI.add.gridTable({
            x: 600,
            y: 470,
            width: 900,
            height: 420,
            scrollMode: 0,

            table: {
                cellWidth: undefined,
                cellHeight: 30,
                columns: 1,

                mask: {
                    padding: 2,
                },
                reuseCellContainer: true,
            },

            header: createRowItem(this,
                {
                    background: this.rexUI.add.roundRectangle(0, 0, 30, 30, 0, 0x0c5daf),
                    id: this.add.text(0, 0, 'Giocatore', { fontSize: 30, color: "#ff4c7c", fontFamily: 'myFont' }),
                    score: this.add.text(0, 0, 'Punteggio', { fontSize: 30, color: "#ff4c7c", fontFamily: 'myFont' }),
                    height: 40
                }
            ),


            space: {
                left: 20,
                right: 20,
                top: 20,
                bottom: 20,

                table: 10,
                header: 10,
                footer: 10,
            },

            createCellContainerCallback: function (cell, cellContainer) {
                var scene = cell.scene,
                    width = cell.width,
                    height = cell.height,
                    item = cell.item,
                    index = cell.index;
                if (cellContainer === null) {
                    cellContainer = createRowItem(scene);
                }

                // Set properties from item value
                cellContainer.setMinSize(width, height); // Size might changed in this demo
                cellContainer.getElement('id').setText(item.username_fk);
                cellContainer.getElement('score').setText(item.punteggio_partita);
                return cellContainer;
            },
            items: this.nomeCanzoni
        })
            .layout()

        // particelle mouse
        const scene = this
        let canvas = this.sys.canvas;
        canvas.style.cursor = 'none';
        var emitter = this.add.particles('cursor').createEmitter({
            x: 0,
            y: 0,
            blendMode: 'SCREEN',
            scale: { start: 0.3, end: 0.2 },
            quantity: 2,
            rotation: 0,
            lifespan: 200,
        });
        var emitZones = [];
        var emitZoneIndex = 0;

        this.input.on('pointermove', function (pointer) {
            emitter.setPosition(pointer.x, pointer.y)
            scene.scale.updateBounds();
        });
        emitter.setEmitZone(emitZones[emitZoneIndex]);
        //------------------------------------------------------------------------------

    }





    create() {

        this.vid = this.add.video(600, 350, 'background').setScale(0.7)
        this.vid.play(true)
        this.vid.setPaused(false)

        this.add.text(100, 30, this.nameSong[0], { fontSize: 32, fontFamily: "myFont" })

        var p1 = this.add.image(590, 70, 'podio').setScale(0.5);

        this.menu = this.add.text(1060, 60, 'Indietro', { fontSize: 35, color: "#ff4c7c", fontFamily: 'myFont' })
            .setOrigin(0.5)
            .setPadding(20)
            .setStyle({ backgroundColor: '#0c5daf' })
            .setInteractive()
            .on('pointerdown', () => this.scene.start("classifica"))
            .on('pointerover', () => this.menu.setScale(1.2))
            .on('pointerout', () => this.menu.setScale(1))



        this.mioCat1 = this.physics.add
            .sprite(40, 580, "catPC")
            .setSize(40, 65)
            .setOffset(70, 20)
            .setScale(0.7)


    }

    update(time, delta) {
        if (this.glafCanz == 1) {
            this.glafCanz = 2
            this.genera()
        }

    }
}


const GetValue = Phaser.Utils.Objects.GetValue;
var createRowItem = function (scene, config) {
    var background = GetValue(config, 'background', undefined);
    if (background === undefined) {
        background = scene.rexUI.add.roundRectangle(0, 0, 20, 20, 0).setFillStyle(0x0c5daf)
    }
    var id = GetValue(config, 'id', undefined);
    if (id === undefined) {
        id = scene.add.text(0, 0, id, { fontSize: 25, color: "white", fontFamily: 'arial' });
    }
    var score = GetValue(config, 'score', undefined);
    if (score === undefined) {
        score = scene.add.text(0, 0, score, { fontSize: 25, color: "white", fontFamily: 'arial' });
    }
    return scene.rexUI.add.sizer({
        width: GetValue(config, 'width', undefined),
        height: GetValue(config, 'height', undefined),
        orientation: 'x',
    })
        .addBackground(
            background
        )
        .add(
            id,    // child
            0,                           // proportion, fixed width
            'center',                    // align vertically
            { left: 30 },                // padding
            false,                       // expand vertically
            'id'                         // map-key
        )
        .addSpace()
        .add(
            score, // child
            0,                           // proportion, fixed width
            'center',                    // align vertically
            { right: 30 },               // padding
            false,                       // expand vertically
            'score'                      // map-key
        )
}

export default podio;