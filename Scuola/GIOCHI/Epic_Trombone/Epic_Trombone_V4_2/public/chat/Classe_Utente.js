class Utenti {
    constructor() {
        this.utenti = {};
    }

    add_utente(id, nome){
        this.utenti[id]={
            "nome": nome,
            "color": this.getRandomColor()
        }
        this.stampa()
    }

    cerca_utente(id){
        if (this.utenti[id]) {
            return this.utenti[id]
        }
    }

    getColor(id){
        let utente = this.cerca_utente(id)
        return utente["color"]
    }

    del_utente(id){
        let nome = this.cerca_utente(id).nome
        delete this.utenti[id];
        return nome
    }

    getUtenti(){
        return this.utenti
    }

    stampa(){
        console.log(this.utenti)
    }

    getRandomColor() {
        let letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
}

module.exports = {Utenti};
