const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const user = urlParams.get('user')
const password = urlParams.get('password')

var messages = document.querySelector('.campo_messaggi');
var form = document.querySelector('.form_mess');
var input = document.querySelector('.messaggio');
var send = document.querySelector('.send');
var lista_utenti = document.querySelector('.lista_utenti');
var lista_css = document.querySelector('.lista');
var conte_css = document.querySelector('.content');

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

////ONCLICK INVIO MESSAGGIO
const invia = () => {
    if (input.value) {
        socket.emit('new_messagge_chat', messagge_format(input.value), (c) => {
            messaggio_layout_io(messagge_format(input.value), c)
            input.value = '';
        })
    }
}

/////ENTER INVIO MESSAGGIO
input.addEventListener("keypress", function (event) {
    if (event.key === "Enter") {
        event.preventDefault();
        send.click();
    }
});

///SOCKET
const socket = io()

socket.on('connect', () => {

    //messaggio benevenuto
    socket.emit('new_client', user, (mess) => {
        messaggio_layout_admin(mess)
    })
})

socket.on('disconnect', () => {
    console.log("SCONNESSO")
})
//////////MESSAGE ADMIN
socket.on('admin', (mess) => {
    messaggio_layout_admin(mess)
})

const messaggio_layout_admin = (msg) => {
    let main_mess = document.createElement('div');
    main_mess.className = "main_mess_admin"
    main_mess.textContent = msg
    main_mess.style.color = "white"

    messages.appendChild(main_mess);

    main_mess.scrollIntoView()
}

//////////MESSAGE USER
const messaggio_layout_io = (msg, c) => {
    let main_mess = document.createElement('div');
    main_mess.className = "main_mess_io"
    main_mess.style.color = c

    let time = document.createElement('div')
    time.textContent = msg.time
    time.className = "time"

    let user = document.createElement('div')
    user.textContent = msg.user
    user.className = "user"

    let mess = document.createElement('div')
    mess.textContent = msg.mess
    mess.className = "mess"

    main_mess.appendChild(user)
    main_mess.appendChild(mess)
    main_mess.appendChild(time)

    messages.appendChild(main_mess);

    main_mess.scrollIntoView()
}

//////////MESSAGE ALTRUI
socket.on('new_messagge', (mess) => {
    messaggio_layout(mess)
})

const messaggio_layout = (msg) => {
    console.log(msg)
    let main_mess = document.createElement('div');
    main_mess.className = "main_mess_altri"
    main_mess.style.color = msg.color

    let time = document.createElement('div')
    time.textContent = msg.dati.time
    time.className = "time"

    let user = document.createElement('div')
    user.textContent = msg.dati.user
    user.className = "user"

    let mess = document.createElement('div')
    mess.textContent = msg.dati.mess
    mess.className = "mess"

    main_mess.appendChild(user)
    main_mess.appendChild(mess)
    main_mess.appendChild(time)

    messages.appendChild(main_mess);

    main_mess.scrollIntoView()
}

const messagge_format = (mess) => {
    let now = new Date()
    let day = now.getDate()
    let hours = now.getHours()
    let minutes = now.getMinutes()
    let time = hours + ":" + minutes
    let fullmess = { "time": time, "user": user, "mess": mess }
    return fullmess
}

/////////////LISTA UTENTI CONNESSI
socket.on('list_user', (u) => {
    console.log(u)
    lista_utenti.innerHTML = "";
    Object.keys(u).forEach(key => {
        console.log(u[key])
        create_div_utente(u[key].nome)
    });
})

const create_div_utente = (user) => {
    let item = document.createElement('div');
    item.textContent = user;
    item.className = "m"
    lista_utenti.appendChild(item);
}

//CSS
lista_css.addEventListener('mouseover', () => {
    lista_css.style["height"] = '20%';
    conte_css.style["height"] = '80%';
    lista_utenti.style.display = "block";
}, false);

lista_css.addEventListener('mouseleave', () => {
    lista_utenti.style.display = "none";
    lista_css.style["height"] = '5%';
    conte_css.style["height"] = '95%';
}, false);
