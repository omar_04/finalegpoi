//classe di insieme di bottone
class Buttons extends Phaser.Physics.Arcade.Group {
    constructor(scene) {
            super(scene.physics.world, scene)

            this.createMultiple({
                frameQuantity: 100,
                key: 'bg_tasto', //forma delle bottone
                active: false,
                visible: false,
                classType: Button
            });
        }
        //cordinate x, y e tipo di nota
    spawnButton(x, y, nt) {
        let bottone = this.getFirstDead(false); //se riesci a instaziare un oggetto (boolean)
        if (bottone) {
            bottone.draw(x, y, nt);
        }
    }

    static preload(scene) {
        scene.load.image("bg_tasto", "./assets/area_tasto.png")
    }
}

//classe di delle bottone
class Button extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'bg_tasto')
    }

    //cordinate x, y e tipo di bottone
    draw(x, y, nt) {
        this.body.reset(x, y);
        this.setActive(true)
        this.setVisible(false)
        this.setScale(1)
        this.type = nt
    }
}

export default Buttons