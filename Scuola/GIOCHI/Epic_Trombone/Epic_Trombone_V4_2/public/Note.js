import Punteggio from "./ClassPunteggio.js"
//classe di insieme di note
class Notes extends Phaser.Physics.Arcade.Group {
    constructor(scene) {
            super(scene.physics.world, scene)

            this.createMultiple({
                frameQuantity: 100,
                key: 'note1', //forma delle note
                active: false,
                visible: false,
                classType: Note
            });

        }
        //cordinate x, y e tipo di nota
    spawnNote(x, y, nt, err) {
        let note = this.getFirstDead(false); //se riesci a instaziare un oggetto (boolean)
        if (note) {
            note.draw(x, y, nt, err);
        }
    }

    static preload(scene) {
        scene.load.atlas("note", "assets/img/note/note.png", "assets/img/note/note.json")
    }
}

//classe di delle note
class Note extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'note')

        scene.anims.create({
            key: 'ballIdle',
            frames: this.anims.generateFrameNames('note', {
                start: 0,
                end: 31,
                zeroPad: 2,
                prefix: 'frame_',
                suffix: '_delay-0.08s.gif'
            }),
            frameRate: 15,
            repeat: -1,
        })
    }

    //cordinate x, y e tipo di nota
    draw(x, y, nt, err) {
        this.body.reset(x, y);
        this.setActive(true)
        this.setVisible(true)
        this.setScale(0.20)
        this.type = nt
        this.btnErr = err
        this.anims.play('ballIdle', true)
    }

    preUpdate(time, delta) {
        super.preUpdate(time, delta);
        if (this.y > 700) {
            Punteggio.Sbagliato()
            this.btnErr.anims.play("errActive")
            this.scene.time.addEvent({ delay: 250, callback: () => { this.btnErr.anims.play('errIdle', false) }, callbackScope: this, loop: false })
            this.destroy()
        }
    }
}

export default Notes