class piume extends Phaser.Scene {

    constructor() {
        super("piume")
    }

    preload() {
        this.load.image('piume', './assets/img/particelle/part1.png');
    }

    create() {
        var particles = this.add.particles('piume');

        var emitter = particles.createEmitter({
            x: { min: 0, max: 1200 },
            y: 0,
            angle: { start: 0, end: 360, steps: 360 },
            speed: { min: -100, max: 100 },
            lifespan: { min: 1000, max: 4000 },
            blendMode: 'ADD',
            quantity: 4,
            scale: { start: 0.90, end: 0.1 },
            moveToX: { min: 0, max: 1200 },
            moveToY: 800,
            rotate: { min: 0, max: 360, start: 0, end: 360 },
        });
        emitter.setFrequency(1000, 2);
    }
    update(time, delta) { }

}

export default piume