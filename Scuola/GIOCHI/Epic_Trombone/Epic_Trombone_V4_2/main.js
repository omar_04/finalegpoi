const express = require('express')
const router = express.Router()
const path = require('path')
const pool = require('./db/db_connection')

router.use(express.static(path.join(__dirname, 'public')))

const path_home = path.join('./Home/public')
console.log(path_home)

//login
router.get('/', (req, res) => {
    pool.query("SELECT * FROM utenti WHERE utenti.username = '" + req.query.user + "'", (err, r, f) => {
        if (err) {
            console.error(err);
            return;
        }
        if (r.length > 0) {
            console.log(req.mess + "Login successful")
            console.log(req.mess + "Epic!")
            res.sendFile('gioco.html', { root: __dirname + "/public" })
        } else {
            console.log(req.mess + "Login failed")
            res.redirect('/')
        }
    });
})

// canzoni
const route_canzoni = require('./sotto_rotte/canzoni')
router.use('/canzoni', route_canzoni)

// utenti
const route_utenti = require('./sotto_rotte/utenti')
router.use('/utenti', route_utenti)

// plays
const route_plays = require('./sotto_rotte/plays')
router.use('/plays', route_plays)

module.exports = router