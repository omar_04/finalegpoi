SELECT punteggio_partita, username_fk
FROM gr2_gioca
ORDER BY punteggio_partita DESC
LIMIT 3
GROUP BY username_fk
-- GROUP BY nome_canzone_fk 

-- WHERE nome_canzone_fk="hxh"
-- GROUP BY username_fk

SELECT SUM(gr2_gioca.username_fk)
FROM gr2_gioca
GROUP BY gr2_gioca.punteggio_partita

--Controllo esistenza
SELECT *
FROM gr2_gioca
WHERE WHERE username_fk = 'Xu' AND
nome_canzone_fk='hxh';

--Aggiorno
UPDATE gr2_gioca
SET punteggio_partita = 0
WHERE username_fk = 'Xu' AND
nome_canzone_fk='hxh';

--Aggiungo
INSERT INTO gr2_gioca 
(punteggio_partita, username_fk, nome_canzone_fk) 
VALUES (100, "Xu", "hxh")

--Podio
SELECT punteggio_partita, username_fk
FROM gr2_gioca
WHERE nome_canzone_fk='demonslayer'
ORDER BY punteggio_partita DESC
LIMIT 3

--Classifica
SELECT punteggio_partita, username_fk 
FROM gr2_gioca 
WHERE nome_canzone_fk='demonslayer' 
ORDER BY punteggio_partita DESC;