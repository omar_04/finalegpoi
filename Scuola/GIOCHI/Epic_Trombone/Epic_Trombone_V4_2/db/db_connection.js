const mysql = require('mysql');

const db_info={
  host: "localhost",
  user: "root",
  password: "",
  database: "Stig_2023"
}

const pool = mysql.createPool(db_info);

pool.on('connection', function (connection) {
    console.log('DB Connection established');

    connection.on('error', function (err) {
        console.error(new Date(), 'MySQL error', err.code);
    });
    connection.on('close', function (err) {
        console.error(new Date(), 'MySQL close', err);
    });

});

module.exports = pool;
