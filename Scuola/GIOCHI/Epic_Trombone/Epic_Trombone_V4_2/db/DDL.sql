DROP DATABASE IF EXISTS db_epicTrombone;

CREATE DATABASE db_epicTrombone;

USE db_epicTrombone;

-------------------------------------------

CREATE TABLE utenti(
	username VARCHAR(15) NOT NULL,
	password CHAR(64) NOT NULL,
	nome VARCHAR(25) NOT NULL,
	cognome VARCHAR(30) NOT NULL,
	email VARCHAR(50) NOT NULL,
	punteggio INT(11) DEFAULT 0,
	nomegioco VARCHAR(25) NULL,
	PRIMARY KEY(username)
);

CREATE TABLE gr2_canzoni (
    nome_canzone VARCHAR(25) NOT NULL PRIMARY KEY,
    mp3 VARCHAR(50) NOT NULL
);

CREATE TABLE gr2_gioca (
	cod_partita INT auto_increment NOT NULL PRIMARY KEY,
    punteggio_partita INT(11) DEFAULT 0,
	username_fk VARCHAR(15),
	nome_canzone_fk VARCHAR(25),
	FOREIGN KEY(username_fk) REFERENCES utenti(username),
	FOREIGN KEY(nome_canzone_fk) REFERENCES gr2_canzoni(nome_canzone)
);