const express = require('express')
const router = express.Router()
const pool = require('../db/db_connection')

router.get('/', (req, res) => {
    console.log(req.mess + "canzoni")
    res.write(req.mess + "canzoni\n")
    res.end(__dirname)
})

router.get('/get', (req, res) => {
    console.log(req.mess + "getSongs")
    pool.query("SELECT * FROM gr2_canzoni", (err, r, f) => {
        if (err) {
            console.error(err);
            return;
        }
        res.setHeader("Content-Type", "application/json")
        res.setHeader("cache-control", "no-cache")
        res.json(r)
    });
})

module.exports = router