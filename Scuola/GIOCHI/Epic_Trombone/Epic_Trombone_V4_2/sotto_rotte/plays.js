const express = require('express')
const router = express.Router()
const pool = require('../db/db_connection')

router.get('/', (req, res) => {
    console.log(req.mess + "plays")
    res.write(req.mess + "plays\n")
    res.end(__dirname)
})

router.get('/controllo_esistenza', (req, res) => {
    const { username, song } = req.query
    pool.query("SELECT * FROM gr2_gioca WHERE username_fk = '" + username + "' AND nome_canzone_fk='" + song + "';", (err, r, f) => {
        if (err) {
            console.error(err);
            return;
        }
        res.setHeader("Content-Type", "application/json")
        res.setHeader("cache-control", "no-cache")
        res.json(r)
    });
})

router.get('/update', (req, res) => {
    const { username, punteggio, song } = req.query
    console.log(req.mess + "updatePlay")
    console.log("username: " + username)
    console.log("punteggio: " + punteggio)
    console.log("song: " + song)
    pool.query("UPDATE gr2_gioca SET punteggio_partita = " + punteggio + " WHERE username_fk = '" + username + "' AND nome_canzone_fk='" + song + "';", (err, r, f) => {
        if (err) {
            console.error(err);
            return;
        }
        res.setHeader("Content-Type", "application/json")
        res.setHeader("cache-control", "no-cache")
        console.log(req.mess + "1 record updated")
        res.json({ mess: "1 record updated" })
    });
})

router.get('/add', (req, res) => {
    // http://192.168.1.158:3001/1/plays/add?username=blank&punteggio=100&song=mii
    const { username, punteggio, song } = req.query
    console.log(req.mess + "addPlay")
    console.log("username: " + username)
    console.log("punteggio: " + punteggio)
    console.log("song: " + song)
    pool.query("INSERT INTO gr2_gioca (punteggio_partita, username_fk, nome_canzone_fk) VALUES (" + punteggio + ", '" + username + "', '" + song + "');", (err, r, f) => {
        if (err) {
            console.error(err);
            return;
        }
        res.setHeader("Content-Type", "application/json")
        res.setHeader("cache-control", "no-cache")
        console.log(req.mess + "1 record added")
        res.json({ mess: "1 record added" })
    });
})

router.get('/podio', (req, res) => {
    const { song } = req.query
    pool.query("SELECT punteggio_partita, username_fk FROM gr2_gioca WHERE nome_canzone_fk='" + song + "' ORDER BY punteggio_partita DESC LIMIT 3;", (err, r, f) => {
        if (err) {
            console.error(err);
            return;
        }
        res.setHeader("Content-Type", "application/json")
        res.setHeader("cache-control", "no-cache")
        console.log(req.mess + "getPodio")
        res.json(r)
    });
})

router.get('/classifica', (req, res) => {
    const { song } = req.query
    pool.query("SELECT punteggio_partita, username_fk, nome_canzone_fk FROM gr2_gioca WHERE nome_canzone_fk='" + song + "' ORDER BY punteggio_partita DESC;", (err, r, f) => {
        if (err) {
            console.error(err);
            return;
        }
        res.setHeader("Content-Type", "application/json")
        res.setHeader("cache-control", "no-cache")
        console.log(req.mess + "getClassifica")
        res.json(r)
    });
})

module.exports = router