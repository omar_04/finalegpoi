const express = require('express')
const app = express()
const port = process.env.PORT || 1729;
const os = require('os')
const socketIO = require('socket.io')
const http = require('http')
const server = http.createServer(app)
const io = socketIO(server)

const { Utenti } = require('./Epic_Trombone_V4_2/public/chat/Classe_Utente')
let utenti = new Utenti()

const ip = require('ip')
console.log("Host ip: " + ip.address())
const private_ip = ip.address()

const user_info = (req, res, next) => {
	const u = os.userInfo().username;
	const a = req.socket.remoteAddress
	const d = new Date().toLocaleString('en-GB', {
		hour12: false,
	});
	req.mess = "[" + d + "] [" + a + "] [" + u + "]: "
	next()
}
app.use(user_info)

// Home
const route_home = require('./Home/main')
app.use('/', route_home)

// Epic Trombone
const route_epic_trombone = require('./Epic_Trombone_V4_2/main')
app.use('/epicTrombone', route_epic_trombone)

// Gestione di tutti gli errori
app.all('*', (req, res) => {
	// let clientIp = userInfo(req)
	console.log(req.mess + "error!")
	res.setHeader("content-type", "application/json")
	res.setHeader("cache-control", "no-cache")
	res.json({ "result": "ko" })
})

io.on('connection', (socket) => {
	socket.on('new_client', (dati, callback) => {
		utenti.add_utente(socket.id, dati)
		console.log(dati + " si è collegato")
		socket.emit('list_user', utenti.getUtenti())
		socket.broadcast.emit('list_user', utenti.getUtenti())
		socket.broadcast.emit('admin', dati + " online")
		callback(dati + " online")
	})

	socket.on('new_messagge_chat', (dati, callback) => {
		let color = utenti.getColor(socket.id)
		console.log(color)
		socket.broadcast.emit('new_messagge', {
			"dati": dati,
			"color": color
		})
		callback(color)
	})

	socket.on('disconnect', () => {
		let nome = utenti.del_utente(socket.id)
		socket.broadcast.emit('admin', nome + " offline")
		socket.broadcast.emit('list_user', utenti.getUtenti())

		// socket.broadcast.emit('altri_client', dati+" si è sconnesso")
	})
})

server.listen(port, () => console.log(`Example app listening on port ${port}!`))
